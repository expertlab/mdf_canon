#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from urllib.request import urlopen, Request, urlretrieve
from json import loads
import subprocess
from time import sleep
from traceback import print_exc, format_exc
import zipfile, tarfile

from mdf_canon import csutil
from mdf_canon.logger import get_module_logging
from collections import namedtuple
logging = get_module_logging(__name__)

home = os.path.join(os.path.expanduser('~'), '.crystal')

template = """<configuration version="{cfversion}">
    <folder id="{target_nick}" label="{target_nick}" path="{target}" type="sendonly" rescanIntervalS="3600" fsWatcherEnabled="true" fsWatcherDelayS="10" ignorePerms="false" autoNormalize="true">
        <filesystemType>basic</filesystemType>
        <device id="{local}" introducedBy="">
            <encryptionPassword></encryptionPassword>
        </device>
        <device id="{remote}" introducedBy="">
            <encryptionPassword></encryptionPassword>
        </device>
        <minDiskFree unit="%">1</minDiskFree>
        {versioning_type_text}
        <copiers>0</copiers>
        <pullerMaxPendingKiB>0</pullerMaxPendingKiB>
        <hashers>0</hashers>
        <order>random</order>
        <ignoreDelete>false</ignoreDelete>
        <scanProgressIntervalS>0</scanProgressIntervalS>
        <pullerPauseS>0</pullerPauseS>
        <maxConflicts>-1</maxConflicts>
        <disableSparseFiles>false</disableSparseFiles>
        <disableTempIndexes>false</disableTempIndexes>
        <paused>false</paused>
        <weakHashThresholdPct>25</weakHashThresholdPct>
        <markerName>.stfolder</markerName>
        <copyOwnershipFromParent>false</copyOwnershipFromParent>
        <modTimeWindowS>0</modTimeWindowS>
        <maxConcurrentWrites>2</maxConcurrentWrites>
        <disableFsync>false</disableFsync>
        <blockPullOrder>standard</blockPullOrder>
    </folder>{versions_folder}{additional_folders}
    <device id="{local}" name="{nick}" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy="">
        <address>dynamic</address>
        <paused>false</paused>
        <autoAcceptFolders>false</autoAcceptFolders>
        <maxSendKbps>0</maxSendKbps>
        <maxRecvKbps>0</maxRecvKbps>
        <maxRequestKiB>0</maxRequestKiB>
    </device>
    <device id="{remote}" name="{remote_nick}" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy="">
        <address>dynamic</address>
        <paused>false</paused>
        <autoAcceptFolders>false</autoAcceptFolders>
        <maxSendKbps>0</maxSendKbps>
        <maxRecvKbps>0</maxRecvKbps>{ignored_folder}
        <maxRequestKiB>0</maxRequestKiB>
    </device>{additional_devices}
    <gui enabled="true" tls="false" debugging="false">
        <address>{address}:{port}</address>
        <apikey>{apikey}</apikey>
        <theme>default</theme>
    </gui>
    <ldap></ldap>
    <options>
        <listenAddress>tcp://0.0.0.0:{listen_port}</listenAddress>
        <listenAddress>dynamic+https://relays.syncthing.net/endpoint</listenAddress>
        <listenAddress>relay://{relay}:443/?id=VHNGUQJ-7GXHKGR-GYX6VGD-TWSR6YJ-YKYAAHJ-SV4KGUI-KOS64F7-3MHKTQU</listenAddress>
        <globalAnnounceServer>default</globalAnnounceServer>
        <globalAnnounceEnabled>true</globalAnnounceEnabled>
        <localAnnounceEnabled>true</localAnnounceEnabled>
        <localAnnouncePort>21027</localAnnouncePort>
        <localAnnounceMCAddr>[ff12::{port}]:{ann_port}</localAnnounceMCAddr>
        <maxSendKbps>0</maxSendKbps>
        <maxRecvKbps>0</maxRecvKbps>
        <reconnectionIntervalS>60</reconnectionIntervalS>
        <relaysEnabled>true</relaysEnabled>
        <relayReconnectIntervalM>10</relayReconnectIntervalM>
        <startBrowser>false</startBrowser>
        <natEnabled>true</natEnabled>
        <natLeaseMinutes>60</natLeaseMinutes>
        <natRenewalMinutes>30</natRenewalMinutes>
        <natTimeoutSeconds>10</natTimeoutSeconds>
        <urAccepted>3</urAccepted>
        <urSeen>3</urSeen>
        <urUniqueID>{report_id}</urUniqueID>
        <urURL>https://data.syncthing.net/newdata</urURL>
        <urPostInsecurely>false</urPostInsecurely>
        <urInitialDelayS>1800</urInitialDelayS>
        <restartOnWakeup>true</restartOnWakeup>
        <autoUpgradeIntervalH>24</autoUpgradeIntervalH>
        <upgradeToPreReleases>false</upgradeToPreReleases>
        <keepTemporariesH>24</keepTemporariesH>
        <cacheIgnoredFiles>false</cacheIgnoredFiles>
        <progressUpdateIntervalS>5</progressUpdateIntervalS>
        <limitBandwidthInLan>false</limitBandwidthInLan>
        <minHomeDiskFree unit="%">1</minHomeDiskFree>
        <releasesURL>https://upgrades.syncthing.net/meta.json</releasesURL>
        <overwriteRemoteDeviceNamesOnConnect>false</overwriteRemoteDeviceNamesOnConnect>
        <tempIndexMinBlocks>10</tempIndexMinBlocks>
        <trafficClass>0</trafficClass>
        <defaultFolderPath>~</defaultFolderPath>
        <setLowPriority>true</setLowPriority>
        <maxFolderConcurrency>0</maxFolderConcurrency>
        <crashReportingURL>https://crash.syncthing.net/newcrash</crashReportingURL>
        <crashReportingEnabled>true</crashReportingEnabled>
        <stunKeepaliveStartS>180</stunKeepaliveStartS>
        <stunKeepaliveMinS>20</stunKeepaliveMinS>
        <stunServer>default</stunServer>
        <databaseTuning>auto</databaseTuning>
        <maxConcurrentIncomingRequestKiB>0</maxConcurrentIncomingRequestKiB>
    </options>
</configuration>"""

template_versions = """
    <folder id="versions/{target_nick}" label="versions/{target_nick}" path="{versions}" type="receiveonly" rescanIntervalS="3600" fsWatcherEnabled="true" fsWatcherDelayS="10" ignorePerms="false" autoNormalize="true">
        <filesystemType>basic</filesystemType>
        <device id="{local}" introducedBy="">
            <encryptionPassword></encryptionPassword>
        </device>
        <device id="{remote}" introducedBy="">
            <encryptionPassword></encryptionPassword>
        </device>
        <minDiskFree unit="%">1</minDiskFree>
        <versioning></versioning>
        <copiers>0</copiers>
        <pullerMaxPendingKiB>0</pullerMaxPendingKiB>
        <hashers>0</hashers>
        <order>random</order>
        <ignoreDelete>false</ignoreDelete>
        <scanProgressIntervalS>0</scanProgressIntervalS>
        <pullerPauseS>0</pullerPauseS>
        <maxConflicts>10</maxConflicts>
        <disableSparseFiles>false</disableSparseFiles>
        <disableTempIndexes>false</disableTempIndexes>
        <paused>false</paused>
        <weakHashThresholdPct>25</weakHashThresholdPct>
        <markerName>.</markerName>
        <copyOwnershipFromParent>false</copyOwnershipFromParent>
        <modTimeWindowS>0</modTimeWindowS>
        <maxConcurrentWrites>0</maxConcurrentWrites>
        <disableFsync>false</disableFsync>
        <blockPullOrder>standard</blockPullOrder>
    </folder>
"""

template_staggered_versioning = """<versioning type="staggered">
            <param key="cleanInterval" val="3600"></param>
            <param key="maxAge" val="31536000"></param>
            <param key="versionsPath" val="{versions}"></param>
        </versioning>"""
        
template_additional_folder = """
    <folder id="{label}" label="{label}" path="{target}" type="{type}" rescanIntervalS="3600" fsWatcherEnabled="true" fsWatcherDelayS="10" ignorePerms="false" autoNormalize="true">
        <filesystemType>basic</filesystemType>
        <device id="{local}" introducedBy="">
            <encryptionPassword></encryptionPassword>
        </device>
        <device id="{device_id}" introducedBy="">
            <encryptionPassword></encryptionPassword>
        </device>
        <minDiskFree unit="%">1</minDiskFree>
        <versioning></versioning>
        <copiers>0</copiers>
        <pullerMaxPendingKiB>0</pullerMaxPendingKiB>
        <hashers>0</hashers>
        <order>random</order>
        <ignoreDelete>false</ignoreDelete>
        <scanProgressIntervalS>0</scanProgressIntervalS>
        <pullerPauseS>0</pullerPauseS>
        <maxConflicts>10</maxConflicts>
        <disableSparseFiles>false</disableSparseFiles>
        <disableTempIndexes>false</disableTempIndexes>
        <paused>false</paused>
        <weakHashThresholdPct>25</weakHashThresholdPct>
        <markerName>.</markerName>
        <copyOwnershipFromParent>false</copyOwnershipFromParent>
        <modTimeWindowS>0</modTimeWindowS>
        <maxConcurrentWrites>0</maxConcurrentWrites>
        <disableFsync>false</disableFsync>
        <blockPullOrder>standard</blockPullOrder>
    </folder>"""

template_additional_device = """
    <device id="{remote}" name="{remote_nick}" compression="metadata" introducer="false" skipIntroductionRemovals="false" introducedBy="">
        <address>dynamic</address>
        <paused>false</paused>
        <autoAcceptFolders>false</autoAcceptFolders>
        <maxSendKbps>0</maxSendKbps>
        <maxRecvKbps>0</maxRecvKbps>
        <maxRequestKiB>0</maxRequestKiB>
    </device>"""

template_ignored = """
        <ignoredFolder id="versions/{target_nick}"></ignoredFolder>"""

stignore = """**.lock
**.ldb
/log/*RPC
**.sync
**.sqlite
**.log
**.log.*
**.backup"""


def get_text_between(s, start, stop, i=0):
    i = s.find(start, i)
    if i < 0:
        logging.debug('Missing tag', start, i)
        return '', -1
    i += len(start)
    e = s.find(stop, i)
    if e < 0:
        logging.debug('Missing end tag', stop, i, e)
        return '', -1
    if i == e:
        logging.debug('Empty tag', start, stop, i, e)
        return '', -1
    return s[i:e], e + len(stop)


def extract_tag(s, tag, i=0):
    o = '<{}>'.format(tag)
    oe = '</{}>'.format(tag)
    return get_text_between(s, o, oe, i)


def iter_tag(s, tag):
    i = 0
    while 1:
        out, e = extract_tag(s, tag, i)
        if not out:
            break
        i = e    
        yield out


CONF_FAILED = 10
CONF_OK = 11
CONF_APPLIED = 12 


def get_versions_folder(xmlpath, target_nick):
    versions = os.path.join(os.path.dirname(xmlpath), 'versions', target_nick)
    if not os.path.exists(versions):
        logging.debug('Creating versions folder', versions)
        os.makedirs(versions, exist_ok=True)
    # stfolder = os.path.join(versions, '.stfolder')
    # if not os.path.exists(stfolder):
    #    logging.debug('Protecting stfolder', stfolder)
    #    os.mkdir(stfolder)
    #    f = os.path.join(stfolder, 'do_not_delete')
    #    f1 = open(f, 'w')
    #    f1.write('PLEASE DO NOT DELETE THIS FILE')
    #    f1.close()
    #    os.chmod(f, mode=0o444)
    #    os.chmod(stfolder, mode=0o555)
        
    return versions


def reconfigure(xmlpath, local, nick, remote, remote_nick, target, versioning=0, additional=[], folders=[], relay='sync.expertlabservice.it'):
    f = open(xmlpath, 'r')
    xml = f.read()
    f.close()
    apikey = extract_tag(xml, 'apikey')[0]
    report_id = extract_tag(xml, 'urUniqueID')[0]
    cfv = get_text_between(xml, '<configuration version="', '">')[0]
    # Do not conflic with standard installations
    target_nick = nick + '_' + os.path.basename(target)
    versions = get_versions_folder(xmlpath, target_nick)
    # Write ignore rules
    ignore_file = os.path.join(target, '.stignore')
    if os.path.exists(ignore_file):
        os.remove(ignore_file) 
    os.makedirs(os.path.dirname(ignore_file), exist_ok=True)
    f = open(ignore_file, 'w')
    f.write(stignore)
    f.close()
    
    # Activate remote versioning folder
    if versioning:
        ignored_folder = ''
        versioning_folder_text = template_versions.format(target_nick=target_nick,
                                             versions=versions,
                                             local=local,
                                             remote=remote)
        versioning_type_text = template_staggered_versioning.format(versions=versions)
    else:
        ignored_folder = template_ignored.format(target_nick=target_nick)
        versioning_folder_text = ''
        versioning_type_text = '<versioning></versioning>'
    
    def f_add_folders(rem_dev_id):
        r = ''
        for fold in folders:
            fold = fold[0]
            f_label = nick + '_' + fold.replace('/', '_').replace('\\', '_')
            r += template_additional_folder.format(label=f_label, target=fold,
                                                       device_id=rem_dev_id, local=local,
                                                       type='sendonly')
        return r
    
    add_folders = f_add_folders(remote)
    add_devices = ''
    addev = set([])
    # label, devid, type    
    for line in additional:
        label, devid, ftype = line
        devid = devid.replace(' ', '')
        ftype = ftype.lower().replace(' ', '')
        if ftype == 'disabled':
            continue
        if ftype not in ('sendonly', 'receiveonly'):
            ftype = 'sendreceive'
        add_folders += template_additional_folder.format(label=label, target=target,
                                                       device_id=devid, local=local,
                                                       type=ftype)
        add_folders += f_add_folders(devid)
        if devid in addev:
            continue
        add_devices += template_additional_device.format(remote=devid, remote_nick='Dev:' + label)
    
    default = template.format(cfversion=cfv,
                              local=local, nick=nick,
                              remote=remote, remote_nick=remote_nick,
                              target=target,
                              target_nick=target_nick,
                              versioning_type_text=versioning_type_text,
                              versions_folder=versioning_folder_text,
                              ignored_folder=ignored_folder,
                              additional_folders=add_folders,
                              additional_devices=add_devices,
                              apikey=apikey,
                              relay=relay,
                              address='127.0.0.1',
                              port=38384,
                              report_id=report_id,
                              ann_port=48384,
                              listen_port=58384)
    
    if xml == default:
        logging.debug('Configuration is already OK!')
        return CONF_OK
    
    # Save backup
    f = open(xmlpath + '.bak', 'w')
    f.write(xml)
    f.close()
    # Write new config
    os.chmod(xmlpath, 0o666)
    f = open(xmlpath, 'w')
    f.write(default)
    f.close()
    logging.debug('Reconfigured')
    return CONF_APPLIED


def initialize(exe_path, nick, remote, remote_nick, target, versioning=0, additional=[], folders=[], relay='sync.expertlabservice.it'):
    xmlpath = os.path.join(home, 'config.xml')
    if os.path.exists(xmlpath):
        os.chmod(xmlpath, 0o666)
    if os.name == 'nt':
        cmd = [exe_path, '-generate', home]
    else:
        cmd = '{} -generate="{}"'.format(exe_path, home)
    logging.debug('Executing', cmd)
    s, msg = csutil.go(cmd)
    logging.debug(msg)
    
    local = ''
    if s:
        logging.error('Failed to generate or inspect current configuration', s)
        return CONF_FAILED, local
    
    new = False
    for line in msg.splitlines():
        if 'INFO: Device ID:' in line:
            local = line.split(' ')[-1]
        elif 'INFO: Default folder created' in line:
            new = True
    r = reconfigure(xmlpath, local, nick, remote, remote_nick or 'crystal', target, versioning, additional, folders,
                    relay=relay)
    os.chmod(xmlpath, 0o444)
    return r, local


ERR_CONF = 1
ERR_INIT = 2
ERR_START = 3
ERR_UNDEF = 4


def get_address():
    conf = os.path.join(home, 'config.xml')
    f = open(conf, 'r')
    xml = f.read()
    f.close()
    address0 = False
    for val in iter_tag(xml, 'address'):
        if ':' in val and '.' in val:
            address0 = val
            break
    assert address0
    apikey = extract_tag(xml, 'apikey')[0]
    return address0, apikey


def get_ping(address0, apikey):
    address = 'http://' + address0 + '/rest/system/ping'
    logging.debug('Query', address, apikey)
    req = Request(address, headers={'X-API-Key':apikey})
    ok = False
    try:
        res = urlopen(req, timeout=1)
        content = res.read(2000)
        content = loads(content)
        logging.debug('Replied:', content)
        ok = content['ping'] == 'pong'
    except:
        print_exc()
    return ok


status = namedtuple('CrystalStatus',
                    ['code', 'address', 'local', 'remote'])
status.__new__.__defaults__ = (None, '', '', '')


def ensure_running(exe_path):
    """Check process running and POST request to verify IDs"""
    address, apikey = get_address()
    if get_ping(address, apikey):
        logging.debug('Service is already running')
        return status(0, address)
    
    cmd = '{} --allow-newer-config --home="{}"'.format(exe_path, home)
    if os.name == 'nt':
        cmd = [exe_path, '--allow-newer-config', '--home', home]
    else:
        cmd = [cmd]
    logging.debug('Executing', cmd)
    try:
        p = subprocess.Popen(cmd, shell=True)
        logging.debug('Syncthing PID:', p.pid)
    except:
        logging.error('Cannot start syncthing', format_exc())
        return status(ERR_INIT, address)
    return status(ERR_START, address)


def ensure_stopped(exe_path):
    address, apikey = get_address()
    address = 'http://' + address + '/rest/system/shutdown'
    logging.debug('Stopping', address, apikey)
    req = Request(address, headers={'X-API-Key':apikey}, data={})
    ok = False
    try:
        res = urlopen(req, timeout=1)
        content = res.read(2000)
        content = loads(content)
        logging.debug('Replied:', content)
    except:
        print_exc()


first_attempt = False


def install_syncthing(exe_path):
    dest = os.path.dirname(exe_path)
    if os.name == 'nt':
        url = "https://github.com/syncthing/syncthing/releases/download/v1.28.0/syncthing-windows-386-v1.28.0.zip"
        out = 'syncthing.zip'
        target = 'syncthing-windows-386-v1.28.0/syncthing.exe'
        extractor = lambda target: zipfile.ZipFile(out, 'r')
    else:
        url = "https://github.com/syncthing/syncthing/releases/download/v1.28.0/syncthing-linux-amd64-v1.28.0.tar.gz"
        out = 'syncthing.tar.gz'
        target = 'syncthing-linux-amd64-v1.28.0/syncthing'
        extractor = lambda target: tarfile.open(out, 'r:gz')
    if os.path.exists(out):
        logging.debug('Removing previous syncthing download', out)
        os.remove(out)
    logging.debug('Downloading syncthing...', url)
    urlretrieve(url, out)
    with extractor(target) as zip_ref:
        logging.debug('Extracting ', target, dest)
        zip_ref.extract(target)
        os.rename(target, os.path.join(dest, os.path.basename(target)))
    os.remove(out)


def check_status(exe_path, confdb):
    global first_attempt
    if first_attempt:
        return first_attempt
    target = os.path.dirname(confdb['database'])
    if not os.path.exists(target):
        return status(ERR_CONF)
    remote = confdb['cloud_remote']
    if not remote:
        return status(ERR_CONF)
    nick = confdb['cloud_localNick']
    if not nick:
        return status(ERR_CONF)
    if not os.path.exists(exe_path):
        try:
            install_syncthing(exe_path)
        except:
            logging.error('Could not install syncthing', format_exc())
    reinit, local = initialize(exe_path, nick, remote,
                               confdb['cloud_remoteNick'],
                               target,
                               confdb['cloud_versioning'],
                               confdb['cloud_additional'],
                               confdb['cloud_folders'],
                               relay=confdb['cloud_relay'])
    
    if reinit == CONF_FAILED:
        return status(ERR_INIT, local=local, remote=remote)
    if reinit == CONF_APPLIED:
        ensure_stopped(exe_path)
        sleep(1)
    st = ensure_running(exe_path)
    first_attempt = st
    return status(st.code, st.address, local, remote)

