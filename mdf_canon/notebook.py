# -*- coding: utf-8 -*-
"""
Short demo for Notebook functionality. Targets: 
0. Print test info
1. Show an image by index
2. Show an image by T
3. Print characteristic shapes
4. Default Plot Vol/T
"""
from mdf_canon.indexer import SharedFile
from mdf_canon import reference, csutil
import numpy as np
from copy import deepcopy
from scipy.interpolate import UnivariateSpline
from traceback import print_exc, format_exc


def temperature_slice(T, T1=None, T2=None):
    step = 1
    i1, i2 = None, None
    if T1 is None and T2 is None:
        return i1, i2, 1
    # Search for start of heating curve
    if T2 and T2 < max(T):
        i2 = np.where(T > T2)[0][0]
    # Search for end of heating curve
    if T1 and T[0] < T1:
        i1 = np.where(T < T1)[0]
        if i2:
            i1 = max(i1[i1 < i2])
        else:
            i1 = i1[-1]
    if T1 and T2 and T1 > T2:
        # Search for start of cooling curve
        if T1 > max(T):
            T1 = max(T)
        i0 = np.where(T == max(T))[0][0]
        i1 = np.where(T < T1)[0]
        i1 = i1[i1 > i0]
        if not len(i1):
            return False
        i1 = min(i1)
        # Search for end of cooling curve
        if T2 < T[-1]:
            i2 = None
        else:
            i2 = np.where(T < T2)[0]
            if not len(i2):
                i2 = None
            i2 = min(i2[i2 > i1])
        # Reverse the cooling
        step = -1
        print('cooling', i1, i2, T[-1], T1, T2)
    return i1, i2, step


class TimeTemperatureValue(object):
    t = None
    T = None
    v = None
    _fT = None
    _ft = None
    ext = 3

    def __init__(self, *a):
        a = list(a)
        if len(a) > 2:
            self.t, self.T, self.v = np.array(a[0]), np.array(a[1]), np.array(a[2])
        if len(a) > 3:
            self.fT = a[3]
        if len(a) > 4:
            self.ft = a[4]
        if self.t is not None:
            self.t -= self.t[0]
            
    @property
    def ft(self):
        if not self._ft:
            self.update()
        return self._ft

    @ft.setter
    def ft(self, val):
        self._ft = val
    
    @property
    def fT(self):
        if not self._fT:
            self.update()
        return self._fT
    
    @fT.setter
    def fT(self, val):
        self._fT = val
            
    def plot(self, *a, **k):
        from matplotlib import pylab
        return pylab.plot(self.T, self.v, *a, **k)

    def tplot(self, *a, **k):
        from matplotlib import pylab
        return pylab.plot(self.t, self.v, *a, **k)
                
    def copy(self):
        return TimeTemperatureValue(self.t.copy(), self.T.copy(), self.v.copy(),
                                    deepcopy(self.fT),
                                    deepcopy(self.ft))
        
    def resample(self, freq=1):
        dur = self.t[-1] - self.t[0]
        n = int(dur / freq)
        if freq == 1:
            nt = np.arange(0, n + 1)
        else:
            nt = np.linspace(0, dur, n + 1)
        nT = UnivariateSpline(self.t, self.T, s=0, ext=self.ext)(nt)
        nv = UnivariateSpline(self.t, self.v, s=0, ext=self.ext)(nt)
        return TimeTemperatureValue(nt, nT, nv)
    
    def cut_cooling(self, inplace=False):
        T = max(self.T)
        i = np.where(self.T == T)[0][0]
        if inplace:
            self.v = self.v[:i]
            self.T = self.T[:i]
            self.t = self.t[:i]
            if self._fT is not None:
                self.update()
            return True
        return TimeTemperatureValue(t, T, v)
        
    def cut(self, T1=None, T2=None, zero=False, oversample=1, uniform=False, inplace=False):
        """Cuts heating curve between T1, T2, optionally translating to zero"""
        c = self.copy()
        t, T, v = c.t, c.T, c.v
        if uniform and T1 == T2 and T1 is None:
            T1 = T[0]
            T2 = max(T)
        i1, i2, step = temperature_slice(T, T1, T2)
        if i1 or i2 or step:
            s = slice(i1, i2, step)
            t = t[s]
            v = v[s]
            T = T[s]
            # Remove non-uniform
            T, v, Ti = csutil.uniformx(T, v)
            t = t[Ti]
            
        if zero:
            v -= v[0]
            
        if oversample > 1:
            x = np.arange(len(T))
            x1 = np.arange(0, oversample * len(T)) / oversample
            t = UnivariateSpline(x, t, s=0, ext=self.ext)(x1)
            T = UnivariateSpline(x, T, s=0, ext=self.ext)(x1)
            v = UnivariateSpline(x, v, s=0, ext=self.ext)(x1)
        if inplace:
            self.t = t
            self.T = T
            self.v = v
            if self._ft is not None:
                self.update()
            return True
        r = TimeTemperatureValue(t, T, v)
        return r
    
    def update(self):
        self.ft = UnivariateSpline(self.t, self.v, s=0, ext=self.ext)
        try:
            self.fT = UnivariateSpline(self.T, self.v, s=0, ext=self.ext)
        except ValueError:
            print('non-uniform T', format_exc(), self.T)
            self.fT = None

    def export(self, sep=';', fmt='g'):
        out = "#time(m){0}temperature(C){0}value\n".format(sep)
        for i, t in enumerate(self.t):
            T = self.T[i]
            v = self.v[i]
            
            out += "{2:{1}}{0}{3:{1}}{0}{4:{1}}\n".format(sep, fmt, t, T, v)
        return out
    
    def read(self, f, sep=';'):
        t, T, v = [], []
        for line in f.readlines():
            if line.startswith('#'):
                continue
            line = line.strip('\n').split(sep)
            t.append(float(line[0]))
            T.append(float(line[1]))
            v.append(float(line[2]))
        self.t = np.array(t)
        self.T = np.array(T)
        self.v = np.array(v)
        
    def smooth(self, num, method='hanning'):
        v1 = csutil.smooth(self.v, num, method=method)
        ret = TimeTemperatureValue(self.t.copy(), self.T.copy(), v1)
        ret.update()
        return ret
        
    def derive(self, order=1, x='t', smooth=0, method='Middle'):
        v1 = self.v
        if smooth:
            v1 = csutil.smooth(self.v, smooth)
        if x is 't':
            x = self.t
        elif x is 'T':
            x = self.T
        if x is None:
            v1 = csutil.derive(v1, order=order, method=method)
        else:
            v1 = csutil.xyderive(x, v1, order=order, method=method)
        ret = TimeTemperatureValue(self.t.copy(), self.T.copy(), v1)
        ret.update()
        return ret
            

def load_dil_curve(path, version=-1, ptsdeg=1):
    crv = MdfFile(path, version=version)
    crv.set_version(version)
    # vertical or horizontal
    crv = getattr(crv.nb.summary, crv.instrument).sample0.d.curve(oversample=ptsdeg)
    return crv
    
    
def render_meta(obj):
    out = '<table>\n<tr><th>Name</th><th>Temperature</th><th>Time</th></tr>'
    keys = []
    for key, opt in obj.desc.items():
        if opt['type'] != 'Meta':
                continue
        keys.append(key)
    keys.sort()
    for key in keys:
        opt = obj.desc[key]
        m = opt['current']
        ok = True
        for k, v in m.items():
            if not isinstance(v, str):
                f = '{:.1f}'
                if k == 'time':
                    v /= 60.
                    f += ' min'
                elif k == 'temp':
                    f += ' °C'
                if k == 'value':
                    m[k] = '{:E}'.format(v)
                else:
                    m[k] = f.format(v)
            elif k == 'time' and v in ['None', None]:
                ok = False
        if not ok:
            continue
        out += '<tr><td>{}</td><td>{}</td><td>{}</td></tr>\n'.format(opt['name'], m['temp'], m['time'])
    out += '</table>\n'
    return out

            
class NodeAccessor(object):
    _node = False
    route = ['get_time']

    def __init__(self, sharedfile, path='/'):
        self._sh = sharedfile
        self._path = path
        self._local = ['_sh', '_path', '_local'] + dir(self)
        
    @property 
    def node(self):
        if self._node is False:
            self._node = self._sh.test.get_node(self._path)
        return self._node
        
    def __getattr__(self, subpath):
        if subpath.startswith('_') or subpath in self._local:
            return object.__getattribute__(self, subpath)
        if subpath not in self.node:
            return object.__getattribute__(self, subpath)
        if not self._path.endswith('/'):
            s = self._path + '/' + subpath
        else: 
            s = self._path + subpath
        return NodeAccessor(self._sh, s)
    
    def __call__(self):
        p = reference.get_node_reference(self._sh, self._path)
        return p
    
    def __getitem__(self, *a, **k):
        return self().__getitem__(*a, **k)

    def values_at_time(self, t, names):
        """Returns indexes and values for `names` at time `t`"""
        idxes = []
        vals = []
        for var in names:
            p1 = getattr(self, var)()
            idx = p1.get_time(t)
            val = p1[idx]
            idxes.append(idx)
            vals.append(val)
            
        return idxes, vals
    
    def value_at_time(self, t):
        val = self._sh.col_at_time(self._path, t)
        return val
        
    def get_profile(self, idx=None, T=None, t=None):
        if not 'profile' in self.node:
            print('This node has no profile dataset')
            return False
        p = reference.get_node_reference(self._sh, self._path + '/profile')
        if T is not None:
            idx, t, val = self._sh.nearest(self._path + '/T', T)
        if t is not None:
            idx = self._sh.get_time_func(self._path + '/profile', t,
                                         func=p.unbound['decode_time'])
        if idx is not None:
            dat = p[idx]
            return dat
        print('No profile found.')
        return False
        
    def draw_profile(self, idx=None, T=None, t=None):
        from matplotlib import pylab
        dat = self.get_profile(idx=idx, T=T, t=t)
        if not dat:
            return False
        t, ((h, w), x, y) = dat
        y -= min(y)
        y = max(y) - y
        x -= min(x)
        pylab.plot(x, y)
        m = max((max(x), max(y)))
        pylab.ylim((-50, m))
        pylab.xlim((-50, m))
        pylab.show()
        return True
    
    def curve(self, T1=None, T2=None, d0=0, percent=True, Tname=False, **kw):
        p = self._path
        if not p.startswith('/summary'):
            p = '/summary' + p
        v = self._sh.col(p)
        if p.endswith('/T'):
            return TimeTemperatureValue(v[:, 0], v[:, 1]).cut(T1, T2, **kw)
        if Tname:
            T = self._sh.col('/summary' + Tname)
        else:
            T = self._sh.col('/'.join(p.split('/')[:-1]) + '/T')
            # No further processing for temperature datasets
            if (T is None) or not len(T):
                T = self._sh.col('/summary/kiln/T')
        # Value times are the reference
        t = v[:, 0]
        v = v[:, 1]
        v -= v[0]
        # Temperature times might differ from value times
        # normalize T:
        try:
            fT = UnivariateSpline(T[:, 0], T[:, 1], s=0)
            T = fT(t)
        except ValueError:
            print('non-uniform T', T)
            # In case T is non-uniform, must express T in index-coordinates
            i = np.arange(len(T))
            it = UnivariateSpline(T[:, 0], i, s=0)
            iT = UnivariateSpline(i, T[:, 1], s=0)
            # Express value-times in index coordinates
            ti = it(t)
            # Now find corresponding T at those index coordinates
            T = iT(ti)
            print('new T', T)
            
        if not d0:
            try:
                d0 = self._sh.conf.instrument_obj.sample0['initialDimension']
            except:
                print_exc()
                d0 = 100
        if not d0:
            d0 = 100
        vp = self._sh.versioned(p)
        u = self._sh.get_node_attr(vp, 'unit')
        cu = self._sh.get_node_attr(vp, 'csunit')
        if not cu: cu = u
        print('CURVE unit', p, u, cu, d0, max(v), len(v))
        if percent and (cu != 'percent' and d0 > 1):
            print('CONVERT TO PERCENT', max(v), d0)
            v *= 100 / d0
        if not percent and (cu == 'percent' and d0 > 1):
            print('CONVERT TO MICRON', max(v), d0)
            v *= d0 / 100
        
        return TimeTemperatureValue(t, T, v).cut(T1, T2, **kw)


class MdfFile(SharedFile):

    def __init__(self, *a, **k):
        SharedFile.__init__(self, *a, **k)
        self.load_conf()
        self.nb = NodeAccessor(self)
        
    @property
    def name(self):
        return self.conf.instrument_obj.measure['name']
    
    @property
    def instrument(self):
        return self.conf.instrument_obj['devpath']
    
    @property
    def date(self):
        return self.conf.instrument_obj.measure['date']
    
    @property
    def elapsed(self):
        return self.conf.instrument_obj.measure['elapsed'] / 60.
    
    @property
    def cycle(self):
        return self.conf.kiln['curve']
    
    @property
    def zerotime(self):
        return self.conf.instrument_obj.measure['zerotime']

    def info(self):
        """Pretty-print main test info"""
        from IPython.display import display, HTML
        ins = self.conf.instrument_obj
        out = '<h1>Name: {}</h1>\n'.format(self.name)
        out += '<h3> Started: {}, Elapsed: {:.1f} min</h3>\n'.format(self.date, self.elapsed)
        out += '<h2> Measurement metadata </h2>\n'
        out += render_meta(ins.measure)
        out += '<h2>  Sample metadata for: {} </h2>\n'.format(ins.sample0['name'])
        out += render_meta(ins.sample0)
        display(HTML(out))

