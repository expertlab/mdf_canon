from mdf_canon.option import ao, mkheader
from mdf_canon.csutil import xmlrpcSanitize
from mdf_canon.logger import get_module_logging
from traceback import format_exc
import re
logging = get_module_logging(__name__)

conf = {}
ao(conf, 'opt', 'Section', 'Options', 'Dashboard')
# rule, position
i0 = range(50).__iter__().__next__
i0()
i = lambda: i0() * 1000

rule_opt_status = [['^/isRunning$', i(), 0],
                   ['^/kiln/analysis$', i(), 0],
                   ['^/kiln/gas/reactive$', i(), 0],
                   ['^/kiln/gas/enabledInternal$', i(), 0],
                   ['^/kiln/gas/enabledExternal$', i(), 0],
                   ['/measure/name$', i(), 0],
                   ['/sample\d/name$', i(), 0],
                   # ['^/(horizontal|vertical|flex|flex3)?/measure/calibration', i(), 0],
                   [r'^/(horizontal|vertical)?/sample\d/initialDimension$', i(), 0],
                   ['^/(flex|flex3)?/sample0/initialLength$', i(), 0],
                   ['^/(horizontal|vertical|flex|flex3)?/sample0/initialThickness$', i(), 0],
                   ['^/(horizontal|vertical|flex|flex3)?/sample0/initialDepth$', i(), 0],
                   ['^/(horizontal|vertical|flex|flex3)?/measure/calibration$', i(), 0],
                   # Test data
                   ['/measure/elapsed$', i(), 0],
                   ['^/kiln/remaining$', i(), 1],
                   ['^/kiln/T$', i(), 1],
                   ['^/kiln/S$', i(), 1],
                   ['^/kiln/P$', i(), 1],
                   ['^/kiln/Ts$', i(), 0],
                   ['^/kiln/Tr$', i(), 0],
                   ['^/kiln/Tk$', i(), 0],
                   ['^/kiln/Th$', i(), 0],
                   ['^/kiln/Te$', i(), 0],
                   ['^/kiln/gas/pressure$', i(), 0],
                   ['^/kiln/gas/purgeStatus$', i(), 0],
                   ['^/kiln/gas/vacuum$', i(), 0],
                   ['^/kiln/gas/gasFlow$', i(), 0],
                   ['^/kiln/gas/burnerTemperature$', i(), 0],
                   # Sample data
                   ['^/hsm/measure/nSamples$', i(), 0],
                   ['^/hsm/measure/shapes$', i(), 0],
                   ['^/hsm/sample\d/h$', i(), 0],
                   [r'^/(horizontal|vertical|flex|flex3)?/sample\d/d$', i(), 0],
                   ['^/horizontal/sample0/cte\d+$', i(), 0],
                   ['^/dta/sample0/deltaT$', i(), 0],
                   # Focus factors
                   [r'^/(horizontal|vertical|flex|flex3)?/sample\d(/|/Left/|/Right/|/Center/|/Base/|/Height/)?focus$', i(), 0],
                   # Calc length options
                   [r'^/(horizontal|vertical)?/sample\d/calcLength(Angle)?$', i(), 1],
                   [r'^/(horizontal|vertical)?/sample\d/calcLengthApply$', i(), 0],
                   ]

ao(conf, 'opt_status', 'Table', rule_opt_status, 'Status',
   header=mkheader(('option', 'String'),
                   ('position', 'Integer'),
                   ('force', 'Boolean')))

ao(conf, 'opt_tc', 'TextArea', '', 'Thermal cycle')

rule_opt_hide = """/(measure|sample\d)?/anerr$
/(measure|sample\d)?/running$
/(measure|sample\d)?/selfTest$
/measure/structuredName$
/sample\d/structuredName$"""
ao(conf, 'opt_hide', 'TextArea', rule_opt_hide, 'Hide always')


class RulesTable(object):

    """Helper object for matching a string in a list of rules."""

    def __init__(self, tab=[]):
        self.set_table(tab)

    def set_table(self, tab):
        self.rules = []
        self.rows = []
        self.lines = []
        self.tab = tab
        for row in tab:
            if len(row) <= 1:
                logging.debug('skipping malformed rule', row)
                continue
            if isinstance(row[0], tuple):
                # Skip header row
                continue
            r = row[0]
            if isinstance(r, list):
                # header
                continue
            # remove empty rows
            r = r.replace('\n\n', '\n')
            if r.endswith('\n'):
                r = r[:-1]
            if len(r) == 0:
                logging.debug('skipping empty rule', row)
                continue
            r = r.replace('\n', '|')
            self.lines.append(r)
            rule = re.compile(r)
            self.rules.append(rule)
            self.rows.append(row[1:])
            
    def query(self, s, latest=False):
        """Return the row corresponding to the first rule matching the string `s`.
        latest = 0, 1, 'all'"""
        f = False
        all = []
        alli = []
        for i, r in enumerate(self.rules):
            if r is False:
                continue
            if r.search(s):
                f = self.rows[i]
                if latest == 'all':
                    all.append(f)
                    alli.append(i)
                elif not latest:
                    return i, f
        # No match found, or all
        if latest == 'all':
            return alli, all
        return None, f

    def __call__(self, s, latest=False):
        return self.query(s, latest=latest)[-1]


def list_options(obj, check=False):
    fp = obj['fullpath']
    for o in obj.keys():
        if check:
            if not obj.dashboard_check(o):
                continue
        yield fp + o


def recursive_list_options(obj, check=False):
    for p in list_options(obj, check=check):
        yield p
    for obj in obj.devices:
        for p in recursive_list_options(obj):
            yield p


def filter_option(option_path, ruler, opts=None, nSamples=1):
    opts = opts if opts is not None else {'kid': set([])}
    h1 = option_path
    v = option_path.split('/')
    handle = v[-1]
    fp = '/'.join(v[:-1])
    pos = ruler.rule_opt_status(h1)
    # Skip if not included
    if not pos:
        return False
    # Skip if hidden
    if ruler.rule_opt_hide(h1):
        return False
    pos, force = pos
    if not pos:
        return False
    if h1 in opts['kid']:
        return False
    pos1 = '{:06}'.format(pos)
    while pos1 in opts:
        pos += 10
        pos1 = '{:06}'.format(pos)
    new_name = ""
    # Set sample number on option name
    if handle == 'name' and v[2].startswith('sample'):
        new_name = 'Sample name'
    for i, part in enumerate(v):
        if not part.startswith('sample'):
            continue
        n = []
        if nSamples > 1:
            n.append(part[6:])
        # Add sample part
        if len(v) - i > 2:
            n.append(v[i + 1])
        if n:
            new_name = '{}'
            new_name += ' ({})'.format(':'.join(n))
            
        break
    return pos1, fp, handle, force, new_name


def add_object_option(option_path, server, opts, ruler, nSamples=1):
    fo = filter_option(option_path, ruler, opts, nSamples)
    if not fo:
        return False
    pos1, fp, handle, force, new_name = fo
    remObj = server.toPath(fp)
    desc = dict(remObj.gete(handle))
    desc = xmlrpcSanitize(desc)
    if new_name:
        if '{}' in new_name:
            new_name = new_name.format(desc['name'])
        desc['name'] = new_name
    new = (fp, handle, force, desc)
    return {pos1: new}


containers = ['kiln', 'beholder', 'morla', 'thermal', 'board']


def create_dashboard(server, remObj, confdb):
    opts = {'kid': set()}  # position: (parent, handle)
    instrument_fp = remObj['fullpath']
    options = server.recursive_list_options()
    if not options:
        options = recursive_list_options(server)
    nSamples = 0 if 'nSamples' not in remObj else remObj['nSamples']
    for opt in options:
        v = opt.split('/')
        if (len(v) > 2) and v[0][1:] != 'kiln' and\
                (not opt.startswith(instrument_fp)) and \
                (v[2] in ('sample0', 'measure') or v[1] not in containers):
            # print('Skip foreign instrument path', opt, instrument_fp)
            continue
        up = add_object_option(opt, server, opts, confdb, nSamples)
        if not up:
            # print('Not adding', opt)
            continue
        # print('Adding', opt)
        opts.update(up)
        opts['kid'].add(opt)
    opts.pop('kid')
    return {k: list(v) for k, v in opts.items()}
