# -*- coding: utf-8 -*-
"""Basic Kalman filtering utility"""
import numpy as np
from mdf_canon.logger import get_module_logging
from functools import partial
logging = get_module_logging(__name__)


def iter_kalman_definitions(handle=''):
    d = dict(**KalmanFilter.options, **KalmanFilter.outputs)
    r = {}
    for opt, desc in d.items():
        newkey = opt if not handle else 'kalman_' + handle + '_' + opt
        kw = {'section': 'kalman', 'handle': newkey, 'type': 'Float',
              'name': handle + ': ' + opt.capitalize() if handle else opt.capitalize()}
        kw.update(desc)
        if handle and opt != 'devfactor':
            kw['parent'] = 'kalman_' + handle + '_devfactor'
        if opt in KalmanFilter.outputs:
            kw['attr'] = ['History', 'ReadOnly', 'Reset', 'Runtime']
        kw['current'] = getattr(KalmanFilter, opt) or 0
        r[newkey] = kw
    return r


class KalmanFilter(object):
    ############################
    # OPTIONS
    devfactor = -1
    duration = 15
    update = 1
    maxbuffer = 500
    minbuffer = 15
    sensorvar = 0
    discardev = 1
    stop = False
    options = {'devfactor': dict(name='Discard above stdevs'),
               'stop': {'type':'Boolean'},
               'duration': dict(name='Buffer duration', unit='second'),
               'update': dict(name='Update variance each', unit='second'),
               'maxbuffer': dict(name='Max buffer length', type='Integer', min=10, max=1000),
               'minbuffer': dict(name='Min buffer length', type='Integer', min=10, max=1000),
               'sensorvar': dict(name='Min sensor var'),
               'discardev': dict(name='Reset filter above stdevs'),
               }
    ############################
    # OUTPUTS
    blend = 1
    correction = 0
    unfiltered = 0
    sensor = 0
    variance = 0
    process = 0
    outputs = {'correction': {}, 'unfiltered': {}, 'variance': {},
               'sensor':{}, 'process':{}, 'blend':{}}
    ############################
    # INTERNALS
    sensorgain = 1  # sensor variance multiplier (experimental)
    processgain = 1  # sensor variance multiplier (experimental)
    mindev = 0  # um/s
    maxdev = np.inf  # um/s
    mingain = 0.025
    absdev = 2  # minimum absolute deviation for rejection
    degree = 1
    
    def __init__(self, log=logging, **kwargs):
        """Kalman filter utility class with jump remover"""
        self.log = log
        self.__dict__.update(kwargs)
        self.log = log
        self.forget()
        
    def update_outputs(self):
        out = {}
        if self.sensorvar:
            out.update({'unfiltered': {}, 'variance': {},
               'sensor':{}, 'process':{}, 'blend':{}})
        if self.devfactor:
            out.update({'correction': {}, 'unfiltered': {}})
        return out

    def forget(self):
        self.x = []
        self.vals = []
        self.rates = []
        self.devs = []
        self.drates = []
        self.results = []
        self.rejects = []
        self.corrections = []
        self.postkalman = []
        self.kalmanblend = []
        self.kalmandyna = []
        self.kalmanvar = []
        self.kalmanest = []
        self.last_varestimate_t = 0
        self.cumulative = 0
        self.iteration = 0
        self.discarded = 0
        
    def discard(self):
        self.cumulative = 0
        self.last_varestimate_t = self.x[-1][0]
        self.discarded = self.iteration
        self.updated = self.iteration
        
    @property
    def active(self):
        ret = (not self.stop) and self.maxbuffer > 3 and (self.devfactor >= 0 or self.sensorvar > 0)
        if not ret and len(self.x):
            self.forget()
        return ret
        
    def pop(self, i=0):
        p = 0
        for k in self.__dir__():
            L = getattr(self, k)
            if not isinstance(L, list):
                continue
            if len(L) > self.minbuffer:
                L.pop(i)
                p += 1
        return p
    
    def __len__(self):
        return len(self.results)
    
    @property
    def len(self):
        return len(self)
    
    def limit(self):
        i = 0
        while self.len > self.maxbuffer:
            if not self.pop():
                break
            i += 1
        if not self.x:
            return i
        # Enforce duration
        last = self.x[-1][0]
        while (last - self.x[0][0]) > self.duration and (self.len >= self.minbuffer):
            if not self.pop():
                break
            
            i += 1
        return i
    
    @property
    def full(self):
        return len(self.rates) >= self.minbuffer
            
    def filter_jump(self, x, v):
        """Purely time-based
        Warning: might cause problems when T inverts"""
        dr = 0
        r = 0
        dev = 0
        rj = 0
        c = 0
        nv = v + self.correction
        
        if self.results:
            dt = (x[0] - self.x[-1][0]) or 1e-6
            r = (v - self.vals[-1]) / dt
            dr = abs(r - self.rates[-1])
        
        if self.full and self.devfactor >= 0:
            dev = np.std(self.rates) or self.mindev
            if dev > self.maxdev:
                dev = self.maxdev
            if dev < self.mindev:
                dev = self.mindev
            # REJECT - revert tolast result
            if self.devfactor > 0 and dr > self.absdev and dr > self.devfactor * dev:
                rj = 1
                nv = self.results[-1]
                c = self.vals[-1] - v
                self.correction += c
                # Ignore a filtered rate
                r = abs(self.rates[-1])
                
        self.corrections.append(self.correction)
        self.rates.append(r)
        self.drates.append(dr)
        self.x.append(x)
        self.vals.append(v)
        self.devs.append(dev)
        self.rejects.append(rj)
        self.results.append(nv)
        self.unfiltered = nv
        return nv
    
    def __call__(self, *args):
        """Accepts list of variables, first one must be time, last one must be the controlled one. 
        Variables in the middle are used for variance correction.
        Eg: x, v
        t, T, v
        etc..."""
        if len(args) == 2:
            x = [args[0]]
        else:
            x = args[:-1]
        v = args[-1]
        if self.results and (x[0] < self.x[-1][0]):
            self.log.debug('Filtering a point in the past', x, self.x)
            return v
        nv0 = self.filter_jump(x, v)
        nv = self.filter_kalman(x, nv0)
        self.limit()
        self.iteration += 1
        return nv
    
    def filter_kalman(self, x, measurement):
        if not (self.sensorgain and self.full and self.sensorvar):
            return measurement
        self.update_variance()
        posterior = self.predict(x)
        # Run the filter
        filtered, new_variance, self.blend = update(posterior, self.variance, measurement,
                                               self.sensor, self.process, self.mingain)
        # self.variance = new_variance
        self.cumulative += abs(filtered - measurement)
        self.kalmanvar.append(self.variance)
        self.kalmanblend.append(self.blend)
        self.postkalman.append(filtered)
        self.kalmandyna.append(self.process)
        self.kalmanest.append(self.sensor)
        return filtered
    
    def predict(self, x):
        if len(self.postkalman) < 2:
            return self.results[-2]
        measurement = self.results[-1]
        pk = self.postkalman[-1]
        if self.iteration > self.minbuffer and (self.iteration - self.discarded) < self.minbuffer:
            return measurement
        std = self.discardev * max(self.sensor / self.sensorgain,
                                 self.process / self.processgain,
                                 self.variance,
                                 self.devs[-1]) ** 0.5
        if std <= 0:
            std = np.inf
            
        n = (self.iteration - self.updated) + 2
        delta = abs(pk - measurement)
        cumul = (delta + self.cumulative) / n
        if delta > std:
            if cumul > std:
                self.discard()
        return pk
    
    def should_update_variance(self):
        if not self.sensor:
            return True
        if self.updated <= 0:
            return True
        if not self.x:
            return False
        dt = self.x[-1][0] - self.last_varestimate_t
        return  dt > self.update
    
    updated = 0

    def update_variance(self):
        n = self.len - 1
        j = self.iteration - self.discarded
        n = max(self.minbuffer, min(n, j))
        if n > self.minbuffer:
            if not self.should_update_variance():
                return True
        
        y = np.array(self.results[-n:-1])
        x = np.array(self.x[-n:-1])[:, 0]
        dt = (x[-1] - x[0])
        p = np.polyfit(x, y, self.degree)
        predictor = partial(np.polyval, p)
        self.predictor = predictor
        # Measurement variance
        if not self.variance or 1:
            self.variance = y.var() / dt
        # Linear process
        yfit = predictor(x)
        # Variance caused by the linear process
        process_variance = yfit.var() / dt
        # Variance caused by noise around the linear process
        sensor_variance = self.sensorgain * abs((yfit - y)).mean()
        self.sensor = max(self.sensorvar, sensor_variance)
        self.process = self.processgain * process_variance
        self.last_varestimate_t = self.x[-1][0]
        self.updated = self.iteration
        self.cumulative = 0
        return True


def update(prior, variance, measurement, sensor_variance, process_variance, mingain=0.05):
    """prior - prior value
    variance - prior variance, estimated from buffer
    measurement - new value
    sensor_variance, R - measurement variance, decreases the blending
    process_variance, Q - dynamic process autovariance, to be added to variance
    """
    gain = (variance + process_variance) / (variance + sensor_variance)
    # gain = process_variance/sensor_variance
    if gain > 1: gain = 1
    elif gain < mingain: gain = mingain
    new = prior + gain * (measurement - prior)
    new_variance = (1 - gain) * variance
    return [new, new_variance, gain]

