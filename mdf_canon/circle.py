#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np

det = lambda m: np.linalg.det(np.matrix(m))


def circle(a, b, c):
    """Calculate circle radius given three points (xa,ya),(xb,yb),(xc,yc)"""
    # following http://mathworld.wolfram.com/Circle.html
    A = det([[a[0], a[1], 1],
             [b[0], b[1], 1],
             [c[0], c[1], 1]])
    if abs(A) < 1e-12:
        return 0, 0, 0
    sa = a[0] ** 2 + a[1] ** 2
    sb = b[0] ** 2 + b[1] ** 2
    sc = c[0] ** 2 + c[1] ** 2
    D = -det([[sa, a[1], 1],
              [sb, b[1], 1],
              [sc, c[1], 1]])
    E = det([[sa, a[0], 1],
             [sb, b[0], 1],
             [sc, c[0], 1]])
    F = -det([[sa, a[0], a[1]],
              [sb, b[0], b[1]],
              [sc, c[0], c[1]]])
    x = -D / (2 * A)
    y = -E / (2 * A)
    G = (D ** 2 + E ** 2) / (4 * (A ** 2))
    r = np.sqrt(G - (F / A))
    if r > 1e12:
        return 0, 0, 0
    return float(r), float(x), float(y)


def absolute_deformation(middle_delta, right_delta, left_delta,
                  right_optics_position, left_optics_position, chord_length=0):
    left_point = [-left_optics_position  , left_delta]
    center_point = [0                      , middle_delta]
    right_point = [right_optics_position  , right_delta]
    radius, x_center, y_center = circle(left_point, center_point, right_point)
    if radius == 0:
        d = middle_delta - (right_delta + left_delta) / 2
        return d, 0
    # Calculate the theoretical point of maximum flexion (which may not be equal to d)
    # Length of the chord is the sum of R/L distances from middle cam
    if not chord_length:
        chord_length = (right_optics_position + left_optics_position)
    q = chord_length / 2.
    # Angle associated with half-chord
    ang = np.arcsin(q / radius)
    # Maximum circle-chord distance
    d1 = radius - radius * np.cos(ang)  # radius - radius*cos
    # Apply the sign of the center y relative to the middle y
    s = -np.sign(y_center - middle_delta)
    d = s * abs(d1)
    if 0:
        from matplotlib import pylab
        o = dict(marker="o", markersize=20)
        pylab.plot(*left_point, color='blue', **o)
        pylab.plot(*right_point, color='red', **o)
        pylab.plot(*center_point, color='black', **o)
        # pylab.plot(x_center, y_center)
        pylab.plot(-q, 0, color='cyan', **o)
        pylab.plot(+q, 0, color='magenta', **o)
        pylab.plot(0, d, color='gray', **o)
        vx = np.arange(-q,+q)
        print(vx)
        y = np.sqrt(radius**2 - (vx-x_center)**2)
        y += s*y_center
        print(y)
        pylab.plot(vx, y)
        pylab.ylim(-q, +q)
        print('r',s*radius, d)
        pylab.show()
    
    return s * radius, d


def absolute_flex(middle_delta, right_delta, left_delta,
                  right_optics_position, left_optics_position, chord_length=0):
    radius, d = absolute_deformation(middle_delta, right_delta, left_delta,
                  right_optics_position, left_optics_position, chord_length=chord_length)
    return {'radius':float(radius), 'd':float(d)}
    

def syn(a, b, c, step=1):
    """Create a circle (x,y) sequence passing through a,b,c"""
    # Find circle radius
    r, Ox, Oy = circle(a, b, c)
    sg = np.sign(step)
    y = a[1]
    vr = []
    vl = []
    vxr = []
    vxl = []
    vy = []
    while sg * (b[1] - y) >= 0:
        y1 = sg * (Oy - y)
        x1 = r * np.cos(np.arcsin(y1 / r))
        vxr.append(Ox - x1)
        vxl.append(Ox + x1)
        vy.append(y)
        vl.append((vxl[-1], y))
        vr.append((vxr[-1], y))
        y += step
    y = vy + vy[::-1]
    if sg > 0:
        x = vxl + vxr[::-1]
        v = vl + vr[::-1]
    else:
        x = vxr + vxl[::-1]
        v = vr + vl[::-1]
    print(v)
    return x, y, v


if __name__ == '__main__':
   # import pylab
# 	x,y,v=syn((-1,0),(0,1),(1,0),0.01)
    # x, y, v = syn([33., 66.], [49., 33.], [66., 66.], -1)
    # pylab.plot(x, y, 'r-')
    # pylab.show()
    r = absolute_flex(1, 50, -50, 250, 250, 720)
    print(r)
    
    r = absolute_flex(100, 100, 100, 25000, 25000, 72000)
    print(r)
    r = absolute_flex(100, 50, 30, 25000, 25000, 72000)
    print(r)
    r = absolute_flex(-100, -50, -30, 25000, 25000, 72000)
    print(r)
    r = absolute_flex(100, -50, -50, 25000, 25000, 72000)
    print(r)
    r = absolute_flex(100, 51.6, 51.6, 25000, 25000, 72000)
    print(r)
    r = absolute_flex(200, 151.6, 151.6, 25000, 25000, 72000)
    print(r)
    r = absolute_flex(110.00, 105.16, 105.16, 36000, 36000, 72000)
    print(r)
