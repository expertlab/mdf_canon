#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import unittest
from time import sleep
from mdf_canon import mod_license

skip_test = False
try:
    from mdf_server import license
    from mdf_crystal.keys import private_key
except:
    skip_test = True


def setUpModule():
    print(('Starting ', __name__))


@unittest.skipIf(skip_test, "Missing mdf_Server or license key file")
class TestModLicense(unittest.TestCase):
    """Test utilities functions"""

    def test_calculate_clientid_linux(self):
        if os.name == 'nt':
            return
        r = mod_license.calculate_clientid_linux()
        print(r)

    def test_calculate_clientid_windows(self):
        if os.name != 'nt':
            return
        r = mod_license.calculate_clientid_windows()
        print(r)

    def test_decrypt_identifier(self):
        msg = 'Ciao\nBla\nBla1'
        edent = mod_license.encrypt_identifier(msg)
        s, ident1, h1 = mod_license.decrypt_identifier(private_key, edent)
        self.assertEqual(s, msg)
        self.assertEqual(ident1, 'Ciao')

    def test_generate_license(self):
        obj = license.License()
        ident = mod_license.calculate_identifier(obj)[1]
        ident1 = mod_license.calculate_identifier(obj, date=False)[1]
        edent = mod_license.encrypt_identifier(ident)
        from mdf_cloud.gen_license import generate_license
        lic, msg0, sig0 = generate_license(private_key, edent, expiry_date=u'02/02/2020')
        t, valid, funcs, rates, msg1, sig1 = mod_license.verify_license(lic, ident1)
        self.assertEqual(sig0, sig1)
        self.assertEqual(msg0, msg1)
        self.assertTrue(valid)

        lic, msg0b, sig0b = generate_license(private_key, decrypted_identifier=ident1, expiry_date=u'02/02/2020')
        t, valid, funcs, rates, msg1b, sig1b = mod_license.verify_license(lic, ident1)
        self.assertEqual(sig0b, sig1b)
        self.assertEqual(msg0b, msg1b)
        self.assertTrue(valid)

        self.assertEqual(sig0, sig0b)
        self.assertEqual(msg0, msg0b)

    def test_calculate_identifier(self):
        obj = license.License()
        print((mod_license.calculate_identifier(obj)[1]))

    def test_encrypt_identifier(self):
        obj = license.License()
        ident1 = mod_license.calculate_identifier(obj)[1]
        edent1 = mod_license.encrypt_identifier(ident1)

        s1, s1_nodate, h1 = mod_license.decrypt_identifier(private_key, edent1)
        self.assertEqual(ident1, s1)

        sleep(1)
        ident2 = mod_license.calculate_identifier(obj)[1]
        self.assertEqual(ident1, ident2)

        edent2 = mod_license.encrypt_identifier(ident2)

        s2, s2_nodate, h2 = mod_license.decrypt_identifier(private_key, edent2)
        self.assertEqual(ident2, s2)
        self.assertEqual(s1, s2)
        # self.assertEqual(edent1, edent2)


if __name__ == "__main__":
    unittest.main()
