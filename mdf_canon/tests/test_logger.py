#!/usr/bin/python
# -*- coding: utf-8 -*-
from time import sleep
import unittest
from mdf_canon import logger


class TestBaseLogger(unittest.TestCase):

    def test_debounce(self):
        bl = logger.BaseLogger()
        bl.debug('A')
        self.assertFalse(bl._debounce)
        bl.debug('A', debounce=1)
        self.assertIn('A', bl._debounce)
        oldt, oldc = bl._debounce['A']
        self.assertGreater(oldt, 0)
        bl.debug('A', debounce=1)
        oldt, oldc = bl._debounce['A']
        self.assertEqual(oldc, 1)
        bl.debug('A', debounce=1)
        oldt, oldc = bl._debounce['A']
        self.assertEqual(oldc, 2)
        # Resume print
        sleep(1.1)
        bl.debug('A', debounce=1)
        oldt, oldc = bl._debounce['A']
        self.assertEqual(oldc, 0)

        # Escalate
        sleep(0.1)
        bl.debug('A', escalate=1)
        level = bl._last_msg[-1][1]['p']
        self.assertGreater(level, 18)
        

if __name__ == "__main__":
    unittest.main(verbosity=2)
