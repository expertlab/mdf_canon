#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
import numpy as np
from mdf_canon import kalman


class TestKalmanFilter(unittest.TestCase):

    def test(self):
        k = kalman.KalmanFilter(devfactor=1, duration=100, sensorvar=0.25, maxbuffer=50)
        for t in range(100):
            v = k(t, t)
            self.assertEqual(k.unfiltered, t)
            self.assertEqual(k.correction, 0)
        # Avoid a jump
        t += 1
        self.assertEqual(k(t, v + 100), v)
        self.assertEqual(k.unfiltered, v)
        self.assertEqual(k.correction, -100)
        
        # Filter is meaning out
        k = kalman.KalmanFilter(devfactor=1, duration=100, sensorvar=1, maxbuffer=50)
        
        noise = 1
        # t+=1
        v = 100
        v0 = v
        # v = k(t, v)
        k.devfactor = -1
        k.buffer = 50
        un = []
        fl = []
        x = np.arange(1, 200)
        
        for t in x:
            # t += j
            u = v + np.random.rand() * 2 * noise - noise
            v1 = k(t, u)
            un.append(u)
            fl.append(v1)
            self.assertEqual(k.correction, 0)
        m = np.mean(un[:50])
        self.assertAlmostEqual(fl[49], m, delta=0.5 * noise)
        self.assertAlmostEqual(v1, 100, delta=0.5 * noise)
        if False:
            from matplotlib import pylab as plt
            plt.plot(x, un)
            plt.plot(x, fl)
            plt.axhline(m)
            plt.show()

        
if __name__ == "__main__":
    unittest.main(verbosity=2)
