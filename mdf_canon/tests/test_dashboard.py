#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
from mdf_canon import dashboard as db


class DummyRuler(object):

    def __init__(self):
        self.rule_opt_status = db.RulesTable(db.rule_opt_status)
        self.rule_opt_hide = db.RulesTable(db.rule_opt_hide)


class TestDashboard(unittest.TestCase):

    def test_filter_option(self):
        ruler = DummyRuler()
        r = db.filter_option('/horizontal/sample0/focus', ruler)
        self.assertEqual(r[-1], '')
        r = db.filter_option('/horizontal/sample0/Right/focus', ruler)
        self.assertEqual(r[-1], '{} (Right)')
        r = db.filter_option('/horizontal/sample0/Left/focus', ruler)
        self.assertEqual(r[-1], '{} (Left)')
        
        r = db.filter_option('/horizontal/sample0/focus', ruler, nSamples=2)
        self.assertEqual(r[-1], '{} (0)')
        r = db.filter_option('/horizontal/sample0/Right/focus', ruler, nSamples=2)
        self.assertEqual(r[-1], '{} (0:Right)')
        r = db.filter_option('/horizontal/sample0/Left/focus', ruler, nSamples=2)
        self.assertEqual(r[-1], '{} (0:Left)')

        r = db.filter_option('/horizontal/sample1/focus', ruler, nSamples=2)
        self.assertEqual(r[-1], '{} (1)')
        r = db.filter_option('/horizontal/sample1/Right/focus', ruler, nSamples=2)
        self.assertEqual(r[-1], '{} (1:Right)')
        r = db.filter_option('/horizontal/sample1/Left/focus', ruler, nSamples=2)
        self.assertEqual(r[-1], '{} (1:Left)')

        
if __name__ == "__main__":
    unittest.main(verbosity=2)
