#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
from mdf_canon import crystal


def setUpModule():
    print(('Starting ', __name__))
    
class TestModLicense(unittest.TestCase):
    """Test utilities functions"""
    maxDiff = None
    def test_reconfigure(self):
        f = open('test.xml','w')
        f.write('foo')
        f.close()
        r = crystal.reconfigure('test.xml', 'LOCALDEVICEID', 'LocalNick', 'REMOTEDEVICEID', 'RemoteNick', './target/bla/test_bla')
        self.assertTrue(r)
        xml1 = open('test.xml','r').read()
        
        xml1b = open('test.xml.bak','r').read()
        self.assertEqual(xml1b, 'foo')
        
        r = crystal.reconfigure('test.xml', 'LOCALDEVICEID', 'LocalNick', 'REMOTEDEVICEID', 'RemoteNick', './target/bla/test_bla')
        xml2 = open('test.xml','r').read()
        
        self.assertEqual(xml1, xml2)
        
        
        self.assertEqual(r, crystal.CONF_OK)
    
if __name__ == "__main__":
    unittest.main()