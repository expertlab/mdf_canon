#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import unittest

import tempfile
import tables
import numpy as np
from pickle import loads
from scipy import interpolate
from mdf_canon import indexer
from mdf_canon.csutil import flatten as flat
from mdf_canon import reference
from mdf_canon import determine_path


class ReferenceFunctions(unittest.TestCase):

    """Tests unbound functions in the reference module"""

    def test_binary_cast(self):
        f = 1.23456789
        b = reference.binary_cast([f], 'd', 'bbbbbbbb')
        self.assertEqual(len(b), 8)
        f1 = reference.binary_cast(b, 'bbbbbbbb', 'd')[0]
        self.assertEqual(f, f1)
        
    def test_accumulate_coords(self):
        x = [1, 2, 2, 3, 4]
        y = [10, 9, 8, 9, 10]
        acc = reference.accumulate_coords(x, y)
        self.assertEqual(list(acc), [33, 80])
    
    def test_decumulate_coords(self):
        x, y = reference.decumulate_coords(1, 10, np.array([33, 80]))
        self.assertEqual(list(x), [1, 2, 2, 3, 4])
        self.assertEqual(list(y), [10, 9, 8, 9, 10])
        
    def test_accumulate_decumulate_coords(self):
        x = [301, 302, 302, 302, 303, 302, 303, 303, 302, 303]
        y = [299, 298, 297, 298, 298, 297, 296, 297, 296, 296]
        acc = reference.accumulate_coords(x, y)
        x1, y1 = reference.decumulate_coords(x[0], y[0], acc)
        self.assertEqual(list(x1), x)
        self.assertEqual(list(y1), y)


class Reference(unittest.TestCase):

    """Tests unbound functions in the reference module"""

    def setUp(self):
        self.ref = reference.Reference(
            False, '', {'name': 'Test', 'handle': 'test'})


def mkfile():
    path = tempfile.mktemp()
    f = tables.open_file(path, mode='w')
    f.close()
    return indexer.SharedFile(path)


class OutFile(unittest.TestCase):

    """HDF File management utility TestCase"""
    outfile = None
    refClass = None
    _outfile = None
    keep = False
    __test__ = False  # nosetests will ignore this
    ref = None
    opt = None
    dt = 0
    t0 = 0

    @classmethod
    def new_opt(cls):
        return {'handle': 'test', 'name': 'Test',
        'unit': 'dummy', 'format': 'm4',
        't0': cls.t0, 'dt': cls.dt}

    def rand(self, *a, **k):
        """Reimplement in specific files to obtain random data"""
        assert False

    def mkfile(self):
        self.outfile = mkfile()
        self.path = self.outfile.get_path()
        destfile = mkfile()  # destination file for copy operations
        self.destpath = destfile.get_path()
        destfile.close()
        self.opt = self.new_opt()
        ref = self.refClass(self.outfile, '/', self.opt)
        # Check attributes
        self.opt['_reference_class'] = ref.__class__.__name__
        attr = ref.get_attributes()
        self.opt['last_edit_time'] = attr['last_edit_time']
        self.assertEqual(self.opt, attr)
        self.ref = ref
        return ref

    def tearDown(self):
        if self.outfile is None:
            return
        self.outfile.close()
        if not self.keep:
            os.remove(self.path)
            os.remove(self.destpath)

    def check_decode(self, encoded, decoded):
        dec = self.refClass.decode(encoded)
        self.assertEqual(flat(dec), flat(decoded))

    def test_encode_decode(self):
        if self.refClass is None:
            raise unittest.SkipTest('')
        tdata = self.rand(1)
        out = self.refClass.encode(tdata)
        self.assertFalse(out is None)
        self.check_decode(out, tdata)
    
    def check_data(self, data, returned_data):
        self.assertSequenceEqual(flat(data), flat(returned_data))
    
    def commit_data(self, ref, start=1, end=10, step=1):
        data = []
        n = int(round((end - start) / step, 0)) + 1
        for t in np.linspace(start, end, n):
            data.append(self.rand(t))
        self.assertTrue(ref.commit(data))
        rdata = ref[-n:]
        self.check_data(data, rdata)
        return data, rdata
        
    def test_commit(self):
        if self.refClass is None:
            raise unittest.SkipTest('')
        ref = self.mkfile()
        self.commit_data(ref, 1, 10)
        self.commit_data(ref, 11, 20)
        data3 = ref[:]
        # Check that data is not committed more than one time
        self.assertEqual(len(data3), 20)
        self.assertEqual(len(ref), 20)


class Array(OutFile):
    refClass = reference.Array
    __test__ = True
    noise = 4

    @classmethod
    def rand(cls, t, noise=None, sinoise=5):
        if noise is None:
            noise = cls.noise
        y = t + np.sin(t / 3) * 10 + np.sin(t * 3) * sinoise
        # add noise
        y += np.random.random() * noise - noise / 2
        return [t, y]
    
    def test_interpolation_range(self):
        ref = self.mkfile()
        self.assertTrue(ref.with_summary)
        self.assertFalse(ref.interpolation_range())
        self.assertFalse(ref.summary)
        self.commit_data(ref, 1, 1.5, 0.1)
        self.assertFalse(ref.interpolation_range())
        self.assertFalse(ref.summary)
        self.commit_data(ref, 1.6, 5.1, 0.1)
        r = ref.interpolation_range()
        # Has created the summary reference
        self.assertIsInstance(ref.summary, self.refClass)
        # But still small dt for commit (must be >3)
        self.assertFalse(r)
        # Commit 2 more seconds
        self.commit_data(ref, 5.2, 7.2, 0.1)
        r = ref.interpolation_range()
        # Check that keeps margin 3
        self.assertEqual(list(r), [2, 3, 4])
        self.commit_data(ref, 7.3, 8.2, 0.1)
        r = ref.interpolation_range()
        self.assertEqual(list(r), [2, 3, 4, 5])
        
    def test_interpolate(self):
        ref = self.mkfile()
        self.noise = 2
        end = 50
        chunk = 0.5
        step = 0.1
        data = []
        out = []
        istep = 1
        imargin = 3
        for start in np.arange(-step, end + chunk, chunk):
            data += self.commit_data(ref, start + step , start + chunk, step)[0]
            new = ref.interpolate(step=istep, margin=imargin, kind=3)
            if new is not None:
                out += [[e[0], e[1]] for e in new]
        # self.assertEqual(len(out), end - 2)
        # self.assertEqual(out[0][0], 1)
        # self.assertEqual(out[-1][0], end - 2)
        self.assertTrue(len(out) > 5)
        data = np.array(data)
        out = np.array(out)
        t1 = out[:, 0]
        t = data[:, 0]
        process = np.array([self.rand(it, 0, 0)[1] for it in t])
        fp = interpolate.UnivariateSpline(t, process, s=0, k=1)
        original = fp(t1)
        error = original - out[:, 1]
        self.assertLess(abs(error).mean(), 0.5)
        if 0:
            from matplotlib import pylab as plt
            plt.plot(t, data[:, 1], label='data')
            plt.plot(t, process, label='process')
            plt.plot(t1, out[:, 1], '.', label='out')
            plt.plot(t1, original, '.', label='original')
            plt.plot(t1, error, '.', label='error {:.3E}'.format(abs(error).mean()))
            plt.legend()
            plt.show()


class FixedTimeArray(OutFile):
    refClass = reference.FixedTimeArray
    __test__ = True
    t0 = 0
    dt = 1

    @classmethod
    def rand(cls, t):
        return [(t * 10.,)]
    
    def check_data(self, data, returned_data):
        """Override to ignore timecol"""
        self.assertSequenceEqual(flat(data), flat(returned_data)[1::2])


class VariableLength(object):

    def test_compress(self):
        data = np.random.random(10, 10)
        dataz = reference.VariableLength.compress(data)
        data1 = reference.VariableLength.decompress(dataz)
        self.assertEqual(flat(data), flat(data1))


class Rect(OutFile):
    __test__ = True
    refClass = reference.Rect

    @classmethod
    def rand(cls, t):
        b = t * 10
        return [t, [b + 1, b + 2, b + 3, b + 4]]


class Meta(OutFile):
    refClass = reference.Meta

    @classmethod
    def rand(cls, t):
        b = t * 10
        return [t, {'value': b + 1, 'time': b + 2, 'temp': b + 3}]


class Image(OutFile):
    __test__ = True
    refClass = reference.Image

    @classmethod
    def rand(self, t=-1):
        """Produce random data"""
        if t < 0:
            t = np.random.random() * 1000
        img = np.random.random((480, 640)) * 2
        # img = np.array([[97, 98], [99, 100]])
        img = img.astype('i8')
        img *= 255
        return t, img

    def check_decode(self, encoded, original):
        t, img = original
        t1, img1 = self.refClass.decode(encoded)
        self.assertEqual(t, t1)
        self.assertTrue((img.flatten() == img1.flatten()).all())

    def test_encode(self):
        m = np.array([[0, 1, 0],
                      [0, 0, 1]])
        m *= 255
        h, w = m.shape
# 		# Theoretical compressed array
# 		mcp=[0,0,0,0,w,h,-1,1,-3,1]
# 		cp=self.refClass.encode((0,m))
# 		self.assertEqual(list(cp),mcp)


class Profile(OutFile):
    __test__ = True
    refClass = reference.Profile

    @classmethod
    def rand(cls, t=-1):
        """Produce random profile-like data"""
        if t < 0:
            t = np.random.random() * 1000
        w = 640
        h = 480
        x = np.random.random(10) * w
        y = np.random.random(10) * h
        return t, ((w, h), x.astype('i8'), y.astype('i8'))

    def check_decode(self, encoded, decoded):
        t, ((w, h), x, y) = decoded
        t1, ((w1, h1), x1, y1) = self.refClass.decode(encoded)
        self.assertEqual(t, t1)
        self.assertEqual(w, w1)
        self.assertEqual(h, h1)
        self.assertEqual(list(x.flatten()), list(x1.flatten()))
        self.assertEqual(list(y.flatten()), list(y1.flatten()))


class CumulativeProfile(Profile):
    __test__ = False
    refClass = reference.CumulativeProfile

    @classmethod
    def rand(cls, t=-1):
        if t < 0:
            t = np.random.random() * 1000
        w = 640
        h = 480

        def coord(n):
            c = np.random.random(n) - 0.5
            c = np.sign(c) * (np.abs(c) > 0.16)
            return c

        x = coord(10)
        y = coord(10)
        # If both are 0, increase x
        x += (x == 0) * (y == 0)
        x = 300 + np.cumsum(x)
        y = 300 + np.cumsum(y)
        return t, ((w, h), x.astype('int16'), y.astype('int16'))
    
    def test_real_shape(self):
        dat = os.path.join(determine_path(__file__), 'cumulative.dat')
        dat = open(dat, 'rb').read()
        res, x, y = loads(dat, encoding="latin1")
        dx, dy = reference.explode_jumps(x, y)
        d = (dx + 1) * 3 + (dy + 1)
        g = reference.couple(d, 20)
        d1 = reference.decouple(g, 20)
        g2 = reference.couple(d1, 20)
        self.assertEqual((abs(g - g2)).sum(), 0)
        self.assertEqual((abs(d - d1)).sum(), 0)
        
        acc = reference.accumulate_coords(x, y)
        x1, y1 = reference.decumulate_coords(x[0], y[0], acc)
        self.assertEqual(x[-1], x1[-1])
        self.assertEqual(y[-1], y1[-1])


class Binary(OutFile):
    __test__ = True
    refClass = reference.Binary

    @classmethod
    def rand(self, t=-1):
        """Produce random binary blob"""
        if t < 0:
            t = np.random.random() * 1000
        idat = np.random.random(1000) * 255
        idat = idat.astype(np.uint8)
        # idat = [97, 98, 99]  # abc
        data = bytes(idat).decode('latin-1')
        return t, data

    def check_decode(self, encoded, original):
        t, dat = original
        t1, dat1 = self.refClass.decode(encoded)
        dat1 = dat1.decode('latin-1')
        self.assertEqual(t, t1)
        self.assertEqual(dat, dat1)
        
    def check_data(self, data, returned_data):
        self.assertEqual(len(data), len(returned_data))
        for dat, rdat in zip(data, returned_data):
            t1, data1 = rdat
            data1 = ''.join([chr(d) for d in data1])
            self.assertEqual(dat[0], t1)
            self.assertEqual(dat[1], data1)


class Log(OutFile):
    __test__ = True
    refClass = reference.Log

    @classmethod
    def rand(cls, t):
        return [1. * t, [10, b'message%i' % t]]


if __name__ == "__main__":
    unittest.main()
