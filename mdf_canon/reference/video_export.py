#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Video export tools"""
from mdf_canon.option import ao, ConfigurationProxy
from copy import deepcopy
from mdf_canon.logger import get_module_logging
import numpy as np
from mdf_canon import csutil, reference, indexer
import platform
from mdf_canon.reference.iterate_images import iterate_images
from datetime import timedelta
logging = get_module_logging(__name__)
try:
    import cv2 as cv
except:
    logging.debug('OpenCV is not available. Video Export is disabled.')
    cv = False
    
if 'Linux' in platform.platform() and cv:
    default_fourcc = cv.VideoWriter_fourcc('M', '4', 'S', '2')
else:
    default_fourcc = -1


def center_profile(x, y, w=0, h=0, margin=(20, 20, 20, 20), pen_width=0, contour_only=False):
    print('center_profile', w, h, margin, pen_width, contour_only)
    if contour_only and pen_width <= 0:
        h = max(y) - min(y)
        w = max(x) - min(y)
        pen_width = int((0.01) * np.sqrt(h ** 2 + w ** 2))
        pen_width = max(pen_width, 10)
    else:
        pen_width = 3
    m_up, m_btm, m_left, m_right = margin
    x_translate = -int(min(x)) + m_left + pen_width
    # Autodetect w,h
    if 0 in (w, h) or (1280, 960) == (w, h):
        w = max(x) + m_right + pen_width + x_translate
        h = max(y) - min(y) + m_btm + m_up + pen_width
    else:
        w += m_left + m_right
        h += m_up + m_btm + pen_width
    # Ensure visibility
    w = max(max(x) - min(x) + m_right + m_left, w)
    h = max(max(y) - min(y) + m_up + m_btm + pen_width, h)
    # Ensure virtual base thickness
    y_translate = +(h - (max(y) + m_btm + pen_width))
    return pen_width, x_translate, y_translate, w, h


def export(sh, frame=['/hsm/sample0/frame'],
           T='/kiln/T',
           output='output.avi',
           framerate=50.0,
           fourcc=default_fourcc,
           prog=False,
           acquisition_start_temperature=20,
           Tstep=0,
           tstep=0,
           centerx=False,
           centery=False):
    """Base video export function"""
    if cv is False:
        logging.debug('No OpenCV library!')
        return False
    if not output.lower().endswith('.avi'):
        output += '.avi'
    if isinstance(sh, str):
        sh = indexer.SharedFile(sh, mode='r')
    frame = list([sh.versioned(f) for f in frame])
    frame = frame[0]
    if T.startswith('/ver_'):
        T = '/'.join([''] + T.split('/')[2:])
    T = sh.versioned(T)
    nT = sh.col(T)
    tT = nT[:, 0]
    vT = nT[:, 1]
    ref = reference.get_node_reference(sh, frame)
    N = sh.len(frame)
    # If dynamic roi is found
    roi = frame.split('/')[:-1]
    roi.append('roi')
    roi = '/'.join(roi)
    if sh.has_node(roi):
        # FIXME: why not get_node??
        roi = sh.col(roi, raw=True)
        if hasattr(roi, 'cols'):
            x = roi.cols.x
            y = roi.cols.y
            w = roi.cols.w
            h = roi.cols.h
        else:
            roi = np.array([[r[1], r[2], r[3], r[4]] for r in roi]).T
            x, y, w, h = roi[0], roi[1], roi[2], roi[3]
        # Translate to 0
        x_translation = -min(x)
        y_translation = -min(y)
        x += x_translation
        y += y_translation
        # Max dimension (video resolution)
        wMax = int(max(x + w))
        hMax = int(max(y + h))
    elif isinstance(ref, reference.Binary):
        t, img = ref[0]
        im = cv.imdecode(np.frombuffer(img, dtype='uint8'), cv.IMREAD_UNCHANGED)
        print(im, dir(im))
        hMax, wMax, channels = im.shape
    else:
        # No dynamic roi - take first frame
        t, img = ref[0]
        ((w, h), x, y) = img
        r = center_profile(x, y, w, h, margin=(50, 50, 50, 50))
        pw, x_translation, y_translation, wMax, hMax = r
    
    # Ensure even dimensions for broad codec compatibility
    wMax += wMax % 2
    hMax += hMax % 2
    logging.debug('Max resolution', wMax, hMax, 'translations', x_translation, y_translation)
    index_acquisition_T = csutil.find_nearest_val(vT, acquisition_start_temperature, seed=0)
    acquisition_start_temperature = vT[index_acquisition_T]
    i = i0 = csutil.find_nearest_val(ref,
                                     tT[index_acquisition_T],
                                     seed=0,
                                     get=lambda i: ref[i][0])
    ti = ref[i][0]
    if prog:
        prog.setMaximum(2 * (N - i))
    logging.debug('Set progress bar', N, i, N - i)
    pg = 0
    done = False

    def get_time(at_time):
        return sh.get_time_func(ref.path, at_time, ref.unbound['decode_time'])

    def time_at(at_idx):
        return sh.time_at(ref.path, at_idx, ref.unbound['decode_time'])
    
    seq = [e for e in iterate_images(sh, get_time, time_at, N, T,
                                     ti, acquisition_start_temperature, Tstep, tstep, prog)]
    if fourcc:
        fourcc = cv.VideoWriter_fourcc(*str(fourcc))
    else:
        fourcc = default_fourcc
        
    if not len(seq):
        logging.error('no frames', N, i)
        return False
    
    out = cv.VideoWriter(output, fourcc, framerate, (wMax, hMax))
    diag = np.sqrt(hMax ** 2 + wMax ** 2)
    ffactor = int(diag / 1000)
    ffactor = max(1, ffactor)
    print('found frames', len(seq), 'diagonal', diag, 'fontfactor', ffactor)
    font = {'fontScale': ffactor,
            'thickness': ffactor + 1,
            'color': (0, 0, 255),
            'fontFace': cv.FONT_HERSHEY_DUPLEX}
    fontpos = 10 * ffactor
    for i, Ti in seq:
        if done:
            break
        # Get image
        t, img = ref[i]
        print('exporting {:.2f} {} {:.0f}'.format(100.*i / N, i, t), 'to', output)
        if isinstance(ref, reference.Binary):
            im = cv.imdecode(np.frombuffer(img, dtype='uint8'), 1)
        elif isinstance(ref, reference.Profile) or isinstance(ref, reference.CumulativeProfile):
            # Blank image
            im = np.ones((hMax, wMax), np.uint8) * 255
            # Color black area contained in path
            ((w, h), x, y) = img
            x += np.uint16(x_translation)
            y += np.uint16(y_translation)
            if centerx:
                x += np.uint16(wMax / 2. - int(x.mean()))
            if centery:
                y += np.uint16(hMax / 2. - int(y.mean()))
            # Avoid black-white inversion
            x = np.concatenate(([0, 0], x, [wMax, wMax, 0]))
            y = np.concatenate(([hMax, y[0]], y, [y[-1], hMax, hMax]))
            p = np.array([x, y], np.int32).transpose()
            cv.fillPoly(im, [p], 0)
            m = im.mean()
            if m > 250 or m < 5:
                logging.debug('invalid frame', p, m)
                # import pylab
                # pylab.plot(x, y)
                # pylab.show()
                break
            im = np.dstack((im, im, im))
        else:
            logging.debug('Unsupported reference')
            break
        # Get T
        dt = timedelta(seconds=int(t))
        cv.putText(im, f"{dt} {Ti:.1f}C", (fontpos, hMax - fontpos), **font)
        
        # Write frame
        out.write(im)
        
        if prog:
            if pg > 1 and prog.value() == 0:
                logging.debug('Export cancelled at frame', i)
                break
            pg = i - i0 + N - i0
            prog.setValue(pg)

    logging.debug('releasing', output)
    out.release()
    return True


def skeleton(opts=None):
    opts = opts or {}
    ao(opts, 'src', 'String', '/hsm/sample0', 'Sample source')
    ao(opts, 'T', 'String', '/kiln/T', 'Temperature source')
    ao(opts, 'ext', 'Chooser', 'profile', 'Rendering source', options=['profile', 'frame'])
    ao(opts, 'startTemp', 'Float', 0, 'Start temperature')
    ao(opts, 'fps', 'Integer', 50, 'Framerate', min=1, max=100, step=1, unit='hertz')
    ao(opts, 'Tstep', 'Float', 1, 'Temperature steps', min=0, max=50, step=1, unit='celsius')
    ao(opts, 'tstep', 'Float', 0, 'Time steps', min=0, max=600, step=1, unit='second')
    ao(opts, 'centerx', 'Boolean', False, 'Center X coord')
    ao(opts, 'centery', 'Boolean', False, 'Center Y coord')
    ao(opts, 'codec', 'Chooser', 'X264', 'Output video codec', options=['X264', 'XVID', 'MJPG'])
    return opts

