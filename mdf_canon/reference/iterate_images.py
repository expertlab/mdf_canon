#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


def iterate_images(proxy, get_time, time_at, N, Tpath, start_time, start_temperature, step, timeStep, prog=None):
    """Yields image index and temperature"""
    step_sign = +1
    step_sign_inversions = 0
    T_zero = proxy.col_at(Tpath, 0, raw=True)[1]
    i_maxT, t_maxT, maxT = proxy.max(Tpath, start_time)
    i_minT, t_minT, minT = proxy.min(Tpath, start_time)
    t_max = proxy.get_time(Tpath, 1e6)
    if start_time:
        start_index = get_time(start_time)
        T_first = proxy.col_at_time(Tpath, start_time, raw=True)[1]
    else:
        start_index = 0
        T_first = T_zero
    
    total_number_of_images = N - start_index
    
    new_i = start_index
    new_t = start_time
    new_T = start_temperature
    image_count = 1
    while True:
        # Avoid proceeding beyond the end
        if image_count >= total_number_of_images:
            logging.debug('End of image dataset')
            break
        # Always return the first image of the dataset
        if image_count == 1:
            image_count += 1
            yield 0, T_zero
        # Always return the first image of the selection
        if image_count == 2 and start_index != 0:
            image_count += 1
            yield start_index, T_first
        # Avoid infinite search
        if step_sign_inversions > 3:
            logging.debug('No mor sign inversions allowed')
            break
        # Advance prog
        if prog and image_count % 10 == 0:
            prog.setValue(new_i - start_index)
        new_t += timeStep
        # No temperature stepping required
        if timeStep > 0 and step == 0:
            if new_t > t_max:
                break
            new_i = get_time(new_t)
            new_T = proxy.col_at_time(Tpath, new_t, raw=True)[1]
            image_count += 1
            yield new_i, new_T
            
        # Temperature stepping
        new_T += step_sign * step
        
        # Need inversion
        if new_T > maxT or new_T < minT:
            logging.debug('Exceeded maxT/minT', new_T, maxT, minT, step * step_sign)
            step_sign *= -1
            step_sign_inversions += 1
            # The next step will actually invert
            new_T += step_sign * step
            continue
        
        # Search nearest next T starting from new time
        near = proxy.nearest(Tpath, new_T, start_time=new_t)
        if near is False:
            continue
        j, t, T = near
        # Newarest image corresponding to that time
        new_i = get_time(t)
        # Effective time of that image
        t1 = time_at(new_i)
        # Time equal to last one: allow one more T increment
        if t1 <= new_t:
            logging.debug('Skip to next step: ', new_T, t1, new_t, step_sign)
            new_i += 1
            if new_i >= N - 1:
                break
            new_t = time_at(new_i)
            continue
        
        # Effective temperature of that image
        T1 = proxy.col_at_time(Tpath, t1, raw=True)[1]
        
        new_t = t1
        new_T = T1
        image_count += 1
        yield new_i, new_T
        
