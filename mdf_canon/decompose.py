"""Decompose an HDF file for cloud backup"""
import os

import tables
from tables.link import SoftLink
from tables import Group

from mdf_canon.indexer import SharedFile
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

import hashlib




def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def cleaname(p):
    return p.replace('/', '+')

    
def decompose_leaf(n, new_dir, meta, new_file=False):
    
    print('decomposing', n._v_pathname)
    filename = cleaname(n._v_pathname + '.h5')
    dump = os.path.join(new_dir, filename)
    if os.path.exists(dump):
        os.remove(dump)
    
    meta_parent = meta.test.get_node(n._v_parent._v_pathname)
    kw = {}
    if new_file:
        f = tables.open_file(dump, mode='a')
        n.copy(f.root, 'dump')
        kw = {'start':0, 'stop':0}
        #TODO: create an empty node of the same type, then copy attrs
        meta.test.create_external_link(meta_parent, n._v_name, filename+':'+n._v_pathname)
        f.close()
        #TODO: create a fake node under meta or a link to the external file
    else:
        dump = False
        n.copy(meta_parent, n._v_name, **kw)

        
        
    meta.flush()
    return dump
     
        
def decompose_group(g, meta):
    if not meta.has_node(g._v_pathname): 
        meta_parent = meta.test.get_node(g._v_parent._v_pathname)
        print('copy meta group', g._v_pathname)
        g._f_copy(meta_parent, g._v_name)
        return True
    return False


def decompose(filepath, output_dir, explode=False, select=lambda path: True):
    sh = SharedFile(filepath, mode='r')
    uid = sh.get_uid()
    new_dir = os.path.join(output_dir, uid)
    os.makedirs(new_dir, exist_ok=True)
    if not os.path.exists(new_dir):
        os.mkdir(new_dir)
        
    meta_path = os.path.join(new_dir, ':meta.h5')
    meta = SharedFile(meta_path, mode='w')
    links = []
    done = set([])
    checksums = {}
    size = 0

    def softlink(full_path, target):
        full_path = full_path.rstrip('/')
        parent = full_path.split('/')
        leaf_path = parent.pop(-1)
        parent = '/'.join(parent)
        if not select(target):
            return False
        links.append([parent, leaf_path, target])
        done.add(full_path)
        print('SOFTLINK', full_path, target, parent, repr(leaf_path))
        return True
        
    # TODO: Profile/Image freq limit
    # TODO: check for last_edit_time or file modification date
    for g in sh.test.walk_groups():
        if not select(g._v_pathname):
            continue
        
        if isinstance(g, SoftLink):
            softlink(g._v_pathname, g.target)
            continue
        
        if g._v_pathname != '/': 
            decompose_group(g, meta)
        
        for n in sh.test.walk_nodes(g):
            p = n._v_pathname
            if p in done or not select(p):
                continue
            if isinstance(n, SoftLink): 
                softlink(p, n.target)
                continue
            if isinstance(n, Group):
                decompose_group(n, meta)
                done.add(p)
                continue
            if 'kid' in n._v_attrs:
                k = str(n._v_attrs['kid'])
                if k.startswith("b'") and k.endswith("'"):
                    k = k[2:-1]
                if p.startswith('/summary/') and not k.startswith('/summary/'):
                    k = '/summary' + k
                if k != p and k.count('/') > 1:
                    softlink(p, k)
                    print('skip kid link', p, k)
                    continue
            if '/userdata/' in p:
                print('skip userdata', p)
                continue
            sz = os.stat(meta_path).st_size
            dump = decompose_leaf(n, new_dir, meta, new_file=explode)
            done.add(p)
            if dump:
                sz = os.stat(dump).st_size
                chk = md5(dump)
            else:
                sz = os.stat(meta_path).st_size - sz 
                chk = ''
                
            checksums[p] = (sz, chk)
            size += sz
    
    for parent, name, target in links:
        logging.debug('Linking', parent, name, target)
        meta.create_soft_link(parent, name, target=target)
        
    original_size = os.stat(filepath).st_size
    sz = os.stat(meta_path).st_size
    chk = md5(meta_path)
    checksums[':meta'] = (sz, chk)
    if dump:
        size += sz
    else:
        size = sz
    hashes = open(os.path.join(new_dir, ':hash'), 'w')
    hashes.write('# Input: {}\n'.format(filepath))
    for path, (sz, chk) in checksums.items():
        hashes.write('{}; {}; {}\n'.format(path, sz, chk))
    
    hashes.write('\n# Final size: {} vs {}, {:.1f}%'.format(size, original_size, 100 * size / original_size))
    
    return meta_path


def recompose(dirpath):
    pass


import re
from collections import defaultdict


def reference_selector(default=False, **kw0):
    """Pass default=True to make undefined args true by default"""
    kw = defaultdict(lambda: default)
    kw.update(kw0)
    # Set standard defaults
    for k in ('fast', 'motors', 'thermal', 'kalman',  # Special keywords
              'morla', 'storage', 'support', 'beholder',  # Devices
              'log', 'frame', 'profile', 'throttling', 'analysis', 'moving'):  # Options
        kw[k] = kw[k]

    def selector_function(p):
        if not kw['fast'] and not p.startswith('/summary') and len(p.split('/')) > 2:
            return False
        if not kw['thermal'] and re.match('^(/summary)?/kiln/pid/', p):
            return False
        if not kw['motors'] and re.match('^(/summary)?/(board|morla)/.*?/[XYZFA]/*.?', p):
            return False
        if not kw['kalman'] and re.match('.*?/kalman(_[a-zA-Z0-9]*)+$', p):
            return False
        for k, v in kw.items():
            if v: continue
            if p.endswith(k):
                return False
            if re.match('^(/summary)?/{}/'.format(k), p):
                return False
        return True
        
    return selector_function


if __name__ == '__main__':
    from sys import argv
    done = 0
    for a in argv[1::]:
        if not os.path.exists(a):
            if done == 0:
                a = f
            else:
                break
        if not a.endswith('.h5'):
            continue
        print('Decomposing ', a)
        o = os.path.join(os.path.dirname(a), '__decompose')
        decompose(a, o, explode=True)
        done += 1
    print('decomposed:',done)
