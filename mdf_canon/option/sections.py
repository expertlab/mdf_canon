import collections
from functools import cmp_to_key
from . import option


def sort_children(prop_dict):
    for handle in prop_dict.keys():
        prop = prop_dict[handle]
        children = prop['children']
        sorted_keys = sorted(list(children.values()), key=cmp_to_key(option.prop_sorter))
        sorted_keys = [p['handle'] for p in sorted_keys]
        children = collections.OrderedDict(
            (k, children[k]) for k in sorted_keys)
        children = sort_children(children)
        prop['children'] = children
        prop_dict[handle] = prop
    return prop_dict


def sections(prop_dict, flat=False, configuration_level=5):
    """Reorder keys according to their sections"""
    if isinstance(prop_dict, (list, tuple)):
        p = {}
        for opt in prop_dict:
            print(opt)
            if ('handle' not in opt):
                h = list(opt.keys())[0]
                if h in p:
                    p[h]['current'] = opt[h]
                else:
                    print('Skipping', opt)
            else:
                p[opt['handle']] = opt
        prop_dict = p
    else:
        prop_dict = prop_dict.copy()
    # Move children options into their parent's "children" key
    allowed_keys = list(prop_dict.keys())
    for handle in allowed_keys:
        prop = prop_dict.get(handle, False)
        if prop is False:
            continue
        # Add the children key to any option (simplifies further processing)
        if 'children' not in prop or isinstance(prop['children'], list):
            prop['children'] = collections.OrderedDict()
            prop_dict[handle] = prop

        parent = prop.get('parent', False)
        if parent is False:
            continue
        parentopt = prop_dict.get(parent, False)
        if parentopt is False:
            logging.debug('Non-existent parent for ', handle, prop['parent'])
            continue
        # Create the children list on the parent option
        if 'children' not in parentopt or isinstance(parentopt['children'], list):
            parentopt['children'] = collections.OrderedDict()
        # Append the child to the parent
        parentopt['children'][handle] = prop
        # Delete the child from the main dictionary
        del prop_dict[handle]
        # Update the parent on the main dictionary
        prop_dict[parent] = parentopt
        
    # Cleanup unwanted children
    for opt in prop_dict.values():
        children = opt['children']
        remove_keys = set(children.keys()) - set(allowed_keys)
        tuple(map(children.pop, remove_keys))
    
    # Sorting
    prop_dict = sort_children(prop_dict)
    # Sectioning
    out = collections.OrderedDict({'Main': collections.OrderedDict()})
    if flat:
        out['Main'] = list(prop_dict.values())
        return out
    for handle, prop in prop_dict.items():
        if prop.get('readLevel', -1) > configuration_level:
            continue
        spl = get_option_section(prop)
        # If section requires no separate tab:
        if spl == 'Main' or 'NoTab' in prop_dict.get(spl, {}).get('attr', []):
            out['Main'][handle] = prop
            continue
        if spl not in out:
            out[spl] = collections.OrderedDict()
        out[spl][handle] = prop
    return out


def get_option_section(opt):
    if 'section' in opt:
        return opt['section']
    handle = opt['handle']
    if ':' in handle:
        spl = ':'
    elif '_' in handle:
        spl = '_'
    else:
        return 'Main'
    return handle.split(spl)[0]


def groups(prop_dict):
    status_dict = collections.OrderedDict()
    results_dict = collections.OrderedDict()
    config_dict = collections.OrderedDict()
    base_dict = collections.OrderedDict()
    # Children must inherit parent's section
    children = {}
    for opt in prop_dict.values():
        for ch in opt.get('children', []):
            children[ch] = opt['handle']
    for handle, opt in prop_dict.items():
        if handle in children:
            print('skip children', opt['handle'])
            continue
        attr = opt.get('attr', [])
        if handle == 'name':
            base_dict[handle] = opt
        elif ('Config' in attr):
            config_dict[handle] = opt
        elif not attr and opt['type'] == 'Meta':
            results_dict[handle] = opt
        elif ('History' in attr and 'Runtime' not in attr) or ('Result' in attr):
            results_dict[handle] = opt
        elif ('Runtime' in attr) or ('ReadOnly' in attr) or opt['type'] == 'ReadOnly' or ('Status' in attr):
            status_dict[handle] = opt
        else:
            config_dict[handle] = opt
    return base_dict, config_dict, results_dict, status_dict

