#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Testing the programmatic widget construction."""
import unittest
import os
import json
from mdf_canon.tests import testdir
from mdf_canon.option import sections
path = os.path.join(testdir, 'storage', 'sections.json')


class TestSections(unittest.TestCase):
        
    def test_sections(self):
        data = json.load(open(path, 'rb'))
        sec = sections(data)
        handles = [o['handle'] for o in sec['Main'].values()]
        self.assertIn('name', handles)
        self.assertNotIn('param_Sint', handles)
