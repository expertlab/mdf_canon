# -*- coding: utf-8 -*-
"""Option persistence."""
from copy import deepcopy
from mdf_canon.logger import get_module_logging
import json
import collections
import re


def _default(self, obj):
    return getattr(obj.__class__, "to_json", _default.default)(obj)


_default.default = json.JSONEncoder().default
json.JSONEncoder.default = _default

logging = get_module_logging(__name__)

defined_attr = {'Config': 'Display in configuration panel',
                'FilterSet': 'Numerical filtering on new value',
                'Hardware': 'Value should be synced with external device',
                'Event': 'Values are recorded only when changed',
                'Hidden': 'Not visible to the user',
                'History': 'Record value over time',
                'Hot': 'Client reads from memory during acquisition',
                'Index': 'Create a database index',
                'MultiChooser':'Allow multiple selections',
                'NoSlider': 'Do no display min/max slider',
                'ParallelForbidden': 'Client reads from memory',
                'Password': 'Obfuscate value',
                'PlainText': 'Value is plain text',
                'ReadOnly': 'Value cannot be changed',
                'Reset': 'Value is reset to default before test starts',
                'Result': 'Display in results panel',
                'Runtime': 'Do not save this value',
                }

defined_types = {
    # Binary types
    "Binary": 'Binary blob',
    'Image': 'Image',

    # Float types
    "Float": 'Float number',
    "Progress": 'Progress indicator',
    "Time": 'Unix timestamp',

    # Integer types
    "Integer": 'Integer number',
    "Boolean": 'True/False',

    # String types
    "Script": 'Executable script',
    "Section": 'Section header',
    "FileList": 'List of file names',
    "String": 'Text field',
    "FilePath": 'File system path',
    "TextArea": 'Long text field',
    "Date": 'Date',
    "Preset": 'Persistently saved preset name',
    "ThermalCycle": 'Thermal cycle curve name',

    # Fixed multicolumn types
    "Meta": 'Metadata composite field (Time, Temperature, Value)',
    'Rect': 'Rectangle x,y,w,h',
    'Point': 'Point x,y',
    'Log': 'Log messages',
    "Role": 'Pointer to another object',
    "RoleIO": 'Pointer to another option',
    "Curve": "t,T,v and thermal cycle calibration curve",

    # Variable multicolumn types
    "Table": 'A table of data',
    "Attachments": 'List of attachments',
    "MultiRole": "List of Role references",

    # Pickled types
    "ReadOnly": 'View only',
    "Hidden": 'Never visible to the user',
    "Chooser": 'Multiple choices menu',
    "Object": 'Generic object',
    "List": 'List of objects',
    'Profile': 'Image contour',
    'Stat': 'File statistics from stat command',
    'LogList': 'List of recent log messages, with time',
    "Dict": "Dictionary of options",
    
    # Other
    "Button": "Executes an action when pressed",
    "GPS": "Geographic coordinates",
    "Captcha": "Human recognition",
    "RangeFloat": "Numerical range",
    "RangeDate": "Calendar range",
}

defined_keys = {"max": "Maximum value", "min": "Minimum value",
                "step": "Step between subsequent values",
                "precision": "Visualization notation",
                "unit": "Measurement unit",
                "csunit": "Visualization measurement unit",
                "attr": "List of modifier attributes",
                "auto": "Value calculated or forced manually",
                "bookmark": "Progressive value retrieval",
                "children": 'List of subordered options',
                "current": "Option value",
                "dataset": "Additional data",
                "factory_default": "Predefined value",
                "flags": "Additional modifier flags",
                "handle": "Option code name",
                "name": "Descriptive string",
                "type": "Type of the option value",
                "header": "List of option definitions for table header",
                "initialDimension": "Percentage conversion factor",
                "section": "Display this option in a separate tab",
                "target": "Acceptable value classes",
                "timedata": "Time array or stepping for dataset",
                "parent": "Parent option in hierarchy",
                "plot": "Role in plotting",
                "options": "Predefined values or value names",
                "values": "Predefined values",
                "readLevel": "Required auth level for reading",
                "writeLevel": "Required auth level for writing",
                "query": "Alternative name of the query index",
                "query_include": "Include or exclude in query reply",
                "validate": "Input validation",
                "valueSignals": "Mapping of special value meanings",
                }

typed_types = {'integer': ['Integer', 'Boolean'],
               'float': ['Float', 'Progress', 'Time', 'DateTime'],
               'binary': ['Binary', 'Image'],
               'string': ['String', 'TextArea', 'Script', 'Section', 'FileList', 'Date', 'Preset', 'Button', 'ThermalCycle'],
               'multicol': ['Meta', 'Rect', 'Point', 'Role', 'RoleIO', 'GPS', 'RangeInteger', 'RangeFloat', 'RangeDate'],
               'pickle': ['ReadOnly', 'Hidden', 'Chooser', 'Object', 'List', 'Profile', 'Table', 'Attachments', 'Log', 'MultiRole', 'Curve', 'Stat', 'LogList'],
               }
bytype = {}
for k, v in typed_types.items():
    for t in v:
        bytype[t] = k

num_types = typed_types['integer'] + typed_types['float']

vkeys = "handle,name,current,factory_default,attr,type,writeLevel,readLevel,mb,step,max,min,options,parent,values,flags,unit,csunit,kid,priority,header".split(
    ',')

str_keys = ('handle', 'name', 'type', 'parent', 'unit', 'csunit', 'kid', 'aggregate')
int_keys = ('readLevel', 'writeLevel', 'mb', 'priority')
type_keys = ('current', 'factory_default', 'min', 'max', 'step')
repr_keys = ('attr', 'flags', 'options', 'values', 'chron', 'header')  # and any other....

nowrite = set(['Binary', 'Runtime'])  # attributes/types which should not be saved
# TODO: limit the nowrite just to the current and factory_default properties.


def tosave(entry, excl=[]):
    excl = set(excl).union(nowrite)
    """Determine if this option should be saved or not"""
    if len(excl.intersection(set([entry['type']]))) > 0:
        logging.debug('nowrite entry by type', entry['handle'], entry['type'])
        return False
    if 'attr' in entry:
        if len(excl.intersection(set(entry['attr']))) > 0:
            logging.debug('nowrite entry by attr', entry['handle'], entry['attr'])
            return False
    return True


def sorter(a, b):
    """Option sorter"""
    if 'priority' not in a[1] and 'priority' not in b[1]:
        return 0
    elif 'priority' in a[1] and 'priority' not in b[1]:
        return -1
    elif 'priority' in b[1] and 'priority' not in a[1]:
        return +1
    elif a[1]['priority'] > b[1]['priority']:
        return +1
    elif a[1]['priority'] == b[1]['priority']:
        return 0
    else:
        return -1


def prop_sorter(a, b):
    if not a and not b:
        return 0
    if not a:
        return +1
    if not b:
        return -1
    
    if (not 'priority' in a) and (not 'priority'  in b):
        return 0
    elif 'priority' in a and not ('priority' in b):
        return -1
    elif 'priority' in b and not ('priority' in a):
        return +1
    elif a['priority'] > b['priority']:
        return +1
    elif a['priority'] == b['priority']:
        return 0
    else:
        return -1


def default_value(otype, attr=[]):
    if otype == 'Boolean':
        current = False
    elif otype in num_types:
        current = 0
    elif otype in ['List', 'Table', 'RoleList', 'MultiRole', 'Attachments']:
        current = []
    elif otype == 'Chooser' and 'MultiChooser' in attr:
        current = []
    # TODO: remove point, does no meaning anymore!
    elif otype == 'Meta':
        current = {'temp': 'None', 'time': 'None', 'value': 'None'}
    elif otype == 'Stat':
        current = {'st_size': 0}
    elif otype == 'LogList':
        current = {}
    elif otype == 'Curve':
        current = {'cycle': [], 'T': [], 'v': [], 'label': ''}
    elif otype == 'Rect':
        current = [0, 0, 640, 480]
    elif otype == 'Point' or otype.startswith('Range'):
        current = [0, 0]
    elif otype == 'Log':
        current = [0, 'log']  # level, message
    elif otype == 'Profile':
        current = []
    elif otype == 'Role':
        current = ['None', 'default']
    elif otype == 'RoleIO':
        current = ['None', 'default', 'None']
    else:
        current = ''
    return current


def ao(d, handle=False, type='Empty', current=None, name=False,
       priority=-1, parent=False, flags=False, unit=None, options=False,
       values=False, attr=False, kid=0, **kw):
    if not handle:
        logging.debug('ao: No handle!', handle)
        return d
    assert 'attrs' not in kw, 'Invalid Option keyword: attrs'
    attr = list(set(attr or []))
    flags = deepcopy(flags) or {}
    if not name:
        name = handle.capitalize()
    if d is not None and (priority < 0 or priority == None):
        priority = len(d)

    ent = {'priority': priority, 'handle': handle, 'name': name,
           'readLevel': 0, 'writeLevel': 0,
           'type': type, 'kid': kid, 'attr': attr, 'flags': flags,
           'parent': parent}
    ent.update(kw)
    if current is not None:
        ent['current'] = current
    if type == 'MultiRole':
        ent['header'] = ao([], 'role', "Role", name="Role")
        if ent['target']:
            ent['header'][0]['target'] = ent.pop('target')
    if values is not False:
        ent['values'] = values
    if options is not False:
        ent['options'] = options
    if unit is not None:
        ent['unit'] = unit
    elif type == 'Meta':
        ent['unit'] = {'time': 'second', 'temp': 'celsius', 'value': 'None'}
    ent['kid'] = kid or str(id(ent))
    ent = validate(ent)
    if type not in ['Object', 'Binary', 'Image', 'Profile']:
        ent['current'] = deepcopy(ent['current'])
    if d is None:
        return ent
    if hasattr(d, 'items'):
        d[handle] = ent
    else:
        d.append(ent)
    return d


zero_means_undefined_attr = set(['ReadOnly', 'Runtime', 'History',
                                 'Hardware', 'Hot', 'Hidden', 'Event'])


def zero_means_undefined(entry):
    no_signals = 'valueSignals' not in entry
    not_auto = 'auto' not in entry
    not_status = entry.get('section', None) != 'Status'
    default_zero = entry.get('factory_default', 0) == 0
    if no_signals and not_auto and not_status and default_zero:
        attr = set(entry.get('attr', []))
        if len(attr - zero_means_undefined_attr) == len(attr):
            entry['valueSignals'] = {'0': '---'}
    return entry


def camel_case_split(s):
    return re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', s)


def validate(entry):
    """Verify coherence of option `entry`"""
    key = entry.get('handle', False)
    if not key:
        logging.debug('No handle for', entry, ': skipping!')
        return False
    # Type guessing
    etype = entry.get('type', False)
    if etype is False:
        cur = entry.get('current', None)
        if type(cur) == type(1):
            etype = 'Integer'
        elif type(cur) == type(''):
            etype = 'String'
        elif type(cur) == type(1.):
            etype = 'Float'
        elif type(cur) in [type([]), type((1,))]:
            etype = 'List'
        elif isinstance(cur, {}):
            if set(cur.keys()) == set('temp', 'time', 'value'):
                etype = 'Meta'
            if len(set('T', 'v').intersection(set(cur.keys()))) == 2:
                etype = 'Curve'
        else:
            logging.debug('No type for', entry, ': skipping!')
            return False
        entry['type'] = etype
    # add attr field if not present
    if 'attr' not in entry:
        entry['attr'] = []
    # Current value guessing
    if 'factory_default' in entry and 'current' not in entry:
        entry['current'] = entry['factory_default']
    elif 'current' not in entry:
        entry['current'] = default_value(etype, entry['attr'])
    if entry['current'] == None:
        entry['current'] = 'None'
    if 'factory_default' not in entry:
        entry['factory_default'] = entry['current']
    
    # Complete other required keys
    if 'name' not in entry:
        n = camel_case_split(entry['handle'].capitalize().replace('_', ' '))
        n = ' '.join(n).lower()
        if len(n) > 2:
            n = n[0].upper() + n[1:]
        entry['name'] = n
    # 0=always visible; 1=user ; 2=expert ; 3=advanced ; 4=technician ;
    # 5=developer; 6=never visible
    if 'readLevel' not in entry:
        entry['readLevel'] = 0
    if 'writeLevel' not in entry:
        # Inizializzo al livello readLevel+1
        entry['writeLevel'] = entry['readLevel'] + 1
    if 'flags' not in entry:
        entry['flags'] = {}
    
    if 'parent' not in entry:
        entry['parent'] = False
    if entry['parent'] == entry['handle']:
        logging.critical('Option parent must differ from handle!')
        entry['parent'] = False
    if 'unit' not in entry:
        if entry['type'] == 'Meta':
            entry['unit'] = {
                'time': 'second', 'temp': 'celsius', 'value': 'None'}
        else:
            entry['unit'] = 'None'
        if entry['type'] == 'Meta':
            entry['unit']['temperature'] = 'celsius'
            entry['unit']['time'] = 'second'
    
    # Type-specific completion
    if etype in ('Integer', 'Float') and entry['current'] == 0:
        entry = zero_means_undefined(entry)
    if etype == 'RoleIO' and 'options' not in entry:
        entry['options'] = ['None', 'default', 'None']
    if etype == 'Role' and 'options' not in entry:
        entry['options'] = ['None', 'default']
    if etype in ['Chooser', 'FileList'] and 'options' not in entry:
        entry['options'] = []
    # add maximum=1 for Progress
    if etype == 'Progress' and 'max' not in entry:
        entry['max'] = 1
    # Table migration
    elif etype in ['Table', 'Attachment']:
        entry = migrate_table_header(entry)
    elif etype == 'Script':
        kid = entry.get('kid', '')
        if not kid or '/' not in kid:
            return entry
        from mdf_canon.milang.scriptable import script_needs_update
        script_needs_update(entry, entry['kid'])
    return entry


read_only_keys = ['handle', 'type']


def mkheader(*nametypes, prefix='', **optlists):
    nh = []
    for i, hdef in enumerate(nametypes):
        if isinstance(hdef, str):
            hdef = (hdef, 'String')
        (name, otype) = hdef
        ao(nh, prefix + str(i), otype, name=name)
        # Used for old list  unit, precision, visible
        for name, enum in optlists.items():
            if name == 'visible' and enum in [0, False]:
                nh[-1]['attr'].append('Hidden')
            elif enum[i] not in [None, 'None']:
                nh[-1][name] = enum[i]
    return nh


def detect_stale_header(row, header):
    """Check if row corresponds to a list of
    2-tuples [name, type] from old header definition"""
    types = [h['type'] for h in header]
    for i, e in enumerate(row):
        if not isinstance(e, (tuple, list)):
            return False
        if not len(e) == 2:
            return False
        if not isinstance(e[0], str):
            return False
        if i >= len(types):
            return True
        if e[1] != types[i]:
            return False
    return True


def migrate_table_header(opt):
    # No data
    if len(opt['current']) == 0:
        return opt
    # An header is already defined
    if len(opt.get('header', [])):
        if detect_stale_header(opt['current'][0], opt['header']):
            opt['current'].pop(0)
        return opt
    h = opt['current'].pop(0)
    n = len(h)
    unit = opt.get('unit', [])
    unit = unit if len(unit) == n else [None] * n
    precision = opt.get('precision', [])
    precision = precision if len(precision) == n else [None] * n
    visible = opt.get('visible', [])
    visible = visible if len(visible) == n else [1] * n
    # print('migrating', opt['handle'], h, opt)
    opt['header'] = mkheader(*h, prefix=opt['handle'] + '_',
                             unit=unit,
                             precision=precision,
                             visible=visible)
    return opt


def namingConvention(path, splt='/'):
    """If path pertains to a sample property, returns its sample number and option name"""
    if not splt + 'sample' in path:
        return path, None
    if path.endswith(splt):
        path = path[:-1]
    v = path.split(splt)
    # Find sample number
    for i, d in enumerate(v):
        if not d.startswith('sample'):
            continue
        idx = int(d[6:])
        break
    # Find option name
    # If it is properly a sample option
    if v[-2].startswith('sample'):
        return v[-1], idx
    # Otherwise, return path starting from sample
    return splt.join(v[i:]), idx


def get_header_types(o):
        """Compatibility header types extraction.
        Old format keep [(Name,Type),...] list as first element of current.
        New format has separate header field with a list of full option dictionaries."""
        h = o.get('header', [])
        if len(h):
            return list([e['type'] for e in h])
        cur = o.get('current', [])
        is_list = isinstance(cur, (list, tuple, dict)) and len(cur)
        h = ([] if not is_list else cur[0])
        return list([e[1] for e in h])


class Option(object):

    """An Option object"""
    _keys = ['handle', 'type', 'attr', 'name', 'current', 'factory_default',
             'current', 'options', 'values', 'unit', 'parent', 'flags', 'priority']
    """Mandatory keys"""
    _entry = {}
    _kid = ''
    _priority = 0

    def __init__(self, **kw):
        self.entry = kw
        
    def to_json(self):
        return self._entry
            
    def items(self):
        return self.entry.items()
    
    def keys(self):
        return self.entry.keys()
    
    def values(self):
        return self.entry.values()
    
    def __len__(self):
        return len(self.entry)

    def __str__(self):
        """String representation useful for printing purposes"""
        s = 'Option object: ' + self._entry['handle'] + '\n'
        for k, v in self.entry.items():
            if k == 'handle':
                continue
            s += '\t |%s=%s\n' % (k, v)
        return s

    def __repr__(self):
        """Pythonic representation"""
        return 'Option(**%s)' % repr(self.entry)

    def pretty_format(self):
        r = '{'
        for key, val in self.entry.items():
            # Avoid obvious keys
            if key in ['kid', 'priority', 'factory_default'] or \
                    (key == 'comment' and 'dummy' in val) or \
                    (key == 'readLevel' and val == 0) or \
                    (key == 'writeLevel' and val == self['readLevel'] + 1) or \
                    (key == 'parent' and val is False) or \
                    (key == 'unit' and val == 'None') or \
                    (key == 'flags' and val == {}) or \
                    (key == 'attr' and val == []):
                continue
            if type(val) == type(''):
                if '\n' in val:
                    val = '"""' + val + '"""'
                else:
                    val = repr(val)
            else:
                val = repr(val)
            r += '"%s": %s, \n\t' % (key, val)
        r += '}'
        return r

    def __eq__(self, other):
        """Checks the equality between two Option objects"""
        return self._entry == getattr(other, '_entry', None)

    def __delitem__(self, k):
        if k not in self._keys:
            logging.debug('Requested key does not exist')
            return False
        if k in self._entry:
            del self._entry[k]
        return True

    def pop(self, *a):
        return self._entry.pop(*a)

    @property
    def entry(self):
        """Return a dictionary entry"""
        return self._entry

    @entry.setter
    def entry(self, e):
        """Sets the dictionary entry"""
        for k in self._keys:
            self._entry[k] = None
        en = validate(e)
        if en:
            self._entry = en

    def get(self, *arg):
        # If key does not exists and default was specified in kw, return
        # default
        arg = list(arg)
        if len(arg) == 0:
            arg.append('current')
        k = arg[0]
        # If keyword does not exist, return default if specified
        if len(arg) == 2 and k not in self._entry:
            return arg[1]
        # Return the value or raise exception
        return self._entry[k]

    __getitem__ = get

    def __contains__(self, key):
        return key in self._entry

    def has_key(self, k):
        return k in self._entry

    def set(self, *arg):
        # If just one argument, pass to `current` key
        if len(arg) == 1:
            k = 'current'
            v = arg[0]
        else:
            # traditional key,val was passed
            k, v = arg
        if k in read_only_keys:
            logging.debug('Read only key!', k, v, self._entry['handle'])
            return
        self._entry[k] = v

    __setitem__ = set

    def set_base_kid(self, kid):
        self._entry['kid'] = kid + self._entry['handle']

    # ##
    # Conversions
    # ##

    def validate(self):
        self._entry = validate(self._entry)

    def copy(self):
        return Option(**self._entry)

    def migrate_from(self, old):
        """Migrate Option from `old`.
        Notice: the first migration always happens between hard-coded `old` and saved configuration file in self.
        In this case `old` is the source code version, and new is the user-configured version."""
        # These keys can only change on software updates.
        # So, their `old` value cannot be overwritten and must be retained
        for k in ('name', 'readLevel', 'writeLevel',
                  'mb', 'unit', 'csunit', 'parent', 'error'):
            if k in old:
                self._entry[k] = old[k]
                
        upkeys = ['step', 'max', 'min', 'options', 'values', 'visible', 'precision',
                  'unit']
        
        # Import any key which was defined in the old definition, but is missing from the new
        for k in old.keys():
            if k not in self._entry:
                self._entry[k] = old[k]
        ot = old['type']
        nt = self['type']
        # Never update script factory_default
        if nt != 'Script' and 'factory_default' in old:
            self._entry['factory_default'] = old['factory_default']
        # Retain special attributes
        oa = set([])
        na = set([])
        if 'attr' in self._entry:
            na = set(self._entry['attr'])
        if 'attr' in old:
            oa = set(old['attr'])
        # Upgrade attributes to source code (old)
        self._entry['attr'] = list(oa)
        
        # Reset table option if its definition changed
        if nt in ['Table', 'Attachments']:
            self._entry = migrate_table_header(self._entry)
            old = migrate_table_header(old)
            new_def = get_header_types(self)
            old_def = get_header_types(old)
            if new_def != old_def:
                logging.debug('Incompatible table definition', self['handle'], new_def, old_def)
                self._entry['current'] = old['current']
                self._entry['header'] = old['header']
        
        # No type change: exit
        if ot == nt:
            # Retain options and values
            if ot == 'Chooser':
                upkeys = ['options', 'values']
            else:
                return
        # Hard-coded 'old' type differs from configured type:
        # Import all special keys that might be defined in old but missing in
        # self
        for k in ['type'] + upkeys:
            if k in old:
                self._entry[k] = old[k]
        # Exit if it was a Chooser
        if ot == nt:
            return
        if 'current' not in self._entry:
            return
        
        nc = self._entry['current']
        # New current value migration from new type (red) to old type (hard
        # coded)
        try:
            if ot in ('String', 'TextArea', 'FileList', 'Section'):
                nc = str(nc)
            elif ot == 'Integer':
                nc = int(nc)
            elif ot in ('Float'):
                nc = float(nc)
            elif ot == 'Button':
                nc = ''
            # Keep the new current if nothing else is found
            elif 'current' in old:
                oc = old['current']
                if type(nc) != type(oc):
                    nc = oc
            self._entry['current'] = nc
        except:
            logging.debug('Impossible to migrate current value', old['handle'], nc, ot)
            # Remove current key
            del self._entry['current']


def merge_conf_def(*definitions):
    out = collections.OrderedDict()
    ref = {}
    changes = {}
    for definition in definitions:
        for opt in definition:
            opt = deepcopy(opt)
            if 'handle' in opt:
                h = opt['handle']
                ref[h] = opt
                out[h] = opt
            else:
                h = list(opt.keys())[0]
                changes[h] = opt[h]
    for k, v in changes.items():
        out[k]['current'] = v
    r = list(out.values())
    return r
