# -*- coding: utf-8 -*-
"""Unit conversion"""
from math import *
import numpy as np
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

base_units = {'micron': 'length',
              'micron^3': 'volume',
              'micron^2': 'area',
              'degree': 'angle',
              'celsius': 'temperature',
              'celsius/min': 'heating rate',
              'percent/min': 'part rate',
              'second': 'time',
              'percent': 'part',
              'hertz': 'frequency',
              'kilobyte': 'memory',
              'poise': 'viscosity',
              'cm^2/second': 'diffusivity',
              'joule/gram*kelvin': 'specific heat',
              'gram/cm^3': 'density',
              'cm^2*kelvin/watt': 'thermal resistance',
              "cm^3/minute": "flow",
              'gram': 'weight',
              'volt': 'tension',
              'ohm': 'resistance',
              'volt/second': 'tensionRate',
              'ampere': 'current',
              'viscosity':'poise',
              'bar': 'pressure',
              '1/kelvin':'cte'}

from_base = {'length': {'micron': lambda v: v,
                        'nanometer': lambda v: v * 1E3,
                        'millimeter': lambda v: v * 1E-3,
                        'centimeter': lambda v: v * 1E-4,
                        'meter': lambda v: v * 1E-6,
                        },
             # area
             'area': {'micron^2': lambda v: v, 'nanometer^2': lambda v: v * 1E6, 'millimeter^2': lambda v: v * 1E-6},
             # volume
             'volume': {'micron^3': lambda v: v, 'nanometer^3': lambda v: v * 1E9, 'millimeter^3': lambda v: v * 1E-9},
             # angle
             'angle': {'degree': lambda v: v, 'radian': lambda v: pi * v / 180.},
             # temperature
             'temperature': {'celsius': lambda v: v, 'kelvin': lambda v: v + 273.15, 'fahrenheit': lambda v: 32 + (9 * v / 5.)},
             'heating rate': {'celsius/min': lambda v: v, 'kelvin/min': lambda v: v + 273.15, 'fahrenheit/min': lambda v: 32 + (9 * v / 5.)},
             # time
             'time': {'microsecond': lambda v: v * 1e6, 'millisecond': lambda v: v * 1000., 'second': lambda v: v,
                      'minute': lambda v: v / 60., 'hour': lambda v: v / 3600., 'day': lambda v: v / 86400.},
             'part': {'percent': lambda v: v, 'permille': lambda v: v * 10., 'permyriad': lambda v: v * 100.,
                      'ppm': lambda v: v * 10000., 'ppb': lambda v: v * (1.E7), 'ppt': lambda v: v * (1.E10)},
             'part rate': {'percent/min': lambda v: v, 'permille/min': lambda v: v * 10., 'permyriad/min': lambda v: v * 100.,
                      'ppm/min': lambda v: v * 10000., 'ppb/min': lambda v: v * (1.E7), 'ppt/min': lambda v: v * (1.E10)},
             'frequency': {'hertz': lambda v: v, 'kilohertz': lambda v: v / 1000.},
             'memory': {
                    'byte': lambda v: v * 1000,
                    'kilobyte': lambda v: v,
                    'megabyte': lambda v: v * 1E-3,
                    'gigabyte': lambda v: v * 1E-6,
                },
    'viscosity': {'poise': lambda v: v * 10,
                  'pascal/s': lambda v: v,
                  },
    'diffusivity': {'cm^2/second': lambda v: v,
                    'm^2/second': lambda v: v * 1E-4,
                    'mm^2/second': lambda v: v * 1E2,
                    },
    'flow': {'cm^3/minute': lambda v: v},
    'specific heat': {'joule/gram*kelvin': lambda v: v},
    'density': {'gram/cm^3': lambda v: v},
    'thermal resistance': {'cm^2*kelvin/watt': lambda v: v},
    'weight': {'gram': lambda v: v,
               'kilogram': lambda v: v * 1E-3,
               'milligram': lambda v: v * 1E3
               },
    'tension': {'volt': lambda v: v,
                'millivolt': lambda v: v * 1E3
                },
    'resistance': {'ohm': lambda v: v,
                   'milliohm': lambda v: v * 1E3,
                   'megaohm': lambda v: v * 1E-6,
                   },
    'tensionRate': {'volt/second': lambda v: v},
    'current': {'ampere': lambda v: v},
    'pressure': {'bar': lambda v: v, 'mbar': lambda v: v * 1E-3, 'Pa': lambda v: v * 1E5, 'kPa': lambda v: v * 1E2, 'atm': lambda v: v / 0.9869233},
}

derivatives = {'length': {'micron': 1,
                          'nanometer': 1E3,
                          'millimeter': 1E-3,
                          'centimeter': 1E-4,
                          'meter': 1E-6},
               'area': {'micron^2': lambda v: v, 'nanometer^2': lambda v: v * 1E6, 'millimeter^2': lambda v: v * 1E-6},
               'volume': {'micron^3': lambda v: v, 'nanometer^3': lambda v: v * 1E9, 'millimeter^3': lambda v: v * 1E-9},
               'angle': {'degree': 1, 'radian': pi / 180.},  # angle
               'temperature': {'celsius': 1, 'kelvin': 1, 'fahrenheit': 9 / 5.},
               'heating rate': {'celsius/min': 1, 'kelvin/min': 1, 'fahrenheit/min': 9 / 5.},
               'time': {'microsecond': 1e6, 'millisecond': 1000., 'second': 1., 'minute': 1 / 60., 'hour': 1 / 3600., 'day': 1 / 86400.},
               'part': {'percent': 1, 'permille': 10., 'permyriad': 100.,  # parts
                        'ppm': 10000., 'ppb': 1.E7, 'ppt': 1.E10},
                'part rate': {'percent/min': 1, 'permille/min': 10., 'permyriad/min': 100.,  # parts
                        'ppm/min': 10000., 'ppb/min': 1.E7, 'ppt/min': 1.E10},
               'frequency': {'hertz': 1, 'kilohertz': 1 / 1000.},  # freq
               'memory': {
    'byte': 1000,
    'kilobyte': 1,
    'megabyte': 1E-3,
    'gigabyte': 1E-6,
},
    'viscosity': {
    'poise': 10,
    'pascal/s': 1,
},
    'diffusivity': {'cm^2/second': 1,
                    'm^2/second': 1E-4,
                    'mm^2/second': 1E2,
                    },
    'flow': {'cm^3/minute': 1},
    'specific heat': {'joule/gram*kelvin': 1},
    'density': {'gram/cm^3': 1},
    'thermal resistance': {'cm^2*kelvin/watt': 1},
    'weight': {'gram': 1,
               'kilogram': 1E-3,
               'milligram': 1E3
               },
    'tension': {'volt': 1,
                'millivolt': 1E3
                },
    'resistance': {'ohm': 1,
                   'milliohm': 1E3,
                   'megaohm': 1E-6,
                   },
    'tensionRate': {'volt/second': 1},
    'current': {'ampere': 1},
    'pressure': {'bar': 1, 'mbar': 1E3, 'Pa': 1E-5, 'atm': 0.9869233},
}

to_base = {'length': {'micron': lambda v: v,
                      "nanometer": lambda v: v * 1E-3,
                      'millimeter': lambda v: v * 1E3,
                      'centimeter': lambda v: v * 1E4,
                      'meter': lambda v: v * 1E6,
                      },
           # area
           'area': {'micron^2': lambda v: v, 'nanometer^2': lambda v: v * 1E-6, 'millimeter^2': lambda v: v * 1E6},
           # volume
           'volume': {'micron^3': lambda v: v, 'nanometer^3': lambda v: v * 1E-9, 'millimeter^3': lambda v: v * 1E9},
           # angle
           'angle': {'degree': lambda v: v, 'radian': lambda v: v * 180. / pi},
           # temperature
           'temperature': {'celsius': lambda v: v, 'kelvin': lambda v: v - 273.15, 'fahrenheit': lambda v: 5 * (v - 32) / 9},
           'heating rate': {'celsius/min': lambda v: v, 'kelvin/min': lambda v: v - 273.15, 'fahrenheit/min': lambda v: 5 * (v - 32) / 9},
           # time
           'time': {'microsecond': lambda v: v / (1e6), 'millisecond': lambda v: v / 1000.,
                    'second': lambda v: v, 'minute': lambda v: v * 60., 'hour': lambda v: v * 3600., 'day': lambda v: v * 86400.},
           'part': {'percent': lambda v: v, 'permille': lambda v: v / 10., 'permyriad': lambda v: v / 100.,  # parts
                    'ppm': lambda v: v / 10000., 'ppb': lambda v: 1. * v / (10 ** 7), 'ppt': lambda v: 1. * v / 10 ** 10},
           'part rate': {'percent/min': lambda v: v, 'permille/min': lambda v: v / 10., 'permyriad/min': lambda v: v / 100.,  # part rates
                    'ppm/min': lambda v: v / 10000., 'ppb/min': lambda v: 1. * v / (10 ** 7), 'ppt/min': lambda v: 1. * v / 10 ** 10},
           'frequency': {'hertz': lambda v: v, 'kilohertz': lambda v: v * 1000.},
           'memory': {
                'byte': lambda v: v * 1E-3,
                'kilobyte': lambda v: v,
                'megabyte': lambda v: v * 1E3,
                'gigabyte': lambda v: v * 1E6,
            },
            'viscosity': {
                    'poise': lambda v: v * 0.1,
                    'pascal/s': lambda v: v,
                },
            'diffusivity': {'cm^2/second': lambda v: v,
                            'm^2/second': lambda v: v * 1E4,
                            'mm^2/second': lambda v: v * 1E-2,
                            },
            'flow': {'cm^3/minute': lambda v: v},
            'specific heat': {'joule/gram*kelvin': lambda v: v},
            'density': {'gram/cm^3': lambda v: v},
            'thermal resistance': {'cm^2*kelvin/watt': lambda v: v},
            'weight': {'gram': lambda v: v,
                       'kilogram': lambda v: v * 1E3,
                       'milligram': lambda v: v * 1E-3},
            'tension': {'volt': lambda v: v,
                        'millivolt': lambda v: v * 1E-3,
                        },
            'resistance': {'ohm': lambda v: v,
                           'milliohm': lambda v: v * 1E-3,
                           'megaohm': lambda v: v * 1E6,
                           },
            'tensionRate': {'volt/second': lambda v: v},
            'current': {'ampere': lambda v: v},
            'pressure': {'bar': lambda v: v, 'mbar': lambda v: v * 1E3, 'Pa': lambda v: v * 1E-5, 'kPa': lambda v: v * 1E-2, 'atm': lambda v: v * 0.9869233},
}

# Latex symbols for Veusz
symbols = {'micron': '{\mu}m', 	'nanometer': 'nm', 	'millimeter': 'mm',
           'centimeter': 'cm', 'meter': 'm',
           'micron^3': '{{\mu}m^3}', 'micron^2': '{{\mu}m^2}',
           'nanometer^3': '{nm^3}', 'nanometer^2': '{nm^2}',
           'millimeter^3': '{mm^3}', 'millimeter^2': '{mm^2}',
           'degree': '{\deg}', 	'radian': 'rad',
           'celsius': '{\deg}C', 'kelvin': '{\deg}K', 	'fahrenheit': '{\deg}F',
           'celsius/min': '{\deg}C/min', 'kelvin': '{\deg}K/min', 'fahrenheit': '{\deg}F/min',
           'second': 's', 'minute': 'min', 'hour': 'hr', 'day': 'd', 'millisecond': 'ms', 'microsecond': '{\mu}s',
           'percent': '%', 'permille': '{\\textperthousand}',
           'percent/min':'%/min', 'permille/min':'{\\textperthousand}/min',
           'hertz': 'Hz', 'kilohertz': 'kHz',
           'byte': 'B',
           'kilobyte': 'KB',
           'megabyte': 'MB',
           'gigabyte': 'GB',
           'poise': 'P',
           'pascal/s': 'Pa/s',
           'cm^2/second': u'{cm^2}{s^-1}',
           'cm^3/minute': u'{cm^3}{min^-1}',
           'm^2/second': u'{m^2}{s^-1}',
           'mm^2/second': u'{mm^2}{s^-1}',
           'gram/cm^3': 'g/{cm^3}',
           'cm^2*kelvin/watt': '{cm^2}K/W',
           'joule/gram*kelvin': 'J/gK',
           'gram': 'g',
           'milligram': 'mg',
           'kilogram': 'kg',
           'volt': 'V',
           'millivolt': 'mV',
           'ohm': '\Omega', 'milliohm': 'm\Omega', 'megaohm': 'M\Omega',
           'volt/second': 'V/s',
           '1/second': '1/s',
           '1/celsius': '{\deg}C^-1', '1/fahrenheit': '{\deg}F^-1', '1/kelvin': '{\deg}K^-1',
           'ampere': 'A',
           }

# HTML symbols for Qt
hsymbols = symbols.copy()
hsymbols.update({'micron': u'\u03bcm', 'micron^2': u'\u03bcm²', 'micron^3': u'\u03bcm³',
                 'nanometer^2': u'nm²', 'nanometer^3': u'nm³',
                 'millimeter^2': u'mm²', 'millimeter^3': u'mm³',
                 'degree': u'°',
                 'celsius': u'°C', 'kelvin': u'°K', 'fahrenheit': u'°F',
                 'celsius/min': u'°C/min', 'kelvin/min': u'°K/min', 'fahrenheit/min': u'°F/min',
                 'microsecond': u'\u03bcs',
                 'permille': u'\u2030', 'permille/min':u'\u2030/min',
                 'pixel': 'px',
                 'cm^2/second': u'cm²/s',
                 'cm^3/minute': u'cm³/min',
                 'm^2/second': u'm²/s',
                 'mm^2/second': u'mm²/s',
                 'gram/cm^3': u'g/cm³',
                 'cm^2*kelvin/watt': u'cm²K/W',
                 'ohm': u'\u03A9', 'milliohm': u'm\u03A9', 'megaohm': u'M\u03A9',
                 '1/second': u's⁻¹',
                 '1/celsius': u'°C⁻¹', '1/fahrenheit': u'°F⁻¹', '1/kelvin': u'°K⁻¹'
                 })

# Create a dictionary unit:dimension
known_units = {}
for domain, definitions in from_base.items():
    for unit_name in definitions.keys():
        known_units[unit_name] = domain

# TODO: resolve composed units; eg: A/B, A*B/C, etc

# TODO: translate into settings. Function to change all loaded options?
user_defaults = {'length': 'micron',
                'area': 'micron^2',
                'volume': 'micron^3',
                'angle': 'degree',
                'temperature': 'celsius',
                'heating rate': 'celsius/min',
                'time': 'second',
                'part': 'percent',
                'part rate': 'percent/min',
                'viscosity': 'posie',
                'diffusivity': 'cm^2/second',
                'specific heat':'joule/gram*kelvin',
                'density': 'gram/cm^3',
                'thermal resistance':'cm^2*kelvin/watt',
                'weight': 'gram',
                'tension': 'volt',
                'resistance': 'ohm',
                'tensionRate': 'volt/second',
                'current': 'ampere'
                 }


def get_unit_info(unit, units):
    # FIXME: implement proper tokenization
    p = 1
    # p = unit.split('^')
    # u = p[0]
    # if len(p) == 2:
    #    p = int(p[1].split('/')[0])
    # else:
    #    p = 1
# 	print units
    for key, group in units.items():
        # Get unit conversion function
        if unit not in group:
            continue
        return key, group[unit], p
    return None, None, p


class Converter(object):
    from_server = lambda val: val
    '''Convert `val` from server-side unit into client-side unit'''
    to_client = from_server
    '''Alias'''
    from_client = lambda val: val
    '''Convert `val` from client-side unit into server-side unit'''
    to_server = from_client
    '''Alias'''
    d = 1
    """Derivative factor for the conversion server->client"""
    unit = None

    @classmethod
    def convert_func(cls, from_unit, to_unit):
        """Returns the conversion function from `from_unit` to `to_unit`"""
        if from_unit in ('None', None):
            return lambda val: val
        if from_unit == to_unit:
            return lambda val: val
        if to_unit == None:
            dom = known_units[from_unit]
            to_unit = user_defaults[dom]
        c = cls(from_unit, to_unit)
        return c.from_server

    @classmethod
    def convert(cls, from_unit, to_unit, val):
        """Direct conversion of `val` from `from_unit` to `to_unit`"""
        if val is None:
            return val
        func = cls.convert_func(from_unit, to_unit)
        return func(val)

    def __init__(self, unit, csunit):
        if not unit:
            unit = ''
        x = unit.count('*')
        d = unit.count('/')
        p = unit.count('^')
        if x + d + p == 0:
            if unit not in known_units:
                unit = 'None'
            self.unit = unit
        # ...
        self.csunit = csunit

        if self.csunit == self.unit or 'None' in [self.unit, self.csunit]:
            # No conversion needed or possible
            self.from_server = lambda val: val
            self.from_client = lambda val: val
        else:
            group = known_units[csunit]
            cfb = from_base[group][csunit]  # client-to-base
            ctb = to_base[group][self.csunit]  # client-to-base
            # How to manage different groups?
            # cud = derivatives[group][csunit]
            # sud = derivatives[group][unit]

            if unit in base_units:
                # If the server unit is a base unit, return the direct conversion
                # to client-side unit
                self.from_server = cfb
                # Conversion derivative server->client
                # self.d = cud
                # return the direct conversion from client-side unit
                self.from_client = ctb
                # Conversion derivative client->server
            else:
                # Otherwise, convert the value to its base unit,
                # then convert this new value to client-side unit
                stb = to_base[group][unit]  # server-to-base
                self.from_server = lambda val: cfb(stb(val))
                # Conversion derivative client->server
                # self.d = sud
                # convert the value from client-side unit to its base unit,
                # then convert this new value to the actual server-side unit
                sfb = from_base[group][unit]
                self.from_client = lambda val: sfb(ctb(val))


def auto_percent(data, ini, from_unit, to_unit, base_unit=None, auto=True, exc=BaseException):
    if not ini:
        if not auto:
            raise exc('Selected dataset does not have an initial dimension set. \
        Please first run "Initial dimension..." tool.')
        ini = np.array(data[:5]).mean() or 100
        logging.debug('calculated initial dimension:', ini)
    from_group = known_units[from_unit]
    to_group = known_units[to_unit]
    if from_group == to_group:
        func = Converter.convert_func(from_unit, to_unit)
    elif from_group == 'part' and to_group != 'part':
        # If current dataset unit is not percent, convert to
        convert_func = Converter.convert_func(from_unit, 'percent')

        def func(out): return convert_func(out * ini / 100.)

    elif from_group != 'part':
        if not base_unit:
            base_unit = user_default.get(from_group, from_unit)
        convert_func = Converter.convert_func(from_unit, base_unit)

        def func(out): return 100. * convert_func(out) / ini

    return ini, func


def version_to_number(client):
    o = [1e20, 1e15, 1e10, 1e5, 1]
    client = map(lambda a: o[a[0]] * int(a[1]), enumerate(client.split('.')))
    print(client)
    return sum(client)

        
def fix_old_client_unit_inconsistency(dataset, attributes, f, output='unit'):
    if 'csunit' not in attributes or attributes['csunit'] == attributes.get('unit', None):
        return dataset, attributes
    ver = f.get_version()
    if not ver:
        return dataset, attributes
    ver_attr = f.get_attributes(ver)
    client = version_to_number(ver_attr.get('client_version', '0'))
    if client > version_to_number('5.4'):
        if output == 'unit':
            return dataset, attributes
        from_unit = attributes['unit']
        to_unit = attributes['csunit']
    else:
        if output == 'csunit':
            return dataset, attributes
        from_unit = attributes['csunit']
        to_unit = attributes['unit']
    ini, func = auto_percent(dataset, attributes.get('initialDimension', None),
                                   from_unit=from_unit,
                                   to_unit=to_unit)
    attributes['initialDimension'] = ini
    dataset = func(dataset)
    print('AAAAAAAA fixed', from_unit, to_unit)
    return dataset, attributes

