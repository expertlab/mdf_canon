#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import os
import zlib 
from datetime import datetime
import subprocess
import base64
import textwrap
from Crypto.Cipher import PKCS1_v1_5
from Crypto.Signature import PKCS1_v1_5 as sign_PKCS1_v1_5
from Crypto.PublicKey import RSA
import Crypto.Hash.SHA as SHA512

import time
if not hasattr(time, 'clock'):
    time.clock = time.process_time

try:
    unicode_decode = lambda s: s.decode('utf8')
    
except:
    unicode_decode = unicode

    
def go(cmd):
    r = subprocess.check_output(cmd, shell=True)
    return unicode_decode(r)


public_key = u"""-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANN+L4oERV46obB4NshB4DxDsz7vJX9I
30rdwflrfejQugdn98lHcmTE3yoXk5v0KlhuZtirN34K5hg1fOGoXkUCAwEAAQ==
-----END PUBLIC KEY-----"""

rsa_key = RSA.importKey(public_key)
rsa_size = rsa_key.size_in_bytes()

public_cipher = PKCS1_v1_5.new(rsa_key)
public_signature = sign_PKCS1_v1_5.new(rsa_key)


def get_sorted_devpaths(obj):
    dp = sorted([o['devpath'] for o in obj.devices])
    names = ['{},{}'.format(obj.child(o)['fullpath'], obj.child(o)['name']) for o in dp]
    return dp, names

    
id_sep = u'\n-----FRAME-----\n'
id_header = u'-----BEGIN INSTRUMENT IDENTIFIER-----\n'
id_footer = u'\n-----END INSTRUMENT IDENTIFIER-----'


def calculate_clientid_windows(date=False):
    disk = go(r'mountvol C:\ /L').strip()
    cmd = r'%Windir%/sysnative/reg.exe query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography" /v  MachineGuid'
    guid = go(cmd)
    guid = '\n'.join([l.strip() for l in guid.splitlines()])
    code = [disk, guid]
    if date:
        code.append(datetime.now().strftime('%d/%m/%Y'))
    return code


import uuid
from traceback import print_exc


def calculate_clientid_linux(date=False):
    """Collect unique identifier from hardware"""
    code = []
    try:
        disk = go(r"lsblk -no UUID $(df -P / | awk 'END{print $1}')")
        code.append(str(disk))
        dmi = go("sudo dmidecode |grep -E -i -w 'ID|Serial|Part|Asset|Manufacturer|Vendor|Version|Revision|UUID'")
        dmi = u'\n'.join(sorted(str(dmi).splitlines()))
        code.append(dmi)
    except:
        print_exc()
        print('Cannot determine disk or dmi uid (running in docker?) - generating random ones')
        code = [str(uuid.uuid4()), str(uuid.uuid4())]
    if date:
        code.append(datetime.now().strftime('%d/%m/%Y'))
    return code


def calculate_clientid(date=True):
    if os.name == 'nt':
        return id_sep.join(calculate_clientid_windows(date))
    return id_sep.join(calculate_clientid_linux(date))


def identifier_hash(ident):
    return SHA512.new(ident).hexdigest()


def calculate_identifier(root, date=True):
    # Internal ids
    code = []
    names = []
    if 'eq_sn' in root:
        code += [root['eq_sn']]
        names += [root['eq_sn']]
        
    def collect(name, code=code, names=names, root=root):
        if root.has_child(name):
            dp, nm = get_sorted_devpaths(getattr(root, name))
            code += dp
            names += nm

    collect('beholder')
    collect('morla')
    collect('thermal')
    collect('board')
    code += calculate_clientid_linux(date)
    return names, id_sep.join(code)


block_sep = u'\n----BLOCK----\n'
wrap_sep = u'$@#$%$\n'


def encrypt_block(cipher, b):
    out = cipher.encrypt(b)
    out = base64.b64encode(out)
    out = out.decode('utf-8')
    out = textwrap.wrap(out, 64, replace_whitespace=False, drop_whitespace=False)
    out = wrap_sep.join(out)
    return out


def decrypt_block(cipher, b):
    b = b.replace(wrap_sep, '')
    b = b.encode('utf-8')
    b = base64.b64decode(b)
    return cipher.decrypt(b, 0)


def encrypt_identifier(text0):
    text0 = text0.encode('ascii')
    text = zlib.compress(text0, 9)
    N = len(text)
    k = rsa_size - 11
    blocks = []
    while N > k:
        blocks.append(text[:k])
        text = text[k:]
        N = len(text)
    if text:
        blocks.append(text)
    blocks = [encrypt_block(public_cipher, b) for b in blocks]
    blocks = block_sep.join(blocks)
    out = id_header + blocks + id_footer
    return out


def decrypt_identifier(private_key, text):
    key = RSA.importKey(private_key)
    private_cipher = PKCS1_v1_5.new(key)
    i = text.find(id_header)
    e = text.find(id_footer, i)
    text = text[i + len(id_header):e]
    text = text.split(block_sep)
    blocks = [decrypt_block(private_cipher, b) for b in text]
    text = b''.join(blocks)
    ident = zlib.decompress(text)
    # Calculate hash without date
    ident1 = b'\n'.join(ident.splitlines()[:-2])
    h1 = SHA512.new(ident1).hexdigest()
    return ident.decode('utf-8'), ident1.decode('utf-8'), h1


sig_header = u"\n-----BEGIN LICENSE SIGNATURE-----\n"
sig_footer = u"\n-----END LICENSE SIGNATURE-----"

ppm_header = u"\n-----BEGIN TOKEN SIGNATURE-----\n"
ppm_footer = u"\n-----END TOKEN SIGNATURE-----"

func_sep = '    - '


def encode_functionalities(encoded, func_dict={}):
    txt = u''
    for name in encoded.split(','):
        txt += func_sep + func_dict.get(name, '*Invalid*') + u'\n'
    return txt


def decode_functionalities(license_text, func_dict={}):
    r = set([])
    for name, text in func_dict.items():
        if func_sep + text in license_text:
            r.add(name)
    return r


rate_sep = u' Celsius/minute, up to '


def encode_heating_rates(maxH):
    text = ''
    for T, rate in maxH:
        text += u'    {:.0f}{}{:.0f} Celsius\n'.format(rate, rate_sep, T)
    return text


def decode_heating_rates(license_text):
    tab = []
    for line in license_text.splitlines():
        if rate_sep not in line:
            continue
        line = line.split(rate_sep)
        R = int(float(line[0][4:]))
        T = int(float(line[1][:-8]))
        tab.append((T, R))
    return tab


epoch = datetime.strptime(u'01/01/1970', u'%d/%m/%Y')


def decode_client_license(license_text):
    r = {}
    found = 0
    for line in license_text.splitlines():
        if found == 1:
            r['remote'] = line.strip()
            found = 0
            continue
        if found == 2:
            r['remoteNick'] = line.strip()
            found = 0
            continue
        if found == 3:
            r['localNick'] = line.strip()
            found = 0
            continue
        if 'REFERENCE DEVICE ID' in line:
            found = 1
        elif 'REFERENCE DEVICE NAME' in line:
            found = 2
        elif 'LOCAL CLIENT NAME' in line:
            found = 3
    return r


def verify_license(license_text, decrypted_identifier_nodate, func_dict={}):
    funcs = set([])
    rates = [(0, 0)]
    i = license_text.find(u'$[')
    e = license_text.find(u']$', i)
    date = license_text[i + 2:e]
    if not len(date):
        print('License without expiry!')
        return 0, False, funcs, rates, u'', u''
    date = datetime.strptime(date, u'%d/%m/%Y')
    t = (date - epoch).total_seconds()
    i = license_text.find(sig_header, e + 2)
    e = license_text.find(sig_footer, i)
    
    msg = license_text[:i] + decrypted_identifier_nodate
    msg_hash = SHA512.new(msg.encode('utf-8'))
    
    signature0 = license_text[i + len(sig_header):e]
    # 0. Unwrap
    signature0 = signature0.replace(u'\n', u'')
    # 1. Decode
    signature = base64.b64decode(signature0)
    # 3. Verify
    valid = public_signature.verify(msg_hash, signature)
    
    if valid:
        funcs = decode_functionalities(license_text[:i], func_dict)
        if 'INSTRUMENT LICENSE' in license_text:
            rates = decode_heating_rates(license_text[:i])
        else:
            rates = decode_client_license(license_text)
        
    return t, valid, funcs, rates, msg, signature0


def verity_ppm_token(license_text):
    i = license_text.find(ppm_header)
    e = license_text.find(ppm_footer, i)
    msg = license_text[:i]
    msg_hash = SHA512.new(msg.encode('utf-8'))
    signature0 = license_text[i + len(ppm_header):e]
    # 0. Unwrap
    signature0 = signature0.replace(u'\n', u'')
    # 1. Decode
    signature = base64.b64decode(signature0)
    # 3. Verify
    valid = public_signature.verify(msg_hash, signature)
    return valid, msg.split('\n')

    
if __name__ == '__main__':
    try:
        type(raw_input)
    except:
        raw_input = input
    private_key = raw_input('Private key (/home/daniele/mdf_license.key):')
    if not private_key:
        private_key = '/home/daniele/mdf_license_512.key'
    private_key = open(private_key, 'rb').read()
    customer = raw_input('Customer (The Customer):')
    if not customer:
        customer = 'The Customer'
    expiry = raw_input('Expiry date (31/12/9999):')
    if not expiry:
        expiry = '31/12/9999'
    functionalities = raw_input('Enabled functionalities, comma separed (hsm,horizontal,flex,dta,cool):')
    if not functionalities:
        functionalities = 'hsm,horizontal,flex,dta,cool'
    rates = raw_input('Heating rates (1400:80,):')
    if not rates:
        rates = '1400:80'
    rates = rates.replace(' ', '').split(',')
    maxH = []
    for e in rates:
        if ':' not in e:
            break
        e = e.split(':')
        maxH.append(list(map(int, e)))
    identifier = raw_input('Instrument identifier file (instrument.identifier):')
    if not identifier:
        identifier = 'instrument.identifier'
    identifier = open(identifier, 'rb').read()
    from mdf_cloud.gen_license import generate_license
    lic, msg, sig = generate_license(private_key, identifier, expiry, customer, functionalities, maxH)
    open('output.license', 'w').write(lic)
    print(lic)
    dedent, dedent1, h1 = decrypt_identifier(private_key, identifier)
    r = verify_license(lic, dedent1)
    print('Authentic', r[1])
    print('Expiry', r[0])
    print('Funcs', r[2])
    print('Rates', r[3])
