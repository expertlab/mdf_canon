#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from mdf_canon.csutil import go, isWindows
from collections import defaultdict


def get_lib_info():
    """Find all libraries loaded in current process and their real shared object name with version"""
    if isWindows:
        return 'Unsupported on Windows platforms'
    r = go('pmap -p {}'.format(os.getpid()))
    paths = set([])
    out = ''
    for line in r[1].splitlines()[1:-1]:
        line = line.split(' ')
        p = line.pop(-1)
        if not p.startswith('/'):
            continue
        if not os.path.exists(p):
            continue
        if p in paths:
            continue
        paths.add(p)
        elf = go('readelf -d {}|grep SONAME'.format(p))[1]
        i = elf.find('[') + 1
        e = elf.find(']')
        out += '{} -> {}\n'.format(p, elf[i:e])
    return out


def parse_vmstat():
    if isWindows:
        return {}
    vm = {}
    r = go('export LANG="en_US.UTF-8"; vmstat -s -S M')[1]
    r = r.replace('  ', '').replace('  ', '').replace('\n ', '\n')
    if r[0] == ' ': 
        r = r[1:]
    for line in r.splitlines():
        line = line.split(' ')
        try:
            val = int(line[0])
        except:
            print('vmstat: unparsable', line, r)
            continue
        key = ' '.join(line[1:])
        vm[key] = val
    return vm


def decode_ps_line(line):
    pid, cpu, mem, virt, rss = list(map(float, line.split(' ')[1:6]))
    virt /= 1000
    rss /= 1000
    return int(pid), cpu, mem, virt, rss


def get_total_processes():
    return len(go('ps aux')[1].splitlines()) - 1


def get_process_stats(process_name):
    d = defaultdict(lambda:0)
    st, r = go('ps aux --sort=-rss|grep "{}"|grep -v grep'.format(process_name))
    d['proc_pid'] = []
    if st != 0:
        return d
    while '  ' in r:
        r = r.replace('  ', ' ')
    
    r = r.splitlines()
    d['proc_n'] = len(r)
    
    for j, line in enumerate(r):
        pid, cpu, mem, virt, rss = decode_ps_line(line)
        d['proc_pid'].append(pid)
        if cpu > d['proc_cpu']:
            d['proc_cpu'] = cpu
        if mem > d['proc_mem']:
            d['proc_mem'] = mem
        if virt > d['proc_virtual']:
            d['proc_virtual'] = virt
        if rss > d['proc_resident']:
            d['proc_resident'] = rss
    
        d['proc_cpuTot'] += cpu
        d['proc_memTot'] += mem
    return d
