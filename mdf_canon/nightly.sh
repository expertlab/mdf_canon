#!/bin/bash
set -e
echo $1
wget -O /opt/mdf/nightly.sh https://gitlab.com/expertlab/mdf_canon/-/raw/master/mdf_canon/nightly.sh
chmod 777 /opt/mdf/nightly.sh || echo "Could not change permission of nightly.sh"
EXTENSION=".*-18.04-.*.tar.gz"
if [ "$1" != "server" ]; then
    EXTENSION="linux.tar.gz"
    if [ "$1" != "client" ]; then
        echo "Required arguments: server or client. \nOptional: run, to download and execute full installer."
    exit 1
fi fi
if [ "$2" == "run" ]; then
    EXTENSION=".*-18.04-.*.run"
fi 

NAME=`wget -O - https://expertlab.gitlab.io/mdf_$1/ |grep  "$EXTENSION"|cut -d ">" -f 2|cut -d "<" -f 1`
set +e
rm -v "$NAME"
set -e
wget https://expertlab.gitlab.io/mdf_$1/$NAME
if [ "$2"  == "run" ]; then
    bash $NAME
    exit $?
fi 
echo "Extracting $NAME"
tar xvf $NAME
if [ "$1" == "server" ]; then
    chmod +x server.bin
    chown --reference=/opt/mdf/server/server.bin server.bin
    mv -v server.bin /opt/mdf/server/server.bin
else
    chmod +x mdf.exe
    chown --reference=/opt/mdf/client/mdf.exe mdf.exe
    mv -v mdf.exe /opt/mdf/client/mdf.exe
fi