# -*- coding: utf-8 -*-
"""Common utilities between client and server"""
import os
import re
import pkgutil
import sys
import numpy as np
import functools
import collections
import itertools
import subprocess
from traceback import format_exc, print_exc
import inspect
from operator import itemgetter
try:
    import _multiprocessing
    import multiprocessing
except ModuleNotFoundError:
    from unittest.mock import MagicMock
    multiprocessing = MagicMock()
try:
    from collections import Iterable
except:
    from collections.abc import Iterable
import hashlib
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
 
py = sys.version_info[0]

enc_options = {}

import zlib
import base64


def clip(value, vmin=None, vmax=None, step=0):
    if vmin is None: vmin = -abs(value)
    if vmax is None: vmax = abs(value)
    if vmax < vmin: vmax = vmin
    elif vmin > vmax: vmin = vmax
    if value < vmin: value = vmin
    if value > vmax: value = vmax
    if step:
        value = step * (value // step)
    return value


def gzdeflate(s, b64=False):
    if type(s) != bytes:
        s = bytes(s, encoding='utf-8')
    compressed_data = zlib.compress(s)  # [2:-4]
    if not b64:
        return compressed_data
    return base64.b64encode(compressed_data)


def gzinflate(s, b64=False):
    if b64:
        s = base64.b64decode(s)
    return zlib.decompress(s)


def unicode_func(value, *a, **k):
    if not isinstance(value, bytes):
        return value
    enc = k.pop('encoding', False)
    if not enc and len(a):
        a = list(a)
        enc = a.pop(0)
    if not enc:
        enc = 'utf8'
    return value.decode(enc, *a, **k)


def is_unicode(s):
    return isinstance(s, str)


def unicode_decode(msg, encoding='utf-8', errors='replace'):
    """python3"""
    if isinstance(msg, str):
        return msg
    else:
        return msg.decode(encoding, errors=errors)

    
def unicode_encode(msg, encoding='utf-8', errors='ignore'):
    if is_unicode(msg):
        return msg.encode(encoding, errors=errors)
    return msg
    

def py2_bytes(s, *a, **k):
    return str(s)


class IterMergeFillValue(object):
    pass


def itermerge(*iterables, **kw):
    fillvalue = kw.get('fillvalue', IterMergeFillValue())
    zipln = itertools.zip_longest(*iterables, fillvalue=fillvalue)
    for rows in zipln:
        for row in rows:
            if row is not fillvalue:
                yield row


def go(cmd, **kw):
    """commands.getstatusoutput clone"""
    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True, **kw)
        if py == 3:
            output = output.decode()
        return 0, output
    except subprocess.CalledProcessError as cpe:
        output = cpe.output
        if py == 3:
            output = output.decode()
        return cpe.returncode, output


from urllib.request import urlopen, quote, unquote
import http.client as httplib
from urllib.parse import urlencode
from io import StringIO, BytesIO
import builtins as exceptions
import pickle
import configparser
DEFAULT_PROTOCOL = 2
pickle.DEFAULT_PROTOCOL = DEFAULT_PROTOCOL
from pickle import dump, dumps, loads
from xmlrpc import client as xmlrpclib
unicode = str
enc_options['encoding'] = 'utf-8' 
enc_options['errors'] = 'ignore'
basestring = str
walk = os.walk

import json


def cloud_query(opt=None):
    opt = opt or {}
    opt['time'] = time()
    if 'cmd' not in opt:
        opt['cmd'] = 'ping'
    url = opt.pop('url', 'https://app.ceramics-genome.ai')
    enc = json.dumps(opt)
    r = urlopen(url=url, data=enc.encode('utf8'), timeout=60)
    return r

    
rename_table = {
    'misura.canon': 'mdf_canon',
    }
    
    
class RenameUnpickler(pickle.Unpickler):

    # https://stackoverflow.com/a/53327348/1645874
    def find_class(self, module, name):
        for k in rename_table:
            if module.startswith(k):
                module = rename_table[k] + module[len(k):]
        return super(RenameUnpickler, self).find_class(module, name)


def load_compat(file_obj, **opt):
    return RenameUnpickler(file_obj, **opt).load()


def loads_compat(tree, **opt):
    file_obj = BytesIO(tree)
    return load_compat(file_obj, **opt)


profiling = True
isWindows = os.name == 'nt'
psutil = False
if isWindows:
    import psutil
        
#############
# TIME SCALING (testing)
# ##
import time as standardTime
time_scaled = multiprocessing.Value('d')
time_scaled.value = 0


def is_time_simulated():
    return time_scaled.value != 0


def time(fixed=-1, scaled=None):
    """If time_scaled is set, return current simulation-scaled time."""
    if fixed >= 0:
        time_scaled.value = -fixed
        return
    elif scaled:
        time_scaled.value = scaled
    ts = time_scaled.value
    if ts > 0:
        return time_step() * time_scaled.value
    if time_scaled.value < 0:
        return -time_scaled.value
    return float(standardTime.time())


utime = time


def sleep(t):
    ts = time_scaled.value
    if ts < 0:
        time_scaled.value = ts - t
        return
    elif ts > 0:
        t = t / ts
    return standardTime.sleep(t)


sh_time_step = multiprocessing.Value('i')


def time_step(set=-1):
    if set < 0:
        return sh_time_step.value
    else:
        sh_time_step.value = set
        return set


def doTimeStep(set=-1):
    t = time_step()
    if set >= 0:
        time_step(set=set)
    else:
        t += 1
        time_step(set=t)
    return t


def limit_freq(t0, t1, maxfreq, ret=False, sleep_func=sleep):
    if not maxfreq:
        if ret:
            return 0, 0
        return 0
    if t1 < 0:
        t1 = time()
    dt10 = t1 - t0
    dt = (1. / maxfreq) - dt10
    dt = min(dt, 10)  # Minimum freq is 0.1!
    s = 0
    if dt > 0:
        s = 0.99 * dt
        (sleep_func or sleep)(s)
    if ret:
        freq = 0
        if dt10 > 0:
            freq = 1. / (dt10)
        return s, freq
    return s


class Decorator(object):
    wrapped_function = None

    def __init__(self, **k):
        self.__dict__.update(k)
        
    def wrapped(self, wrapped_func, *args, **kwargs):
        pass
    
    def __call__(self, func):
        # Save original function to be retrieved by func_args
        self.wrapped_function = func

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return self.wrapped(func, *args, **kwargs)

        wrapper.wrapped_function = func
        return wrapper


class rate_limit(Decorator):
    """Rate limiting a function call"""
    limit = 0  # can be a frequency or an item [key] to read freq from
    target = None  # option name on which h_get will be called for last timestamp
    sleep = None  # sleep function

    def wrapped(self, wrapped_func, wrapped_obj, *args, **kwargs):
        target = self.target or 'call_' + wrapped_func.__name__
        lim = self.limit if isinstance(self.limit, (float, int)) else wrapped_obj[self.limit]
        if lim > 0:
            t0, p = wrapped_obj.h_get(target, -1)
            s = limit_freq(t0, -1, lim, sleep_func=self.sleep)
            if s:
                print('rate_limit', s, wrapped_func, *args, **kwargs)
        return wrapped_func(wrapped_obj, *args, **kwargs)


def camelcase(name):
    name = name.split('_')
    if len(name) == 1:
        return name[0]
    for i in range(1, len(name)):
        name[i] = name[i].capitalize()
    return ''.join(name)


class Void(object):
    pass


class FakeBinary(object):

    """Fake the xmlrpc.Binary behavior when not needing the overhead"""

    def __init__(self, data=''):
        self.data = data


binfunc = FakeBinary


def logprint(*a):
    print(a)


class FakeLogger(object):

    def __getattr__(self, *a):
        return logprint

    def __call__(self, *a, **k):
        print(k, a)


fakelogger = FakeLogger()


def xmlrpcSanitize(val, attr=[], otype=False):
    if otype == 'Profile':
        return binfunc(pickle.dumps(val))
    elif ('Binary' in attr):
        return binfunc(val)
    if isinstance(val, dict):
        r = {}
        for k, v in val.items():
            r[k] = xmlrpcSanitize(v)
        return r
    if isinstance(val, bytes):
        return unicode_func(val)
    if isinstance(val, (np.string_, np.str_, bytes)):
        val = ''.join(map(unicode_func, val[:]))
        return val
    if isinstance(val, np.ndarray):
        return val.tolist()
    if hasattr(val, '__iter__') and not isinstance(val, (dict, str)):
        r = list(xmlrpcSanitize(el) for el in val)
        return r
    if isinstance(val, (np.float64, np.float32)):
        return float(val)
    if isinstance(val, (np.int64, np.int32, np.int8)):
        return int(val)
    return val


class sanitize(Decorator):

    def wrapped(self, wrapped_func, wrapped_obj, *a, **k):
        r = wrapped_func(wrapped_obj, *a, **k)
        return xmlrpcSanitize(r)


def natural_keys(text):
    """Natural ordering key function for list.sort().
    See http://www.codinghorror.com/blog/archives/001018.html"""
    r = [int(s) if re.match(r'([-]?\d+)', s) else s for s in re.split(r'([-]?\d+)', text)]
    return r


def func_args(func):
    """Return a flatten list of function argument names.
    Correctly detect decorated functions"""
    # Detect decorated function
    if hasattr(func, 'wrapped_function'):
        func = func.wrapped_function
    if not hasattr(func, '__call__'):
        return set()
    parameters = inspect.signature(func).parameters
    r = filter(lambda p: p.default != p.empty, parameters.values())
    r = set([p.name for p in r])
    return r


#####
# FILESYSTEM UTILS
#######
goodchars = ['-', '_', ' ']
badchars = []


def validate_filename(fn, good=goodchars, bad=badchars):
    """Purifica un filename da tutti i caratteri potenzialmente invalidi"""
    fn = unicode(fn)
    if py == 2:
        fn = fn.encode('ascii', 'ignore')
    fn = list(fn)

    def check_char(x):
        return (x.isalpha() or x.isdigit()) or (x in good and x not in bad)

    fn = filter(check_char, fn)
    # scarta tutti i caratteri non alfanumerici
    fn = "".join(list(fn))
    return fn


def incremental_filename(original):
    if not os.path.exists(original):
        return original
    ext = ''
    base = original.split('.')
    
    if len(base) > 1:
        ext = '.' + base.pop(-1)
    base = '.'.join(base)
    prenum = base.split('_')
    number = 1
    if len(prenum) > 1 and prenum[-1][0] != '0' and len(prenum[-1]) < 4:
        try: 
            number = int(prenum[-1]) + 1
            prenum.pop(-1)
        except:
            pass
            
    prenum = '_'.join(prenum)
    
    new = prenum + '_' + str(number) + ext
    while os.path.exists(new):
        number += 1
        new = prenum + '_' + str(number) + ext
        
    return new


def iter_cron_sort(top, field=1, reverse=False):
    """
    Return ordered of tuples (path,ctime,size) for all files from `top` folder recursively and ordered by field.
    field 0: name, 1: time, 2: size
    """
    r = []
    for root, dirs, files in os.walk(top):
        for name in files:
            f = os.path.join(root, name)
            s = os.stat(f)
            r.append((f, s.st_ctime, s.st_size))
    r = sorted(r, key=itemgetter(field), reverse=reverse)
    return r


def disk_free(path, unit=1048576.):
    """Calculate free disk space"""
    if isWindows:
        u = psutil.disk_usage(path)
        return u.free / unit, u.total / unit, u.used / unit
    st = os.statvfs(path)
    f = st.f_frsize / unit
    free = st.f_bavail * f
    total = st.f_blocks * f
    used = (st.f_blocks - st.f_bfree) * f
    return free, total, used


def list_file_types(path, ext='.h5'):
    r = []
    for root, dirs, files in os.walk(path):
        f = list(filter(lambda f: f.endswith(ext), files))
        r += list([os.path.join(root, f) for f in f])
    return r


def chunked_upload(upfunc, fp, sigfunc=lambda v: 0):
    name = os.path.basename(fp)
    w = 2000000
    N = 1. * os.stat(fp).st_size / w
    if N < 1:
        N = 1
    f = open(fp, 'rb')
    pf = 100. / N
    for i in range(int(N)):
        dat = f.read(w)
        upfunc(xmlrpclib.Binary(dat), i, name)
        pc = pf * i
        sigfunc(int(pc))
        print('sent %.2f' % (pc))
    ui = -1
    if N == 1:
        ui = 0
    dat = f.read()
    upfunc(xmlrpclib.Binary(dat), ui, name)
    if N == 1:
        upfunc(xmlrpclib.Binary(''), -1, name)
    sigfunc(100)


def flatten(x):
    """Build a flat list out of any iter-able at infinite depth"""
    result = []
    for el in x:
        # Iteratively call itself until a non-iterable is found
        if hasattr(el, "__len__") and not isinstance(el, str):
            flt = flatten(el)
            result.extend(flt)
        else:
            result.append(el)
    return result


def find_nearest_brute(v, x, sort=False):
    """`v`: lista o vettore monodimensionale dove ricercare il valore `x`"""
    v = abs(np.array(v) - np.array(x))
    
    if not sort:
        f = np.where(v == v.min())[0][0]
        return f
    
    return np.argsort(v)


def find_nearest_val(v, t, get=False, seed=None, start=0, end=-1, length=0, callback=lambda *a: 1, position=None):
    """Finds value nearest to `t` in array `v`, assuming v is monotonic.
    Optionally use the `get` function instead of v.__getitem__, in order to retrieve single elements from the array.
    Limit the search in range (start, end)"""
    # TODO: explore builtin bisect module!!!
    if get is False:
        g1 = v.__getitem__
    else:
        g1 = get
    g = g1
    # Take a specific position in the get iterable
    if position is not None:
        g = lambda *a, **k: g1(*a, **k)[position] 
    
    def retval(bi):
        bi = int(bi)
        if position is None:
            return bi
        return bi, g1(bi)
    
    n = length if length else len(v)
    if n <= 1:
        return retval(0)
    start, end = int(start), int(end)
    if end < 0:
        end = n + end
    if end > n:
        end = n - 1
    if start < 0:
        start = n + start
    g0 = g(start)
    g_1 = g(end)
    positive = g_1 > g0
    if (t < g0 and positive) or (t > g0 and not positive):
        return retval(start)
    if (t > g_1 and positive) or (t < g_1 and not positive):
        return retval(end)
    if seed is None:
        i = int(start + (end - start) // 2)  # starting index
    else:
        i = int(seed)
    i = min(end, max(start, i))
    smaller = start  # prev smaller index
    bigger = end  # prev bigger index
    bi = i  # best index
    bd = np.inf  # min delta
    ok = 2  # ping-pong counter
    
    while ok > 0:
        i = int(i)
        c = g(i)  # current delta
        d = t - c  # delta
        b = abs(d)
        if not callback(i, c, d, b):
            return retval(bi)
        # remember best index and delta
        if b < bd:
            bd = b
            bi = i
        # can't be better than that
        if d == 0:
            bi = i
            break
        # bigger d: choose smaller index
        if (d < 0 and positive) or (d > 0 and not positive):
            if i - smaller < 1:
                break
            bigger = i
            # Detect last integer reduction
            if i == smaller + 1:
                i = smaller
                ok -= 1
            else:
                # Half-way reduce towards `smaller`
                i = smaller + (i - smaller) // 2
        # smaller d: choose bigger index
        else:
            if bigger - i < 1:
                break
            smaller = i
            # Detect last integer increase
            if i == bigger - 1:
                i = bigger
                ok -= 1
            else:
                # Half-way increase towards `bigger`
                i += (bigger - i) // 2
    
    return retval(bi)


from scipy import signal


def uniformx(x, y, sorting=0):
    assert len(x) == len(y), 'Lengths mismatch {} {}'.format(len(x), len(y))
    n = int(len(x) / 10)
    direction = np.sign(np.sign(np.diff(x[:n])).mean())
    x1, y1, vi = [], [], []
    xmax = -direction * abs(max(x))
    for i, xi in enumerate(x):
        if direction == np.sign(xi - xmax):
            xmax = xi
            x1.append(xi)
            y1.append(y[i])
            vi.append(i)
    if sorting and direction != sorting:
        x1 = x1[::-1]
        y1 = y1[::-1]
        vi = vi[::-1]
    ret = x1, y1, vi
    if isinstance(x, np.ndarray):
        ret = list(map(np.array, ret))
    return ret


class InconsistentTimeValue(BaseException):
    pass


def file_hash(fname):
    with open(fname, "rb") as f:
        h = hashlib.blake2b()
        for chunk in iter(lambda: f.read(4096), b""):
            h.update(chunk)
    return h.hexdigest()


def stat_dict(path):
    stat = os.stat(path)
    flt = filter(lambda k: k.startswith('st_'), dir(stat))
    return {k:getattr(stat, k) for k in flt}


def ensure_monotonic_time(tT, T):
    discard = len(tT)
    tT1, idx = np.unique(tT, return_index=True)
    discard -= len(idx)
    if discard:
        scaled = max(tT) * (T + max(tT))
        rows, ridx = np.unique(scaled + tT, return_index=True)
        sd = set(idx).symmetric_difference(set(ridx))
        if sd:
            raise InconsistentTimeValue(len(sd))
            
        tT = tT1
        T = T[idx]
    return tT, T, discard


# Fraser-Suzuki
# https://perso.ens-rennes.fr/~mwerts/skewed-gaussian-fraser-suzuki.html
def _skew_gauss_vec(x, A, x0, w, b):
    """vectorised version of skewed Gaussian, called by skew_gauss"""
    ndeps = np.finfo(x.dtype.type).eps
    lim0 = 2.*np.sqrt(ndeps)
    # Through experimentation I found 2*sqrt(machine_epsilon) to be
    # a good safe threshold for switching to the b=0 limit
    # at lower thresholds, numerical rounding errors appear
    if (abs(b) <= lim0):
        sg = A * np.exp(-4 * np.log(2) * (x - x0) ** 2 / w ** 2)
    else:
        lnterm = 1.0 + ((2 * b * (x - x0)) / w)
        sg = np.zeros_like(lnterm)
        sg[lnterm > 0] = \
            A * np.exp(-np.log(2) * (np.log(lnterm[lnterm > 0]) / b) ** 2)
    return sg


def skew_gauss(x, A, x0, w, b):
    """Fraser-Suzuki skewed Gaussian.

    A: peak height, x0: peak position,
    w: width, b: skewness"""
    if type(x) == np.ndarray:
        sg = _skew_gauss_vec(x, A, x0, w, b)
    else:
        x = float(x)
        ndeps = np.finfo(type(x)).eps
        lim0 = 2.*np.sqrt(ndeps)
        if (abs(b) <= lim0):
            sg = A * np.exp(-4 * np.log(2) * (x - x0) ** 2 / w ** 2)
        else:
            lnterm = 1.0 + ((2 * b * (x - x0)) / w)
            if (lnterm > 0):
                sg = A * np.exp(-np.log(2) * (np.log(lnterm) / b) ** 2)
            else:
                sg = 0
    return sg


def derive(v, method='Middle', order=1):
    """Derive one time an array, always returning an array of the same length.
    method = Middle, Right, Left"""
    if method == 'Middle':
        return np.gradient(v, order)
    d = np.diff(v, order)
    if method == 'Right':
        app = np.array([d[-1]] * order)
        d = np.concatenate((d, app))
        return d
    app = np.array([d[0]] * order)
    d = np.concatenate((app, d))
    return d


def xyderive(x, y, order=1, method='Middle'):
    """Compute order-th derivative of `y` with respect to `x`"""
    x = derive(x, method)
    for i in range(order):
        y = derive(y, method)
        y = y / x
    return y


def smooth(x, window0=10, method='hanning', recurse=0, beta=None, half=0):
    """method='flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'kaiser','gaussian', 'exponential'"""
    window = window0
    if half:
        window *= 2
    s = np.r_[2 * x[0] - x[window - 1::-1], x, 2 * x[-1] - x[-1:-window:-1]]
    # print(len(s))
    if method == 'flat':  # moving average
        w = np.ones(window, 'd')
    elif method == 'kaiser':
        w = np.kaiser(window, beta)
    elif method == 'gaussian':
        w = signal.windows.gaussian(window, beta)
    elif method == 'exponential':
        w = signal.windows.exponential(window, tau=beta)
    elif method in ('fraser', 'suzuki', 'skewed'):
        w = np.arange(window, dtype=float)
        position, width, skewness = beta
        assert 0 <= position <= 1
        w = skew_gauss(w, 1, window * position, width, skewness)
    elif beta != None:
        raise BaseException("Invalid parameter beta for method")
    else:
        w = eval('np.' + method + '(window)')
    if half < 0:
        w[window0:] = 0
    elif half > 0:
        w[:window0] = 0
    sum = w.sum()
    if sum > 0:
        y = np.convolve(w / sum, s, mode='same')
        y = y[window:-window + 1]
    else:
        y = x.copy()
    if recurse > 0:
        y = smooth(y, window0, method, recurse=recurse - 1, beta=beta, half=half)
    return y


from scipy.signal import butter, lfilter


def butter_bandpass(lowcut, highcut, fs, order=5, btype='band'):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    if btype.startswith('band'):
        b, a = butter(order, [low, high], btype=btype)
    elif btype == 'lowpass':
        b, a = butter(order, high, btype=btype)
    else:
        b, a = butter(order, low, btype=btype)
        
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5, invert=False):
    btype = 'band'
    if lowcut <= 0:
        btype = 'lowpass'
    elif highcut > fs:
        bytpe = 'highpass'
    # Invert the meaning
    if invert:
        btype = {'h': 'lowpass', 'l':'highpass', 'b': 'bandstop'}[btype[0]]
    b, a = butter_bandpass(lowcut, highcut, fs, order=order, btype=btype)
    y = lfilter(b, a, data)
    return y


def _update_filter(new, old):
    """Add new filter to old filter in old array indexes"""
    j = 0
    nj = 0
    n = len(new)
    print('start', new, old)
    for o in old:
        if o >= j:
            j += o - j + 1
        
        while nj < n - 1 and new[nj] + 1 < j: 
            nj += 1
        
        if new[nj] + 1 < j:
            break
            
        new[nj:] += 1
        
    return np.sort(np.concatenate((old, new)))


def smooth_discard(x, percent=10., drop='absolute', passes=1, **kw):
    y = smooth(x, **kw)
    d = abs(x - y)
    if drop == 'absolute':
        # Delete all elements which exceeds the stdev by percent%
        m = (d.mean() + d.std()) * (100 + percent) / 100.
        s = np.where(d > m)[0]
        n = None  # take all
    elif drop == 'relative':
        s = np.argsort(d)
        # Delete most distant percent% elements
        n = -int(len(s) * percent / 100.)
    else:
        raise BaseException('Unknown parameter drop: ' + drop)
     
    if not len(s):
        print('smooth_discard: not filtering', drop, percent, passes)
        return y, x, s
    # Delete selected far points
    s = s[n:]
    
    y = np.delete(y, s)
    # Multi-pass filtering
    if passes > 1:
        print('smooth pass', passes, len(y), len(s))
        y, x1, s1 = smooth_discard(y, percent=percent, drop=drop, passes=passes - 1, **kw)
        # Concatenate filters
        if len(s) and len(s1):
            s = _update_filter(s1, s)
    # Delete raw only in the end
    else:
        x = np.delete(x, s) 
    # Smoothed filtered, raw filtered, filter
    return y, x, s


def toslice(v):
    """Recursive conversion of an iterable into slices"""
    if isinstance(v, slice):
        r = [v.start, v.stop, v.step]
        for i in (0, 1, 2):
            if not isinstance(r[i], (int, type(None))):
                r[i] = int(r[i])
        return slice(*r)
    if not isinstance(v, Iterable):
        if not isinstance(v, slice):
            v = int(v)
        return v
    r = []
    isSlice = True
    for i, el in enumerate(v):
        if isinstance(el, Iterable):
            el = toslice(el)
            isSlice = False
        if isinstance(el, slice):
            isSlice = False
        r.append(el)
    if isSlice == True:
        return toslice(slice(*r))
    return tuple(r)


def decode_cool_event(event):
    """Decode a ">cool,temperature,timeout" thermal cycle event"""
    if not event.startswith('>cool'):
        return False
    event = event.split(',')
    T = float(event[1])
    if len(event) > 2:
        timeout = float(event[2])
    return T, timeout


def decode_checkpoint_event(event):
    if not event.startswith('>checkpoint'):
        return False
    event = event.split(',')
    delta = float(event[1])
    if len(event) > 2:
        timeout = float(event[2])
    return delta, timeout


def power_event_duration(ev):
    if isinstance(ev, str):
        ev = ev.split(',')
    return float(ev[2]) * int(ev[3]) + float(ev[4])


move_openclose_wait = 60


def move_event_duration(ev):
    if isinstance(ev, str):
        ev = ev.split(',')
    if len(ev) == 2:
        ev.append(0)
    return float(ev[2]) + move_openclose_wait


def mancool_event_duration(ev):
    if isinstance(ev, str):
        ev = ev.split(',')
    return float(ev[1])

    
def event_duration(ev):
    if ev.startswith('>power'):
        return power_event_duration(ev)
    if ev.startswith('>move'):
        return move_event_duration(ev)
    if ev.startswith('>mancool'):
        return mancool_event_duration(ev)
    return 0


def next_point(crv, row, delta=1, events=False,):
    """Search next non-event point in thermal cycle curve `crv`.
    Starts by checking `row` index, then adds `delta` (+-1),
    until proper value is found.
    events allows *any* event if True, or only events in a list if iterable
    (events='>a,>b,>c' or ['>d',>e'])"""
    N = len(crv)
    if row < 0:
        row += N
        row %= N
    only_events = isinstance(events, (basestring, list, tuple))
    while row >= 0 and row < N:
        ent = crv[row]
        c = isinstance(ent[1], basestring)
        if not c:
            if only_events:
                row += delta
                continue
            break
        # Found an event
        if not events:
            row += delta
            continue
        if only_events:
            if ent[1].split(',')[0] in events:
                break
        elif events:
            break
        row += delta
        continue
    # NOT FOUND
    if row < 0:
        return -1, False
    if row >= N:
        return -1, False

    return row, ent


class ConcurrentInitializationError(BaseException):
    pass


class initializeme(Decorator):
    target = 'initializing'
    repeatable = False
    attribute = False

    def wrapped(self, wrapped_func, wrapped_obj, *args, **kwargs):
        repeatable = self.repeatable
        target = self.target
        if self.attribute:
            value = getattr(wrapped_obj, target)
            setter = functools.partial(setattr, wrapped_obj, target)
        else:
            value = wrapped_obj[target]
            setter = functools.partial(wrapped_obj.__setitem__, target)
        log = getattr(wrapped_obj, 'log', logging)
        if value and not repeatable:
            msg = f'{target} already set, cannot exec'
            log.error(msg, wrapped_func)
            raise ConcurrentInitializationError(msg)
        try:
            setter(True)
            r = wrapped_func(wrapped_obj, *args, **kwargs)
            setter(False)
            return r
        except KeyboardInterrupt:
            raise KeyboardInterrupt()
        except:
            log.error('initializeme: exc calling', target,
                      wrapped_func, args, kwargs, format_exc())
            raise
        finally:
            setter(False)
        return False


class lockme(Decorator):
    """Decorator to lock/unlock method execution.
    The class having its method decorated must expose
    a _lock object compatible with threading.Lock."""
    timeout = 5
    
    def wrapped(self, wrapped_func, wrapped_obj, *args, **kwargs):
        if wrapped_obj._lock is False:
            return wrapped_func(wrapped_obj, *args, **kwargs)
        wrapped_obj._lockme_error = False
        wrapped_obj._lock.acquire(timeout=self.timeout)
        try:
            return wrapped_func(wrapped_obj, *args, **kwargs)
        except KeyboardInterrupt:
            raise KeyboardInterrupt()
        except:
            print('lockme: exc calling', wrapped_obj, wrapped_func,
                  args, kwargs)
            print_exc()
            wrapped_obj._lockme_error = (wrapped_func, args, kwargs, format_exc())
        finally:
            try:
                wrapped_obj._lock.release()
            except:
                pass


class unlockme(Decorator):
    """Decorator to finally unlock after method execution.
    Useful if locking must be delayed.
    The class having its method decorated must expose
    a _lock object compatible with threading.Lock."""

    def wrapped(self, wrapped_func, wrapped_obj, *args, **kwargs):
        if wrapped_obj._lock is False:
            return wrapped_func(wrapped_obj, *args, **kwargs)
        try:
            return wrapped_func(wrapped_obj, *args, **kwargs)
        finally:
            try:
                wrapped_obj._lock.release()
            except:
                pass


class retry(Decorator):
    times = None
    hook = None

    def wrapped(self, wrapped_func, *args, **kwargs):
        # If retry was defined as None,
        # it is expected as a property of the first argument (the func's
        # obj).
        retry = self.times
        if retry is None:
            retry = args[0].retry
        times = retry
        retry0 = retry
        while True:
            try:
                r = wrapped_func(*args, **kwargs)
                return r
            except KeyboardInterrupt:
                raise
            except:
                print('retry', retry0 - retry, wrapped_func, times, 'pid:' + str(os.getpid()))
                retry -= 1
                if retry <= 0:
                    print('End retry')
                    raise
                # Call the retry hook
                if self.hook is not None:
                    self.hook(
                        wrapped_func, args, kwargs, sys.exc_info(), times - retry)


import cProfile
import pstats

profile_path = '/tmp/mdf/profile/'


def start_profiler(obj):
    obj.__p = cProfile.Profile()
    obj.__p.enable()


def stop_profiler(obj):
    out = '{}{}_{}.prf'.format(
        profile_path, obj.__class__.__name__, str(id(obj)))
    print('PROFILING STATS FOR', out)
    s = pstats.Stats(obj.__p)
    s.sort_stats('cumulative')
    s.dump_stats(out)
    obj.__p.disable()


prf_uid = -1


class profile(Decorator):

    def wrapped(self, wrapped_func, wrapped_obj, *a, **k):
        global prf_uid
        prf_uid += 1
        p = cProfile.Profile()
        try:
            fp = getattr(wrapped_obj, '__getitem__', {'fullpath': ''}.get)('fullpath')
        except:
            fp = ''
        name = wrapped_func.__name__
        if isinstance(wrapped_obj, object):
            name = wrapped_obj.__class__.__name__ + '.' + name
        if not os.path.exists(profile_path):
            os.makedirs(profile_path)
        out = '{}{}_{}_{}.prf'.format(profile_path,
                                      name,
                                      fp.replace('/', '_'),
                                      prf_uid)
        print('START PROFILING STATS FOR', wrapped_func.__name__,
              ' AT ', repr(wrapped_obj), out)
        p.enable()
        r = wrapped_func(wrapped_obj, *a, **k)
        s = pstats.Stats(p)
        s.sort_stats('cumulative')
        s.dump_stats(out)
        p.disable()
        print('END PROFILING STATS FOR', wrapped_func.__name__,
              ' AT ', repr(wrapped_obj), out)
        return r


def from_seconds_to_hms(seconds):
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return "%d:%02d:%02d" % (hours, minutes, seconds)


def ensure_directory_existence(path):
    directory = os.path.dirname(path)
    if not os.path.exists(directory):
        os.makedirs(directory)


# LOCALE CONTEXT MANAGER
from contextlib import contextmanager
import locale
import threading
LOCALE_LOCK = threading.Lock()


@contextmanager
def setlocale(name):
    """Temporary locale context manager. For datetime.strptime with Qt
    From: http://stackoverflow.com/a/24070673/1645874"""
    with LOCALE_LOCK:
        saved = locale.setlocale(locale.LC_ALL)
        try:
            yield locale.setlocale(locale.LC_ALL, name)
        finally:
            locale.setlocale(locale.LC_ALL, saved)
          

from datetime import datetime  


def decode_datetime(val, format='%a %b %d %H:%M:%S %Y'):
    # Wed Nov 11 18:35:36 2015
    with setlocale('C'):
        ret = datetime.strptime(val, format)
    return ret


def filter_calibration_filenames(filenames):
    return [filename for filename in filenames if not '/calibration/' in filename.lower()]


def only_hdf_files(filenames):
    return [filename for filename in filenames if filename.endswith('.h5')]


from random import random


class SharedProcessResources(object):
    
    def __init__(self):
        self.res = []
        self.pid = multiprocessing.current_process().pid
        self.name = '{}-{}'.format(self.pid, random())
    
    def register(self, setter, *args, **kwargs): 
        if os.name != 'nt':
            return
        print('Register', self.name)   
        self.res.append((setter, args, kwargs))
    
    def __call__(self):
        if os.name != 'nt':
            return
        t0 = standardTime.time()
        print('Restoring', self.name, len(self.res))
        for (setter, args, kwargs) in self.res:
            setter(*args, **kwargs)
        print('RESTORED', 1000 * (standardTime.time() - t0))


sharedProcessResources = SharedProcessResources()
    
############
# MULTIPROCESSING
#


def check_pid(pid):
    """ Check For the existence of a unix pid. """
    if pid <= 0:
        return False
    if isWindows:
        return psutil.pid_exists(pid)
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

    
def join_pid(pid, timeout=10, label=''):
    """Wait until process `pid` finishes."""
    if pid <= 0:
        return True
    t0 = time()
    while time() - t0 <= timeout:
        multiprocessing.active_children()
        if not check_pid(pid):
            return True
        sleep(.5)
        print(label, 'Still waiting for subprocess', pid)
    return False


def kill_pid(pid):
    """Terminate process `pid`"""
    if pid <= 0:
        return False
    if isWindows:
        # TODO: test linux and unify
        p = psutil.Process(pid)
        for sub in p.children():
            sub.terminate()
        p.kill()
        return True
    try:
        os.kill(pid, 9)
    except OSError:
        return False
    else:
        return True


def delete_locking_process(path):
    print('delete_locking_process', path)
    for i in range(3):
        s, o = go('lslocks -i -oPID| grep "{}"'.format(path))
        if s != 0:
            print('delete_locking_process OK for', path, i)
            return False
        
        for i, pid in enumerate(o.splitlines()):
            if i == 0:
                continue
            print('delete_locking_process', path, pid, i)
            p = int(pid)
            kill_pid(p)
        sleep(1)
    return pids


import concurrent.futures
_executor = False


def executor():
    global _executor
    if not _executor:
        _executor = concurrent.futures.ThreadPoolExecutor()
    return _executor


def h5repack(path):
    st, out = go('h5repack --version')
    if st != 0:
        msg = 'Please install hdf5-tools to repack'
        logging.warning(msg)
        return st, msg
    ver = out.split(' ')[-1]
    st, out = go('h5repack "{}" "{}.repack"'.format(path, path))
    if st != 0:
        msg = 'h5repack error: ' + out
        logging.error(msg)
        return st, msg
    # Overwrite
    st, out = go('mv "{}.repack" "{}"'.format(path, path))
    return 0, ver


def is_excluded(modname, exclude):
    names = modname.split('.')
    if names[0] in exclude:
        return True
    for e in exclude:
        if modname.startswith(e):
            return True
    return False


def iter_package(package, exclude=[]):
    prefix = package.__name__ + "."
    exclude = set(exclude)
    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix):
        if is_excluded(modname, exclude):
            print('skip: excluded', modname, exclude)
            continue
        print("Found submodule %s (is a package: %s)" % (modname, ispkg))
        try:
            module = __import__(modname, fromlist="dummy")
            print("Imported", module)
        except:
            print('Could not import', modname)
            continue
        yield module, ispkg
