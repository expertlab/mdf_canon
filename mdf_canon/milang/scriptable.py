#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Mdf Language or Mini Language.
Secure minimal Python language subset for conditional evaluation of numerical datasets."""

from .dataenv import DataEnvironment
from .objenv import InterfaceEnvironment
from .milang import MiLang
import re

script_registry = {}
_script_registry = None


def compile_script_registry():
    global _script_registry
    _script_registry = {}
    for k, v in script_registry.items():
        k1 = re.compile(k)
        _script_registry[k] = k1, v


def script_needs_update(opt, path):
    """Always override any saved script"""
    if not script_registry:
        # print('script_registry is empty')
        return None
    if not _script_registry:
        compile_script_registry()
    matched = False
    for k, (reg, scr) in _script_registry.items():
        m = reg.match(path)
        if not m:
            continue
        if opt['factory_default'] != scr:
            print('Script default was outdated', opt['handle'], path, k, reg)
            opt['factory_default'] = scr
        if opt['current'] != scr:
            opt['current'] = scr
            print('Script was updated:', opt['handle'], path, k, reg)
            return True
        matched = True
        break
    # print('Script does not need update:', opt['handle'], path, matched)
    return False


class Scriptable(object):

    """A configuration object fragment which can contain and execute Script-type options"""
    
    def __init__(self):
        self.reset()
        
    def reset(self):
        self.scripts = []
        self.end_scripts = []
        self.always_scripts = []
        self.all_scripts = {}
        self.env = DataEnvironment()
        
    def close(self):
        self.reset()
        self.env = False
        
    def check_script_registry(self, opt, path):
        up = script_needs_update(opt, path)
        if up:
            self.log.debug('Updating script source:', path)
            self.sete(opt['handle'], opt)
            return True
        return False
    
    def compile_scripts(self, hdf=False):
        """Compile all Script-type options,
        assigning them to the appropriate container dictionary."""
        self.reset()
        self.script_env = InterfaceEnvironment(self)
        cold = isinstance(self.desc, dict)
        if cold and (_script_registry is None):
            compile_script_registry()
        if hdf is not False:
            self.env.hdf = hdf
            self.script_env.hdf = hdf
        fp = self['fullpath']
        self.log.debug('compile_scripts', fp)
        for handle, opt in self.describe().items():
            if opt['type'] != 'Script':
                continue
            if not opt.get('enabled', True):
                continue
            if cold:
                self.check_script_registry(opt, fp + handle)
            p = opt['flags'].get('period', None)
            exe = MiLang(
                opt['current'], env=self.env, script_env=self.script_env)
            exe.period = p
            exe.handle = handle
            meta = opt.get('parent', False)
            if meta and (meta in self) and (self.getattr(meta, 'type') == 'Meta'):
                exe.meta = meta
            self.add_script(exe)

    def add_script(self, exe):
        if not exe.code:
            self.log.error('Compilation failed', exe.handle, exe.error)
            return False
        # Insert into all_scripts
        self.all_scripts[exe.handle] = exe
        # Inserting into frequency containers
        # If no period is defined, the script will be executed at
        # characterization time (about every 30s)
        if exe.period == None:
            self.scripts.append(exe.handle)
        # If an >=0 period is defined, the script is always executed
        # TODO: implement custom execution freq based on this number
        elif exe.period >= 0:
            self.always_scripts.append(exe.handle)
        # If period <0, the script will be executed only at the end of the test
        elif exe.period < 0:
            self.end_scripts.append(exe.handle)
        return True

    def execute_scripts(self, ins=None, period=False):
        """Execute script contained in `scripts` dictionary, passing `ins` onto eval()"""
        if not ins:
            ins = self.parent()
        r = True
        if period == False:
            scripts = self.scripts
        elif period == 'end':
            scripts = self.end_scripts
        elif period == 'always':
            scripts = self.always_scripts
        elif period == 'all':
            scripts = self.all_scripts

        scripts = set(scripts)
        for handle in scripts:
            r1 = self.execute_script(handle, ins)
            r = r and r1
        return r

    def execute_script(self, handle, ins):
        exe = self.all_scripts[handle]
        en = exe.script_env.obj.getFlags(handle).get('enabled', True)
        if not en:
            self.log.debug('Disabled script, skipping:', handle)
            return True
        if ins:
            exe.set_env_outFile(ins.outFile)
        u = exe.eval(self, ins=ins)
        if u:
            self.log.debug('execute_script done:', handle, u,
                       exe.env.time, exe.env.temp, exe.env.value)
        return u

    def validate_script(self, handle):
        """Validates a script option by name"""
        if handle not in self.script:
            self.log.info('Not a Script option!', handle)
            return False
        exe = MiLang(self[handle], self.env)
        if not exe.code:
            self.log.error(
                'Compilation failed', handle, self[handle], exe.error)
            return False
        return True

    xmlrpc_validate_script = validate_script

    def validate_script_text(self, text):
        """Validates a script text"""
        exe = MiLang(text, self.env)
        if not exe.code:
            self.log.error(
                'Custom text compilation failed', exe.error, exe.error_line, exe.error_col)
            return exe.error, exe.error_line, exe.error_col
        return "", -1, -1

    xmlrpc_validate_script_text = validate_script_text

    ######
    # Instrument-related methods

    def distribute_scripts(self, hdf=False):
        """Compile Samples and Measure scripts."""
        if not hasattr(self, 'measure'):
            self.log.error('Script distribution makes no sense on this object')
            return False
        if hdf is False:
            hdf = self.outFile
        self.log.debug('distributing scripts', repr(self.samples))
        self.measure.compile_scripts(hdf)
        for smp in self.samples:
            if not smp:
                self.log.error('Invalid sample', smp, self.samples)
                continue
            self.log.debug('distributing sample scripts', smp)
            smp.compile_scripts(hdf)
            
    def xmlrpc_distribute_scripts(self, *a, **k):
        return self.distribute_scripts(*a, **k)

    def characterization(self, period=False):
        """Execute scripts."""
        if not hasattr(self, 'measure'):
            self.log.error('Characterization makes no sense on this object')
            return False
        for smp in self.samples:
            smp.execute_scripts(self, period=period)
        self.measure.execute_scripts(self, period=period)
