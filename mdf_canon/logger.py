# -*- coding: utf-8 -*-
"""Generalized logging utilities"""
import os
import logging
import functools
from datetime import datetime
from time import time

fromtimestamp = datetime.fromtimestamp
root_log = logging.getLogger()
root_log.setLevel(-1)
formatter = logging.Formatter(u"%(levelname)s: %(asctime)s %(message)s")
for h in root_log.handlers:
    h.setFormatter(formatter)

    
def concatenate_message_objects(*msg):
    # Ensure all message tokens are actually strings
    # (avoid "None" objects pollute the buffer!)
    msg = list(msg)
    for i, e in enumerate(msg):
        if isinstance(e, str):
            msg[i] = e.encode('utf-8', 'replace').decode()
        elif isinstance(e, bytes):
            msg[i] = e.decode('utf8', errors='replace')
        else:
            msg[i] = repr(e)
    return msg

    
def formatMsg(*msg, **po):
    """Format the message for pretty visualization"""
    t = po.get('t', time())
    st = fromtimestamp(t).strftime('%x %X.%f')
    # Owner e priority
    o = po.get('o', '*')
    p = po.get('p', 0)
    pid = po.get('pid', os.getpid())
    if p == None:
        p = logging.NOTSET
    if o == None or o == '':
        own = ' '
        o = ''
    elif pid:
        own = ' (%s%i): ' % (o, pid)
    else:
        own = ' (%s): ' % (o)
    msg = concatenate_message_objects(*msg)
    smsg = ' '.join(tuple(msg))
    smsg = smsg.splitlines()
    pmsg = '%s%s' % (own, smsg[0])
    if len(smsg) > 1:
        for l in smsg[1:]:
            pmsg += '\n\t | %s' % l
    return t, st, p, o, msg, pmsg


def justPrint(*msg, **po):
    t, st, p, o, msg, pmsg = formatMsg(*msg, **po)
    print(str(t) + u' ' + st + u' ' + pmsg)
    return pmsg


def toLogging(*msg, **po):
    """Send log to standard python logging library"""
    t, st, p, o, msg, pmsg = formatMsg(*msg, **po)
    return logging.log(po.get('p', 10), pmsg)


message_buffer = 10

from collections import defaultdict


def convert_last_log(log):
    r = defaultdict(list)
    for msg, po in log:
        t, st, p, o, msg, pmsg = formatMsg(*msg, **po)
        smsg = ' '.join(tuple(msg))
        r[o].append([t, p, smsg])
    return dict(r)


class BaseLogger(object):

    """Interface for standard logging functions definition and routing."""
    __buffer = -1
    __prefix = False
    
    @property
    def _buffer(self):
        if self.__buffer < 0:
            return message_buffer
        else:
            return self.__buffer
    
    def __init__(self, log=justPrint):
        self._log = log
        self._last_msg = []
        self._debounce = {}
        
    def set_prefix(self, new=False):
        self.__prefix = new
        
    def debounce(self, msg, opt):
        """Decide if to debounce a message. 
        Returns True to debounce, False to log"""
        debounce = opt.pop('debounce', 0)
        escalate = opt.pop('escalate', 0)
        if (debounce + escalate) == 0:
            return False
        label = opt.pop('label', False)
        if not label:
            label = '_'.join(concatenate_message_objects(*msg))
        old_time, old_count = self._debounce.get(label, (0, 0))
        delta = opt['t'] - old_time
        # Raise log level depending on log frequency
        
        if escalate:
            level = opt.get('p', 10)
            opt['p'] = int(level + escalate / delta)
        
        ret = True  # Ignore message
        if debounce and delta > debounce:
            if old_count > 1:
                self.debug('Debounced:', label, old_count)
            old_count = -1
            old_time = opt['t']
            ret = False  # Log message
        self._debounce[label] = [old_time, old_count + 1]
        return ret
        
    def log(self, *msg, **po):
        if self.__prefix:
            msg = [self.__prefix] + list(msg)
        po['t'] = po.get('t', time())
        self._last_msg.append((msg, po))
        if len(self._last_msg) > self._buffer:
            self._last_msg.pop(0)
        if self.debounce(msg, po):
            return False
        return self._log(*msg, **po)
    
    def replay(self, n=0):
        if n <= 0:
            n = len(self._last_msg)
        msg = []
        for m, po in self._last_msg[-n:]:
            msg.append(formatMsg(*m, **po)[-1])
        return '\n'.join(msg)
    
    def clear_buffer(self):
        self._last_msg = []

    def __call__(self, *msg, **po):
        return self.log(*msg, p=po.get('p', logging.DEBUG))

    def debug(self, *msg, **k):
        k['p'] = logging.DEBUG
        return self.log(*msg, **k)

    def info(self, *msg, **k):
        k['p'] = logging.INFO
        return self.log(*msg, **k)

    def warning(self, *msg, **k):
        k['p'] = logging.WARNING
        return self.log(*msg, **k)

    def error(self, *msg, **k):
        k['p'] = logging.ERROR
        return self.log(*msg, **k)

    def critical(self, *msg, **k):
        k['p'] = logging.CRITICAL
        return self.log(*msg, **k)


class SubLogger(BaseLogger):

    """Implicit owner logging."""

    def __init__(self, parent):
        # TODO: pass DirShelf option path as log, instead of using a
        # parent.desc.
        BaseLogger.__init__(self, self._log)
        self.parent = parent

    def _log(self, *msg, **po):
        p = po.get('p', 0)
        msg = list(msg)
        for i, e in enumerate(msg):
            msg[i] = str(e)
        smsg = u' '.join(tuple(msg))
        if self.parent and self.parent.desc:
            self.parent.desc.set_current('log', [p, smsg])
        return smsg

    
from unittest import mock


class LogMock(mock.Mock):

    def __init__(self, *a, side_effect=justPrint, **k):
        return super().__init__(*a, side_effect=side_effect, **k)
    
    def _get_child_mock(self, **k):
        r = super()._get_child_mock(**k)
        r.side_effect = self.side_effect
        return r


def get_module_logging(owner):
    logfunc = functools.partial(toLogging, o=owner, pid=False)
    r = BaseLogger(log=logfunc)
    return r


def set_log_file(log_file=False, logsize=10e6, count=10):
    import logging.handlers
    os.makedirs(os.path.dirname(log_file), exist_ok=True)
    rotating_file_handler = logging.handlers.RotatingFileHandler(log_file, encoding="utf8", maxBytes=logsize,
                                                                     backupCount=count)
    rotating_file_handler.setFormatter(formatter)
    root = logging.getLogger()
    print('set_log_file HANDLERS:', root.handlers)
    root.addHandler(rotating_file_handler)
    sa = logging.StreamHandler()
    sa.setFormatter(formatter)
    root.addHandler(sa)
    return rotating_file_handler


global Log, log
Log = BaseLogger(log=toLogging)
log = Log.log

