# -*- coding: utf-8 -*-
"""Redis-based FileBuffer implementation"""
import os
from struct import pack, unpack
from collections import defaultdict
import shutil
import redis
from mdf_canon.csutil import dumps, loads, go, time, is_time_simulated

redis_db_number = 0

# In-memory blobs
blobs_size = 1e3
blobdir = '/dev/shm/mdf/blobs/'

# Disk blobs
blobdisk = '/tmp/mdf/blobs/'
blobdisk = False
disk_interval = 1  # s, integer

sep = '/'

redis_kw = {'host':os.environ.get('REDIS_HOST', 'localhost'), }


def reset_redis_io():
    global redis_kw, redis_cache
    redis_cache = {}
    shutil.rmtree(blobdir, ignore_errors=1)
    os.makedirs(blobdir, exist_ok=True)
    print('Created blobdir', blobdir)
    if blobdisk:
        shutil.rmtree(blobdisk, ignore_errors=1)
        os.makedirs(blobdisk, exist_ok=True)
        print('Created blobdisk', blobdisk)
        
# no performance increase
# redis_kw = {'unix_socket_path':'/var/run/redis/redis-server.sock',
#            'retry_on_timeout':True}


def redis_cli():
    return redis.Redis(db=redis_db_number, **redis_kw)


redis_cache = {}


def cli():
    tid = os.getpid()  # *threading.get_ident()
    if tid not in redis_cache:
        print('new redis for thread', tid)
        redis_cache[tid] = redis_cli()
    return redis_cache[tid]
    
#########################
# ENCODE / DECODE MAPS


RE_PICKLE = 1
RE_BYTE = 2
RE_INT = 3
RE_STRING = 4
RE_FLOAT = 5
RE_PATH = 8

typecode = defaultdict(lambda:RE_PICKLE)
codetype = defaultdict(lambda:'pickle')
codeload = defaultdict(lambda:loads)
typedump = defaultdict(lambda:dumps)

z_types = [type(0), type(b''), type(''), type(0.0001)]
z_codes = [RE_INT, RE_BYTE, RE_STRING, RE_FLOAT]
z_load = [lambda a: int(a.decode()), lambda a: a, lambda a: a.decode(), lambda a: unpack('d', a)[0]]
z_dump = [lambda a: str(a).encode(), lambda a: a, lambda a: a.encode(), lambda a: pack('d', a)]

for (t, code, fload, fdump)  in zip(z_types, z_codes, z_load, z_dump):
    typecode[t] = code
    codetype[code] = t
    codeload[code] = fload
    typedump[t] = fdump

    
def blob_path(path, t, val, retype=RE_PICKLE | RE_PATH):
    return '{}{}:{}:{}:{}'.format(blobdir, path[1:].replace('/', '.'), t, len(val), int(retype))


def blob_time(path):
    return float(path.split(':')[1])


def blob_write(path, t, val, retype):
    path = blob_path(path, t, val, retype)
    f = open(path, 'wb')
    f.write(val)
    f.close()
    return path.encode()


offloaded_size_marker = '_'


def convert_ram_name_to_disk(rampath, disk_interval=disk_interval):
    """Round the timestamp, remove the size, folder is blobdisk"""
    bp = rampath.split(':')
    # Round the timestamp
    tstamp = str(disk_interval * int(float(bp[1]) // disk_interval))
    # Keep the rest of the path
    dest = blobdisk + ':'.join([bp[0], tstamp, offloaded_size_marker] + bp[3:])
    return dest


TIME_ZERO = time()
TIME_MIN = 0
TIME_MAX = 10 * TIME_ZERO


def time_dump(t): return pack('d', t) + b':'


TIME_LEN = len(time_dump(TIME_ZERO))


def time_read(v): return v[TIME_LEN:]


def data_read(data):
    r = time_read(data)
    code = r[0]
    isblob = (code == code | RE_PATH)
    if isblob:
        code -= RE_PATH
    # Select loader function2
    fload = codeload[code]
    # Load from blob
    if isblob:
        path = r[1:].decode()
        if blobdisk and not os.path.exists(path):
            path = convert_ram_name_to_disk(path)
        f = open(path, 'rb')
        r = f.read()
        f.close()
    else:
        r = r[1:]
    return fload(r)


def data_write(key, t, val, blobs_size=blobs_size):
    blpath = ''
    vt = type(val)
    val = typedump[vt](val)
    PRE = typecode[vt]
    if len(val) > blobs_size:
        retype = PRE | RE_PATH
        blpath = blob_write(key, t, val, retype)
        val = bytes([retype]) + blpath
    else:
        val = bytes([PRE]) + val
    val = time_dump(t) + val
    return val


def write(path, val=None, t=-1, newmeta=None, blobs_size=blobs_size):
    if t < 0 or t is None: t = time()
    path = resolve(path)
    dat = ''
    if newmeta and (val is None):
        if not cli().exists(path):
            val = newmeta.get('current', None)
    if val is not None:
        dat = data_write(path, t, val, blobs_size=blobs_size)
        if newmeta:
            newmeta['current'] = val
    if newmeta and 't' not in newmeta:
        # Add internal timestamp
        newmeta = newmeta.copy()
        newmeta['t'] = t
    p = cli().pipeline()
    p.zrange(path, -1, -1, withscores=True)
    if val is not None:
        if is_time_simulated():
            p.zremrangebyscore(path, t, t)
        p.zadd(path, {dat: t})
        p.publish('>' + path, dat)
    if newmeta:
        p.set(path + '#meta', dumps(newmeta))
    r = p.execute()
    if len(r) and len(r[0]) and len(r[0][-1]):
        if r[0][-1][1] > t:
            print('Wrote in the past! {} mem={} > new={}'.format(path, r[0][-1][1], t))
            return False
    return True


def read_meta(path):
    path = resolve(path)
    p = cli().pipeline()
    p.get(path + '#meta')
    p.zrange(path, -1, -1, withscores=True)
    meta, latest = p.execute()
    if meta is None:
        return None
    meta = loads(meta)
    # Remove internal timestamp
    t = meta.pop('t', 0)
    if latest:
        current, t = latest[0]
        meta['current'] = data_read(current)
    return t, meta


def read(path, startIdx=None, endIdx=None):
    """Returns a list of elements between idx. If equals, list contains only one element"""
    if startIdx is None:
        startIdx = -1
    if endIdx is None:
        endIdx = startIdx
    path = resolve(path)
    r = cli().zrange(path, startIdx, endIdx, withscores=True)
    return [[t, data_read(d)] for d, t in r]


def read_time(path, startTime=0, endTime=-1):
    if endTime <= 0: endTime = time()
    path = resolve(path)
    r = cli().zrange(path, startTime, endTime, withscores=True, byscore=True, desc=False)
    return [[t, data_read(d)] for d, t in r]

    
def length(path):
    path = resolve(path)
    return cli().zcard(path)

    
def clear(path):
    """Remove all elements except latest one"""
    path = resolve(path)
    go('rm {}{}:*'.format(blobdir, path.replace('/', '.')))
    if path == '*':
        cli().publish('>clear', path)
        return True
    p = cli().pipeline()
    p.zremrangebyrank(path, 0, -2)
    p.publish('>clear', path)
    p.execute()
    return True


def nearest(path, t, index=False):
    path = resolve(path)
    kw = dict(byscore=True, num=1, offset=0, withscores=True)
    c = cli().pipeline()
    c.zrange(path, t, TIME_MIN, desc=True, **kw)
    c.zrange(path, t , t + TIME_MAX, desc=False, **kw)
    low, high = c.execute()
    if low and high:
        if abs(low[0][1] - t) < abs(high[0][1] - t):
            ret = low
        else:
            ret = high
    elif low:
        ret = low
    elif high:
        ret = high
    else:
        return None
    data, t = ret[0]
    d = data_read(data)
    out = [t, d]
    if index:
        out.append(cli().zrank(path, data))
    return out
        

resolved = {}
linked = defaultdict(set)


def resolve(name):
    if name[0] != '@':
        return name
    r = resolved.get(name, 0)
    if not r:
        r = cli().get(name).decode()
        resolved[name] = r
        linked[r].add(name) 
    return r


def new_name(basedir):
    """Find a new progressive name for subdirectory of `base`"""
    space = 'keyspace#' + basedir
    N = str(cli().scard(space))
    name = sep.join((basedir, N, ''))
    cli().sadd(space, name)
    return name
