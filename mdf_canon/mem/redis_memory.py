import os, threading
from .redis_io import cli
import json
from traceback import format_exc
import uuid
from mdf_canon.csutil import executor
import shutil
import numpy as np
from . import redis_io as rio
from .redis_io import blobdisk, disk_interval, convert_ram_name_to_disk, blob_time, time_dump
from mdf_canon.logger import get_module_logging
from mdf_canon.csutil import time
from collections import defaultdict
logging = get_module_logging(__name__)

maxnumber = 1000
memory_limit = 200e6  # ~200MB


def save_blob(blob_name):
    if disk_interval:
        dest = convert_ram_name_to_disk(blob_name)
    else:
        dest = blobdisk + blob_name
    # If destination path exists, remove source
    source = rio.blobdir + blob_name
    if os.path.exists(dest):
        os.remove(source)
    else:
        shutil.move(source, dest)
    return True


def offload_blob(blob_name):
    try:
        if blobdisk:
            return save_blob(blob_name)
        else:
            os.remove(rio.blobdir + blob_name)
            return True
    except:
        logging.error(format_exc())
        return False


class RedisMemoryManager(object):
    name = '-'
    _ping = time()
    overall = 0
    count = 0
    loops = 0
    removed = set([])
    skipped = set([])
    trimlist = set([])
    trimmed = set([])
    cleanup_time = 0
    trim_time = 0
    cleanup_list_time = 0
    
    @classmethod
    def start(cls):
        name = '{}-{}-{}'.format(os.getpid(), threading.get_ident(), str(uuid.uuid4()))
        if not cli().setnx('RedisMemoryManager', cls.packet()):
            return False
        cls.log('start', name)
        cli().expire('RedisMemoryManager', 5)
        cls.name = name
        executor().submit(cls.loop)
        return True
    
    @classmethod
    def log(cls, *msg):
        msg = list(msg)
        ch = ''
        if len(msg) > 1:
            ch = msg.pop(0)
        msg = ('{} ' * len(msg)).format(*msg)
        logging.info('RedisMemoryManager.' + ch + ': ' + msg)
        cli().publish('RedisMemoryManager.' + ch, msg)
    
    @classmethod
    def noduplicate(cls):
        pkt = cli().get('RedisMemoryManager')
        if not pkt:
            logging.debug(pkt)
            return False
        pkt = json.loads(pkt)
        if pkt['id'] == cls.name:
            return True
        msg = 'Me: {} / Other: {}'.format(cls.name, pkt['id'])
        cls.log('noduplicate', msg)
        return False
    
    @classmethod
    def packet(cls):
        return json.dumps({'id':cls.name,
                 'memory':cls.overall,
                 'count':cls.count,
                 'time':time(),
                 'removed':len(cls.removed),
                 'skipped':len(cls.skipped),
                 'trimmed':len(cls.trimmed),
                 'trimlist':list(cls.trimmed),
                 'pid': os.getpid(),
                 'thread': threading.get_ident(),
                 'loops': cls.loops,
                 'cleanup_time': cls.cleanup_time,
                 'cleanup_list_time': cls.cleanup_list_time,
                 'trim_time': cls.trim_time
                 })
        
    @classmethod
    def ping(cls):
        pkt = cls.packet()
        cli().set('RedisMemoryManager', pkt)
        cli().publish('RedisMemoryManager', pkt)
        cls._ping = time()
    
    @classmethod
    def stop(cls):
        cli().publish('>!shutdown', 'reset')
        cli().publish('>!shutdown', 'reset')
        cli().publish('>!shutdown', 'reset')
        cli().delete('RedisMemoryManager')
        
    @classmethod
    def loop(cls):
        cls.log('loop', 'started', cls.name)
        cls.ping()
        sub = cli().pubsub()
        sub.psubscribe(">*")
        cls.loops = 0
        for ev in sub.listen():
            cls.loops += 1 
            if ev['channel'] == b'>!shutdown':
                cls.log('loop', 'EXITING on event:', ev)
                break
            key = ev['channel'][1:].decode()
            cls.trimlist.add(key)
            dt = time() - cls._ping
            if dt < 1:
                continue
            
            if not cls.noduplicate():
                break
            try:
                cls.cleanup()
                cls.trim_all()
            except:
                cls.log('loop', format_exc())
            cls.ping()
                
        cli().delete('RedisMemoryManager')
        cls.log('loop', 'EXITED', cls.name)
    
    @classmethod
    def auto_trim(cls, key):
        rd = cli()
        n = rd.zcard(key)
        if n <= maxnumber:
            return 0
        n1 = int(maxnumber * 0.9)
        rd.zremrangebyrank(key, 0, n1)
        cls.trimmed.add(key)
        if time() - cls.printstamp > 5:
            cls.printstamp = time()
        return n - maxnumber

    printstamp = time()
    
    @classmethod
    def trim_all(cls):
        t0 = time()
        cls.trimmed = set([])
        for key in cls.trimlist:
            cls.auto_trim(key)
        cls.trimlist = set([])
        cls.trim_time = time() - t0
                
    @classmethod
    def remove(cls, key, path, retype):
        ret = offload_blob(path)
        if not ret:
            cls.log('remove', 'Failed:', path)
        
        # Cleanup key
        t = blob_time(path)
        data = time_dump(t) + bytes([retype]) + (rio.blobdir + path).encode()
        # get position for exact time removal
        n = cli().zrem(key, data)
        if not n:  # only for testing
            # cls.log('remove','Orphan', key, data)
            return 'NotFound'
        return ret
    
    @classmethod
    def cleanup(cls):
        t0 = time()
        keys = []
        times = []
        paths = []
        sizes = []
        retypes = []
        cls.overall = 0
        cls.count = 0
        keycount = defaultdict(lambda: 0)
        for path in os.listdir(rio.blobdir):
            if path.count(':') != 3:
                continue
            key, t, size, retype = path.split(':')
            if size == '_':
                size = os.stat(rio.blobdir + path).st_size
            else:
                size = int(size)
            key = '/' + key.replace('.', '/')
            retypes.append(int(retype))
            keys.append(key)
            keycount[key] += 1
            times.append(t)
            sizes.append(size)
            paths.append(path)
            cls.overall += size
            cls.count += 1
        cls.cleanup_list_time = time() - t0
        if cls.overall < memory_limit:
            cls.cleanup_time = time() - t0
            return 0
        
        ordered = np.argsort(np.array(times))
        cls.removed = set([])
        cls.skipped = set([])
        for i in ordered:
            if keys[i] in cls.skipped:
                continue
            if keycount[keys[i]] < 3:
                cls.skipped.add(keys[i])
                continue
            cls.remove(keys[i], paths[i], retypes[i])
            cls.removed.add(paths[i])
            cls.overall -= sizes[i]
            keycount[keys[i]] -= 1
            if cls.overall < memory_limit:
                break
        cls.cleanup_time = time() - t0
        return len(cls.removed)
        
