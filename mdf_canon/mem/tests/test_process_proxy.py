#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Simple remote object access."""
import unittest
import os
from time import sleep
from traceback import print_exc
from multiprocessing import Lock

from mdf_canon.mem.process_proxy import ProcessProxy, ProcessProxyManager


class DummyCallable(object):

    def __init__(self, a=0, b=1):
        self.a = a
        self.b = b

    def set(self, name, value):
        setattr(self, name, value)
        return 'set ', name, value

    def get(self, name):
        return getattr(self, name, 'UNDEFINED')
    
    def sleep(self, t):
        sleep(t)
        return True


class TestProcessProxy(unittest.TestCase):
    pp = False
    @classmethod
    def tearDown(self):
        if self.pp:
            self.pp._stop()
            #self.pp._process.terminate()
        self.pp = False
            
    def test(self):
        self.pp = pp = ProcessProxy(DummyCallable)
        pp._start(0, b=2)
        for i in range(100):
            pp.set('a', i)
            self.assertEqual(pp.get('a'), i)
            pp._set_instance_attribute('a', i + 0.5)
            self.assertEqual(pp._get_instance_attribute('a'), i + 0.5)
            
        os.remove(pp._path + '/pid')
        sleep(1)
        
        
    def test_fail_start(self):
        self.pp = pp = ProcessProxy(DummyCallable)
        self.assertRaises(RuntimeError, pp._start, 1, 2, 3)
        
    def test_max_restarts(self):
        self.pp = pp = ProcessProxy(DummyCallable)
        pp._max_restarts = 2
        pp._start(1, 2)
        
        os.remove(pp._pid_path)
        sleep(1)
        self.assertFalse(pp._is_alive())
        self.assertEqual(pp.get('a'), 1)
        self.assertTrue(pp._is_alive())
        
        os.remove(pp._pid_path)
        sleep(1)
        self.assertFalse(pp._is_alive())
        self.assertEqual(pp.get('a'), 1)
        self.assertTrue(pp._is_alive())
        
        os.remove(pp._pid_path)
        sleep(1)
        self.assertFalse(pp._is_alive())
        self.assertRaises(RuntimeError, lambda: pp.get('a'))
        self.assertFalse(pp._is_alive()) 
               
        
    def test_unpicklable_noinit(self):
        self.pp = pp = ProcessProxy(DummyCallable)
        pp._set_logging(os.path.join('/dev', 'shm', 'mdf', 'pplog'))
        # pp._log.debug(Lock())
        pp._max_restarts = 1
        l = Lock()
        pp._start(1, l)
        a = lambda: pp.set('a', Lock())
        self.assertRaises(RuntimeError, a)
        os.remove(pp._pid_path)
        pp.get('a')
             
    def test_manager(self):
        pm = ProcessProxyManager()
        pm.register('DummyCallable', DummyCallable)
        self.pp = pp = pm.DummyCallable(1, b=3)
        self.assertTrue(pp._process.is_alive())
        self.assertEqual(pp.get('a'), 1)
        self.assertEqual(pp.get('b'), 3)
        
    def test_timestamp(self):
        self.pp = pp = ProcessProxy(DummyCallable)
        pp._start(1)
        ts = pp._get_timestamp()
        pp.set('a', 2)
        ts1 = pp._get_timestamp()
        self.assertGreater(ts1, ts)
        
    def _test_recursion(self):
        self.pp = pp = ProcessProxy(DummyCallable)
        pp._timeout = 0.001
        pp._max_restarts = -1
        pp._start(1)
        for i in range(2000):
            print(('CYCLE', i))
            try:
                pp._start(1)
            except:
                print_exc()


if __name__ == "__main__":
    unittest.main(verbosity=2)
