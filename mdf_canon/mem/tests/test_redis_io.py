#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mdf_canon.mem import reset_redis
from mdf_canon.mem import redis_io as rio
            
if __name__ == "__main__":
    unittest.main(verbosity=2)

def setUpModule():
    print('Starting', __name__)
    reset_redis('/dev/shm/mdf_test/', 1)
    
key = 'Test_redis_io'

 
class Test_redis_io(unittest.TestCase):
    
    @classmethod
    def tearDown(self):
        rio.cli().delete(key)
    
    @classmethod
    def setUp(cls):
        rio.cli().delete(key)
        
    def test_blob_path(self):
        p = rio.blob_path('/a/b',1234.5, 'sdgjkh')
        self.assertEqual(rio.blob_time(p), 1234.5)
    
    def test_convert_name_to_disk(self):
        d = 'a.b.c:11.1234:pid:type'
        old = rio.blobdisk
        rio.blobdisk = '/pippo/'
        f = rio.convert_ram_name_to_disk
        self.assertEqual(f(d), rio.blobdisk + 'a.b.c:11:_:type')
        self.assertEqual(f(d, 2), rio.blobdisk + 'a.b.c:10:_:type')
        self.assertEqual(f(d, 3), rio.blobdisk + 'a.b.c:9:_:type')
        self.assertEqual(f(d, 4), rio.blobdisk + 'a.b.c:8:_:type')
        rio.blobdisk = old
        
    def test_write_read(self):
        rio.write(key, 'pippo', 1234.0)
        re = rio.read(key)[0]
        self.assertEqual(re, [1234.0, 'pippo'])
        rio.cli().delete('')
        
    def test_clear(self):
        rio.write(key, 'pippo')
        rio.write(key, 'pippo')
        self.assertEqual(rio.length(key), 2)
        rio.clear(key)
        self.assertEqual(rio.length(key), 1)
        
    def test_nearest(self):
        for i in range(5, 10): 
            rio.write(key, str(i), float(i))
        r = rio.nearest(key, 7)
        self.assertEqual(r, [7.0, '7'])
        
        # Rounding
        r = rio.nearest(key, 7.1)
        self.assertEqual(r, [7.0, '7'])
        r = rio.nearest(key, 7.8)
        self.assertEqual(r, [8.0, '8'])
        
        # Boundaries
        r = rio.nearest(key, 3)
        self.assertEqual(r, [5.0, '5'])
        r = rio.nearest(key, 12)
        self.assertEqual(r, [9.0, '9'])
        
        # With indexes
        r = rio.nearest(key, 3, index=True)
        self.assertEqual(r[-1],0)
        r = rio.nearest(key, 12, index=True)        
        self.assertEqual(r[-1], 4)
        r = rio.nearest(key, 7, index=True)
        self.assertEqual(r[-1], 2)

        
