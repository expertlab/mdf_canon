#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from traceback import print_exc
import os
import random
from time import time, sleep
import multiprocessing

# from mdf_server import utils_testing as ut
from mdf_canon import csutil
from mdf_canon.csutil import dumps

from mdf_canon.mem import redisbuffer as rb
from mdf_canon.mem import redis_memory
from mdf_canon.mem import redis_io
from mdf_canon.mem.redisbuffer import RedisBuffer

redis_memory.blobdir = '/dev/shm/mdf_test/blobs/'
os.makedirs(rb.redis_io.blobdir, exist_ok=1)
dr = '/fb/'
p = dr + 'test'


def setUpModule():
    print('Starting', __name__)


class TestRedisBuffer(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        rb.reset_redis('/dev/shm/mdf_test/', 1)
        sleep(0.5)
        
    @classmethod
    def tearDownClass(cls):
        rb.stop_redis()

    def setUp(self):
        self.fb = RedisBuffer()
        self.fb.close(p)

    def tearDown(self):
        self.fb.delete_key(p)
        self.fb.close()
        
    def test_meta(self):
        m = {'a':2}
        self.fb.write_meta(p, m)
        r = self.fb.read_meta(p)
        self.assertDictEqual(r[1], m)
            
    def test_dump(self):
        dumps(self.fb)
        
    def test_link(self):
        self.fb.link(p, 'pippo')
        self.assertEqual(rb.resolve('@pippo'), p)
        self.assertEqual(self.fb._length(p), 0)
        self.fb.write_current('@pippo', 1)
        self.assertEqual(self.fb._length(p), 1)
        
    def test_create_keyspace(self):
        k0 = RedisBuffer.create_keyspace('a')
        self.assertEqual(k0, 'a/0/')
        k0 = RedisBuffer.create_keyspace('a')
        self.assertEqual(k0, 'a/1/')
        self.assertTrue(rb.cli().delete('keyspace#a'))

    def test_write(self):
        fb = self.fb
        # Fill with 0 values
        for i in range(100):
            fb.write_current(p, i)
            
    def test_write_read(self):
        fb = self.fb
        # Fill with 0 values
        for i in range(100):
            fb.write_current(p, i)
            self.assertEqual(fb.read_current(p, -1)[1], i)
            
    def test_sequence(self):
        [self.fb.write_current(p, i) for i in range(100)]
        r = self.fb.sequence(p, 0, 10)
        self.assertEqual(len(r), 11)
        self.assertEqual(r[0][1], 0)
        self.assertEqual(r[5][1], 5)
        
    def test_sequence_from_time(self):
        self.fb.retention = 0
        [self.fb.write_current(p, i, float(1000 + i)) for i in range(100)]
        r = self.fb.sequence_from_time(p, 1050)
        self.assertEqual(len(r), 50)
        self.assertEqual(r[0][0], 1050)
        self.assertEqual(r[-1][0], 1099)
            
    def test_get_time_idx(self):
        fb = self.fb
        for i in range(10):
            # Record fifth time
            t0 = time()
            if i == 5:
                t = t0
            fb.write_current(p, i, t0)
        # Search for fifth time index
        self.assertEqual(fb.get_time_idx(p, t), 5)
        # Out of boundaries values
        self.assertEqual(fb.get_time_idx(p, 0), 0)
        self.assertEqual(fb.get_time_idx(p, time()), 9)
        
    def test_clear(self):
        fb = self.fb
        meta = {'metainfo':1}
        print('Writing')
        [fb.write_meta(p, meta, i) for i in range(10)]
        print('Getting last item')
        last = fb.read_current(p, -1)
        print('last1', last)
        
        print('Clearing')
        fb.clear(p)
        self.assertEqual(fb.read_meta(p)[1], meta)
        last1 = fb.read_current(p, -1)
        self.assertEqual(last, last1)
        print('done clear')
        
    def stress_write0(self, pid, dat='a', dt=10, spr=False):
        print('stress_write', pid)
        if spr:
            spr()
        # assert filebuffer.locker.locks[1000].value==9, filebuffer.locker.locks[1000].value
        i = 0
        t0 = time()
        meta = '{}'
        while time() - t0 < dt:
            rn = 'X' * int.from_bytes(os.urandom(1), 'big')
            if meta:
                self.fb.write_meta(p + str(pid), newmeta=meta)
            self.fb.write_current(p + str(pid), dat + rn, newmeta=meta)
            meta = False
            i += 1
        v = 1000.*(time() - t0) / i
        print('W', pid, v)
        self.w_tot.value += v

    # @csutil.profile()
    def stress_write(self, fid, wpid, dat='a', dt=10, spr=False):
        print('stress_write', fid, 'wpid', wpid)
        if spr:
            spr()
        # assert filebuffer.locker.locks[1000].value==9, filebuffer.locker.locks[1000].value
        i = 0
        t0 = time()
        ex = 0
        er = 0
        if os.path.exists(fid):
            os.remove(fid)
            
        while time() - t0 < dt:
            rn = ''
            rn = 'X' * int.from_bytes(os.urandom(1), 'big')
            # print('W pre',i)
            r = None
            try:
                r = self.fb.write_current(fid, dat + rn)
            except:
                print('Error in stress_write', fid, wpid, i)
                print_exc()
                ex += 1
            if not r:
                er += 1
            # print('W post',i)
            # sleep(0.1)
            i += 1
        self.w_ex.value += ex
        self.w_er.value += er
        v = 1000.*(time() - t0) / i
        print('W', fid, 'wpid', wpid, v, i, 'EX', ex, 'ER', er)
        # print(open(p+str(pid),'rb').read())
        self.w_tot.value += v
        
    # @csutil.profile()
    def stress_read(self, fid, wpid, rpid, dt=10, spr=False):
        print('stress_read', fid, 'wpid', wpid, 'rpid', rpid)
        if spr:
            spr()
        # assert filebuffer.locker.locks[1000].value==9, filebuffer.locker.locks[1000].value
        i = 0
        t0 = time()
        ex = 0
        while time() - t0 < dt:
            # print('R pre',i)            
            r = False
            try:
                r = self.fb.read_current(fid, -1)
            except:
                print('Error in stress_read', fid, wpid, i)
                print_exc()
            if not r:
                ex += 1
            # sleep(0.1)
            i += 1
        v = 1000.*(time() - t0) / i
        self.r_ex.value += ex
        print('R', fid, 'wpid', wpid, 'rpid', rpid, v, i, 'EX', ex)
        self.r_tot.value += v
    
    def stress_clear(self, fid, wpid, dt=10, spr=False):
        print('stress_clear', fid, wpid)
        if spr:
            spr()
        t0 = time()
        i = 0
        while time() - t0 < dt:
            sleep(random.random() / 1)
            self.fb.clear(fid)
            i += 1
        v = 1000.*(time() - t0) / i
        print('CLEARS', fid, 'wpid', wpid, v, i)
        self.c_tot.value += i
    
    @unittest.skip('Not enough disk space on gitlab runners')
    def test_performance(self):
        data = 'a' * int(1e6)
        self.w_tot = multiprocessing.Value('f')
        self.w_tot.value = 0
        self.r_tot = multiprocessing.Value('f')
        self.r_tot.value = 0
        self.c_tot = multiprocessing.Value('i')
        self.c_tot.value = 0
        
        self.w_ex = multiprocessing.Value('i')
        self.w_er = multiprocessing.Value('i')
        self.r_ex = multiprocessing.Value('i')
        
        files = 1
        clear = 0  # run a clearing process
        w_concurrency = 2
        r_concurrency = 2
        
        c = []
        w = []
        r = []
        # filebuffer.locker.locks[1000].value=9
        for n in range(files):
            fid = '{}{}'.format(p, n)
            for wpid in range(w_concurrency):
                w.append(multiprocessing.Process(target=self.stress_write,
                                                 name='Write{}-{}'.format(fid, wpid),
                                                 args=(fid, wpid, data),
                                                 kwargs={'spr': csutil.sharedProcessResources}))     
                w[-1].start()
                
            sleep(1)
            for wpid in range(w_concurrency):
                for rpid in range(r_concurrency):
                    
                    r.append(multiprocessing.Process(target=self.stress_read,
                                                      name='Read{}-{}-{}'.format(fid, wpid, rpid),
                                                      args=(fid, wpid, rpid),
                                                      kwargs={'spr': csutil.sharedProcessResources}))
                    r[-1].start()
                
                if clear:
                    c.append(multiprocessing.Process(target=self.stress_clear,
                                                 name='Clear{}{}'.format(fid, wpid),
                                                 args=(fid, wpid,),
                                                 kwargs={'spr': csutil.sharedProcessResources}))
                    c[-1].start()
                
        # Stop all
        j = lambda p: p.join(30)
        list(map(j, w))
        list(map(j, r))
        list(map(j, c))
        tc = files * (w_concurrency + r_concurrency)
        wbr = files * (w_concurrency * r_concurrency or 1)
        print('EXCEPTIONS', self.w_ex.value, self.r_ex.value)
        print('WROTE IN THE PAST', 100 * self.w_er.value / self.w_tot.value, '%')
        print('TOT', (self.w_tot.value + self.r_tot.value) / tc,
              'WRITE', self.w_tot.value / w_concurrency / files,
              'READ', self.r_tot.value / wbr,
              'CLEAN', self.c_tot.value / w_concurrency / files)
        
        self.assertFalse(self.w_ex.value)
        self.assertFalse(self.r_ex.value)
        if r_concurrency:
            self.assertGreater(self.r_tot.value, 0)
            
            
if __name__ == "__main__":
    unittest.main(verbosity=2)
