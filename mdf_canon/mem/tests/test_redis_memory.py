#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from traceback import print_exc
import os
from time import sleep

from mdf_canon.mem import redisbuffer as rb
from mdf_canon.mem import redis_memory
from mdf_canon.mem import reset_redis

redis_memory.blobdir = '/dev/shm/mdf_test/blobs/'
os.makedirs(rb.redis_io.blobdir, exist_ok=1)
dr = '/fb/'
p = dr + 'test'


def setUpModule():
    print('Starting', __name__)
    reset_redis('/dev/shm/mdf_test/', 1)


class TestRedisMemoryManager(unittest.TestCase):
        
    def test_memory_manager(self):
        rd = rb.cli()
        self.assertFalse(rd.setnx('RedisMemoryManager', 1))
        self.assertFalse(rb.RedisMemoryManager.start())
        rb.memory_manager.stop()
        sleep(.1)
        self.assertTrue(rb.RedisMemoryManager.start())
        self.assertFalse(rd.setnx('RedisMemoryManager', 1))

            
if __name__ == "__main__":
    unittest.main(verbosity=2)
