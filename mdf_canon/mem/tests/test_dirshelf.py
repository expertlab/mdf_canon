#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import unittest
from time import time
from mdf_canon.csutil import dumps
from mdf_canon.mem import dirshelf, reset_redis

props = {'a': {'handle': 'a', 'type': 'Object', 'current': [1, 2]}}

dpath = '/dev/shm/mdf_test/ds'


def setUpModule():
    print('Starting', __name__)
    reset_redis('/dev/shm/mdf_test_canon/', 1)


class TestDirShelf(unittest.TestCase):

    def setUp(self):
        self.d = dirshelf.DirShelf(dpath, 'dirshelf')

    def tearDown(self):
        self.d.close()
        self.d.cache.clear()

    def test_dump(self):
        dumps(self.d)

    def test_update(self):
        d = self.d
        d.update(props)
        p1 = d['a']
        self.assertEqual(p1['current'], props['a']['current'])
    
    def test_get(self):
        # self.assertEqual(self.d.get_current('b', 42), 42)
        self.assertEqual(self.d.get('b', 42), 42)

    def test_nocron(self):
        d = self.d
        d.idx_entries = 1
        d.set('a', 1, newmeta=False)
        t = time()
        d.set('a', 2, newmeta=False)
        # self.assertEqual(d.info[:2], (0, 2))
        # self.assertAlmostEqual(d.info[2], t, delta=0.001)
        d.set('a', 3, newmeta=False)
        self.assertEqual(d.get('a', meta=False), 3)
        for i in range(10):
            t = time()
            d.set('a', i, newmeta=False)
            self.assertEqual(d.get('a', meta=False), i)
            # self.assertEqual(d.info[:2], (0, i + 4))
            # self.assertAlmostEqual(d.info[2], t, delta=0.001)


if __name__ == "__main__":
    unittest.main(verbosity=2)
