# -*- coding: utf-8 -*-
"""Shelve-like object implemented using filesystem-level FileBuffer circular buffer."""
import os
from mdf_canon.csutil import pickle, time
from .redisbuffer import RedisBuffer as MemBuffer
from .redisbuffer import invalid

sep = '\\' if os.name == 'nt' else '/'

type_sanitizers = {'Float': float,
              'Integer': int,
              'Number': float,
              'List':list,
              False: lambda val: val}


class DirShelf(MemBuffer):
    """Permanent object storage with good concurrency.
    Each key is saved/red to/from a file in a directory (possibly in the shared memory space /dev/shm.
    Keys must be valid file names. Values are automatically pickled/unpickled. """
    _dir = False
    basedir = False
    viewdir = False
    
    def __init__(self, basedir='', viewdir=False, desc=False, protocol=pickle.DEFAULT_PROTOCOL, readonly=False, **k):
        """`basedir`: RAM directory for sharing object pickles and current values"""
        super(DirShelf, self).__init__(readonly=readonly)
        self.types = {}  # data types cache
        if not basedir.endswith(sep):
            basedir += sep
        self.basedir = basedir
        self.viewdir = super().create_keyspace(self.basedir, viewdir)
        self.protocol = protocol
        if desc:
            self.update(desc)
            
    @property
    def dir(self): 
        if not self._dir and self.basedir and self.viewdir:
            self._dir = os.path.join(self.basedir, self.viewdir.lstrip('/'))
        return self._dir

    def close(self):
        super(DirShelf, self).close(self.dir)
        if self.readonly:
            return True
        super().remove_base(self.dir)
        return True
        
    def has_key(self, key):
        return super().has_key(self.fp(key))
    
    def __del__(self):
        self.close()
        
    def __contains__(self, key):
        return self.has_key(key)

    def fp(self, key):
        """Object path"""
        if key == '':
            r = os.path.join(self.dir, 'self')
        else:
            r = os.path.join(self.dir, str(key), 'self')
        return r
    
    def get_meta(self, key, *a):
        try:
            fpk = self.fp(key)
            return self.read_meta(fpk)[1]
        except:
            if len(a) == 1:
                return a[0]
            print('Exception reading', self.dir, key)
            raise
    
    def set_meta(self, key, meta, t=-1):
        if t < 0: t = time()
        self.types[key] = meta['type']
        val = meta.pop('current', None)
        if meta['handle'] != key:
            print('Handle mismatch: {} {} {}'.format(key, meta['handle'], self.dir))
            meta['handle'] = key
        return self.write_meta(self.fp(key), meta, t=t, current=val)
        
    def set(self, key, val=None, t=-1, newmeta=True):
        assert self.readonly == False
        if newmeta:
            return self.set_meta(key, val, t=t)
        return self.set_current(key, val, t=t)
        
    __setitem__ = set

    def set_current(self, key, val, t=-1):
        cur = type_sanitizers.get(self.types.get(key, False),
                                  lambda a:a)(val)
        self.write_current(self.fp(key), cur, t=t)
        if key == 'fullpath' and len(cur) > 2:
            self.link(self.dir, cur)
        return cur

    def get(self, key, *a, **k):
        meta = k.get('meta', True)
        path = self.fp(key)
        if meta:
            r = self.read_meta(path)
        else:
            r = self.read_current(path)
        if r is None: r = invalid
        if r is invalid:
            if len(a) == 1:
                return a[0]
            return r
        return r[-1]
    
    def get_current(self, key, *a, **k):
        k['meta'] = False
        return self.get(key, *a, **k)

    def __getitem__(self, key):
        return self.get_meta(key)
    
    def __delitem__(self, key):
        self.delete_key(self.fp(key))
        
    def pop(self, key, alt=None):
        ret = self[key]
        if ret is not invalid:
            del self[key]
        elif alt is not None:
                return alt
        return ret

    def keys(self, key=''):
        return super().subkeys(self.dir, key=key)
    
    def __len__(self):
        return len(list(super().subkeys(self.dir)))
            
    def update(self, desc):
        assert self.readonly == False
        for k, v in desc.items():
            self.set_meta(k, v)

    def items(self, key=''):
        for k in self.keys(key):
            try: 
                r = self.get(k)
                if r == invalid:
                    print('invalid item', k, r)
                    continue
                yield (k, r)
            except:
                pass
            
    def values(self, key=''):
        for k in self.keys(key):
            yield self.get(k) 
            
    def copy(self, key=''):
        """Returns a dictionary containing all items, mimicking dict.copy"""
        res = {}
        for k, v in self.items(key):
            res[k] = v
        return res

    def h_get(self, key, startIdx=0, endIdx=-1):
        """Returns key sequence from startIdx to endIdx"""
        if startIdx == endIdx:
            return self.read_current(self.fp(key), startIdx)
        r = self.sequence(self.fp(key), startIdx, endIdx)
        return r

    def h_time_at(self, key, idx=-1):
        """Returns the time of the last recorded point"""
        return self.time(self.fp(key), idx)

    def h_get_time(self, key, t):
        """Searches the nearest point to time `t` and returns its index for option `name`."""
        r = self.get_time_idx(self.fp(key), t)
        return r
    
    def h_get_time_value(self, key, t):
        """Find index, time, value nearest to time `t`"""
        r = self.get_time_value(self.fp(key), t)
        return r

    def h_get_history(self, key, t0=-2, t1=0):
        """Returns history vector for option `name` between time `t0` and time `t1`"""
        # Translate time to the past
        t = time()
        if t1 <= 0:
            t1 += t
        if t0 <= 0:
            t0 += t
        # Get the index corresponding to oldest point
        idx0 = self.h_get_time(key, t0)
        # Get the index corresponding to newest point
        idx1 = self.h_get_time(key, t1)
        v = self.h_get(key, idx0, idx1)
        # Nothing found
        if len(v) == 0:
            return False
        return v

    def h_clear(self, key=''):
        """Clears the history registry."""
        self.clear(self.fp(key))
