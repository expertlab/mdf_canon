# -*- coding: utf-8 -*-
"""Redis-based FileBuffer implementation"""
from time import  sleep
from mdf_canon.csutil import time
import os
from traceback import print_exc
from . import redis_io
from .redis_io import cli, write, resolve, sep, new_name, linked
from .redis_memory import RedisMemoryManager
from .process_proxy import ProcessProxy
import shutil
import gc
memory_manager = None


class InvalidBufferEntry(object):
    """Invalid item"""

    def __contains__(self, *foo):
        return False

    def __bool__(self):
        return False


invalid = InvalidBufferEntry()
key_cache = set([])


def reset_redis(dbpath='/dev/shm/mdf/', db=0):
    global memory_manager
    gc.collect()
    redis_io.redis_db_number = db
    blobdir = dbpath + 'blobs/'
    redis_io.blobdir = blobdir
    print('reset_redis', blobdir, 'db number', db)
    redis_io.reset_redis_io()
    stop_redis()
    shutil.rmtree(os.path.join(dbpath, 'RedisMemoryManager'), ignore_errors=True)
    memory_manager = ProcessProxy(RedisMemoryManager, base_path=dbpath, max_restarts=5, unique=True)
    memory_manager.start()
    return 1


def stop_redis():
    global memory_manager, key_cache
    r = cli()
    r.publish('>!shutdown', 'reset')
    r.publish('>!shutdown', 'reset')
    r.publish('>!shutdown', 'reset')
    sleep(1)
    cli().execute_command('FLUSHDB SYNC')
    key_cache = set([])
    
    if memory_manager:
        memory_manager._stop()
        memory_manager = None


class RedisBuffer(object):
    _ping = time()
    cache = {}
    readonly = False
    
    def __init__(self, readonly=False):
        self.readonly = readonly
        
    def __del__(self):
        self.close()
        
    def __len__(self):
        return self.length()
    
    @staticmethod
    def resolve(*a, **k):
        return resolve(*a, **k)
    
    @classmethod
    def listdir(cls, base):
        for k in cli().keys(base + '*'):
            k = k.decode()
            if '#' in k:
                continue
            yield k
            
    @classmethod  
    def walk(cls, base, func):
        """Call func on each key"""
        for k in cli().keys(base + '*/self'):
            k = k.decode()
            if '@' in k:
                continue
            func(k)
    
    @classmethod
    def remove_base(cls, basedir):
        """Entirely delete all sub-keys of basedir"""
        for k in cli().keys(basedir + '*'):
            cli().delete(k)
        return True
    
    @staticmethod
    def delete_key(path):
        resolve(path)
        r = cli().delete(path)
        r += cli().delete(path + '#meta')
        for k in linked.get(path, []):
            print('removing linked', k, path)
            r += cli().delete(k)
        if path in key_cache:
            key_cache.remove(path)
        if not r:
            print('delete_key failed', path)
        return r
    
    @staticmethod
    def has_key(path):
        """Check if `path` exists"""
        path = resolve(path)
        if path in key_cache:
            return True
        r = cli().exists(path)
        if r:
            key_cache.add(path)
        return r
    
    @staticmethod
    def subkeys(basedir, key=''):
        q = basedir 
        if key:
            q += key + '/'
        q += '*/self'
        for k in cli().keys(q):
            if b'#' in k:
                continue
            k = k.decode()
            y = k.split('/')[-2]
            yield y

    @staticmethod
    def create_keyspace(basedir, viewdir=False):
        if viewdir:
            if not viewdir.endswith(sep):
                viewdir += sep
            cli().sadd('keyspace#' + basedir, viewdir)
        else:
            viewdir = new_name(basedir) 
            
        print('created keyspace', basedir, viewdir)
        return viewdir
    
    def close(self, *a, **k):
        self.path = False
        
    def length(self):
        return self._length(self.path)
    
    @staticmethod
    def _length(path):
        return cli().zcard(path)
    
    @staticmethod
    def time(path, idx=-1):
        r = redis_io.read(path, idx)
        if not r:
            return -1
        return r[0][0]
    
    def get_time_idx(self, path, t):
        """Find nearest index corresponding to time `t`"""
        r = redis_io.nearest(path, t, index=True)
        if not r:
            return -1
        return r[-1]
    
    def get_time_value(self, path, t=0):
        """Find index, time, value nearest to time `t`"""
        return redis_io.nearest(path, t, index=False)
    
    @staticmethod
    def link(numbered, link):
        cli().set('@' + link, numbered)
        cli().set('@' + numbered, link)
    
    def read_meta(self, path):
        try:
            return redis_io.read_meta(path)
        except:
            print('error while getting', path)
            raise
    
    def read_current(self, path, idx=-1):
        r = redis_io.read(path, idx)
        if not len(r):
            return invalid
        else:
            t, r = r[0]
        return [t, r]
    
    def write_current(self, path, val, t=-1):
        try:
            return write(path, val, t=t)
        except BlockingIOError:
            print_exc()
            return False
    
    def write_meta(self, path, meta, current=None, t=-1):
        return write(path, current, newmeta=meta, t=t)
    
    @staticmethod
    def clear(path):
        return redis_io.clear(path)

    def sequence(self, path, startIdx=0, endIdx=-1):
        return redis_io.read(path, startIdx, endIdx)
    
    def sequence_from_time(self, path, startTime=0, endTime=-1):
        return redis_io.read_time(path, startTime)  

