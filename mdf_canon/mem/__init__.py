# -*- coding: utf-8 -*-
"""Shared data management"""
from .circularbuffer import CircularBuffer
from .redisbuffer import RedisBuffer, reset_redis, stop_redis, InvalidBufferEntry, invalid
from .dirshelf import DirShelf
from .process_proxy import ProcessProxy

MemBuffer = RedisBuffer


def reset(dbpath):
    print('memreset')
    r = reset_redis(dbpath)
    return r


def stop():
    stop_redis()
