# MDF Canon (Measurement Development Platform)

Welcome to the repository for the MDF Canon component development. 
``mdf_canon`` contains common data access code, written in Python3 and released with a MIT license.

MDF is an open source software platform divided in a client package (``mdf_client``), a server package (``mdf_droid``) and a bridge package (``mdf_canon``).

``mdf_canon`` was forked in 2019 from the github project ``misura.canon``, and since substantially diverged.